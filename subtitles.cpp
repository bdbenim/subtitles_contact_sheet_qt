#include "subtitles.h"

#include <QRegularExpression>
#include <QTime>

//subtitles::subtitles(QObject *parent)
//	: QObject{parent} {

//}

sub_format String2SubFormat(QString format) {
	format = format.toLower();
	if (format == "srt") return sub_srt;
	if (format == "ass") return sub_ass;
	if (format == "vtt" ) return sub_vtt;
	return sub_none;
}

QList<sub_line_t> ProcessSrt(QStringList &content, subtitle_totals_t &totals) {

	QList<sub_line_t> output;

	QString SUB_NUM = "";
	QString SUB_START = "";
	uint64_t start = 0;
	QString SUB_END = "";
	uint64_t end = 0;
	QString SUB_TEXT = "";
	uint64_t len = 0;
	int sub_text_len = 0;
	uint64_t words = 0;

	QRegularExpression Number("^([0-9]+)$");
	QRegularExpression Timestap("^([0-9:,]+) --> ([0-9:,]+)$");

	for (int i = 0; i < content.size(); ++i) {
		QString line = content.at(i).trimmed();

		if (line == "") {
			if (SUB_TEXT == "") continue;
			sub_text_len = SUB_TEXT.length();
			words = SUB_TEXT.split(' ').size();

			totals.Len += len;
			totals.TextLen += sub_text_len;
			totals.Words += words;
			totals.Sub++;
			totals.EmptyLines = SUB_NUM.toInt() - totals.Sub;

			output.append({start, end, len, sub_text_len, false, false});
			//				qInfo() << start << end << len << sub_text_len;
			SUB_TEXT = "";
			SUB_START = "";
			SUB_END = "";
			SUB_NUM = "";
			continue;
		}

		if (Number.match(line).hasMatch()) {
			SUB_NUM = line;
			//				qInfo() << SUB_NUM;
			continue;
		}

		QRegularExpressionMatch time = Timestap.match(line);
		if (time.hasMatch()) {
			SUB_START = time.captured(1);
			SUB_END = time.captured(2);
			start = QTime::fromString(SUB_START, "HH:mm:ss,zzz").msecsSinceStartOfDay();
			end = QTime::fromString(SUB_END, "HH:mm:ss,zzz").msecsSinceStartOfDay();
			len = end - start;
			//				qInfo() << start << end << len;
			continue;
		}

		if (SUB_TEXT != "") SUB_TEXT = SUB_TEXT + "\n";
		SUB_TEXT = SUB_TEXT + line;
	}

	return output;
}

QList<sub_line_t> ProcessVtt(QStringList &content, subtitle_totals_t &totals) {
	QList<sub_line_t> output;

	QString SUB_START = "";
	uint64_t start = 0;
	QString SUB_END = "";
	uint64_t end = 0;
	QString SUB_TEXT = "";
	uint64_t len = 0;
	int sub_text_len = 0;
	uint64_t words = 0;

	QRegularExpression VTT_Header("^WEBVTT.*$");
	QRegularExpression VTT_Note("^NOTE .*$");
	QRegularExpression VTT_Number("^([0-9]+)$");
	QRegularExpression VTT_Timestap("^([0-9:.]+) --> ([0-9:.]+)$");

	bool StartSubtitle = false;

	for (int i = 0; i < content.size(); ++i) {
		QString line = content.at(i).trimmed();

		if (line == "") {
			if (SUB_TEXT == "") {
				if (StartSubtitle) totals.EmptyLines++;
				SUB_TEXT = "";
				SUB_START = "";
				SUB_END = "";
				StartSubtitle = false;
				continue;
			};
			sub_text_len = SUB_TEXT.length();
			words = SUB_TEXT.split(' ').size();

			totals.Len += len;
			totals.TextLen += sub_text_len;
			totals.Words += words;
			totals.Sub++;

			output.append({start, end, len, sub_text_len, false, false});
			//				qInfo() << start << end << len << sub_text_len;

			SUB_TEXT = "";
			SUB_START = "";
			SUB_END = "";
			StartSubtitle = false;
			continue;
		}

		if (VTT_Header.match(line).hasMatch()) continue;
		if (VTT_Note.match(line).hasMatch()) continue;

		if (VTT_Number.match(line).hasMatch()) continue;

		QRegularExpressionMatch VTT_Time = VTT_Timestap.match(line);
		if (VTT_Time.hasMatch()) {
			StartSubtitle = true;
			SUB_START = VTT_Time.captured(1);
			SUB_END = VTT_Time.captured(2);
			start = QTime::fromString(SUB_START, "HH:mm:ss.zzz").msecsSinceStartOfDay();
			end = QTime::fromString(SUB_END, "HH:mm:ss.zzz").msecsSinceStartOfDay();
			len = end - start;
			//				qInfo() << start << end << len;
			continue;
		}

		if (!StartSubtitle) continue;

		if (SUB_TEXT != "") SUB_TEXT = SUB_TEXT + "\n";
		SUB_TEXT = SUB_TEXT + line;
	}
	return output;
}

QList<sub_line_t> ProcessAss(QStringList &content, subtitle_totals_t &totals) {
	QList<sub_line_t> output;

	QString SUB_START = "";
	uint64_t start = 0;
	QString SUB_END = "";
	uint64_t end = 0;
	QString SUB_TEXT = "";
	uint64_t len = 0;
	int sub_text_len = 0;
	uint64_t words = 0;

	QRegularExpression AssLine(
				"^Dialogue: [0-9]+,([0-9:.]+),([0-9:.]+),[a-zA-Z]+,[a-zA-Z]*,[0-9:.]+,[0-9:.]+,[0-9:.]+,[a-zA-Z]*,(.*)$");

	for (int i = 0; i < content.size(); ++i) {

		QRegularExpressionMatch AssMatch = AssLine.match(content.at(i).trimmed());
		if (AssMatch.hasMatch()) {
			SUB_START = AssMatch.captured(1);
			SUB_END = AssMatch.captured(2);
			SUB_TEXT = AssMatch.captured(3);

			start = QTime::fromString(SUB_START, "H:mm:ss.z").msecsSinceStartOfDay();
			end = QTime::fromString(SUB_END, "H:mm:ss.z").msecsSinceStartOfDay();
			len = end - start;

			//TODO: Filter {} comands in text
			sub_text_len = SUB_TEXT.length();
			words = SUB_TEXT.split(' ').size();

			if (sub_text_len == 0) totals.EmptyLines++;

			totals.Len += len;
			totals.TextLen += sub_text_len;
			totals.Words += words;
			totals.Sub++;

			output.append({start, end, len, sub_text_len, false, false});
		}
	}
	return output;
}

QList<sub_line_t> ProcessSubtitles(sub_format Format, QStringList &content, subtitle_totals_t &totals) {
	switch (Format) {
		case sub_srt:
			return ProcessSrt(content, totals);
		case sub_vtt:
			return ProcessVtt(content, totals);
		case sub_ass:
			return ProcessAss(content, totals);
		case sub_none:
			break;
	}
	return QList<sub_line_t>();
}
