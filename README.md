# Subtitles Contact Sheet QT

This program generates video and subtitles previews, contact sheets, small animations (gif, webp,...), screenshots and thumbnails. it is programmed in C++ using the QT libraries, It is specially designed to display subtitles, but isn't limited to that. It works with subtitles in **srt**, **ass**, **vtt** format and with all video files supported by **FFmpeg**. It has many options for customization of appearance, performance and control over the output format. It also has batch processing options and can save multiple configurations to make it much easier to use.

## Features

- Multiplatform (linux and windows tested).
- Supports all FFmpeg video formats.
- Supports srt, ass and vtt subtitles.
- Supports multiple configuration files.
- Parallel processing, can perform multiple screenshots simultaneously.
- Can select keyframes for speedup the screenshots.
- Can skip black frames.
- Can select frames containing subtitles.
- It has a text block that displays information about the video and/or subtitles, fully configurable in content and appearance.
- You can create gif, webp animations and x264, x265 videos, compositing the final result in several ways, individual clips, concatenated or in a grid.
- It has batch processing options. You can process all files recursively from one or several folders and also process the same files with several different options.
- Can take screenshots of virtual reality videos correcting the distortion.
- It allows to select the output folder, absolute or relative to the input file.

## Usage

A complete list of options with explanations of how to use them can be found on the man page. [man page](manpage.md)

### Images Examples (Outdated for version 0.5)

- [Video examples](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/wikis/Video-examples)
- [Subtitles examples](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/wikis/Subtitles-examples)
- [VR examples](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/wikis/VR-examples)
- [Customization examples](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/wikis/Customization-examples)

## Installing

The binaries can be downloaded from the [Deployments page](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/releases).

### Windows

In windows it works as a portable application, just unzip the zip file and put inside the files ffmpeg.exe and ffprobe.exe. Can be downloaded by following the instructions on the [FFmpeg page](https://ffmpeg.org/download.html#build-windows).

### Linux

For distributions that support it, you can install the **.deb**, For the rest the best way to install it is to compile it, you can find the manual below.

## Compiling

### Requirements

Needs qmake and QT base development files, In debian they are the packages *qtbase5-dev* and *qt5-qmake* or *qmake6* and *qt6-base-dev* for the QT6 version.

### Linux

[GNU/Linux Compilations instructions](Manuals/compile_linux.md)

### Windows

I compile using a [static compilation](https://wiki.qt.io/Building_a_static_Qt_for_Windows_using_MinGW) so I don't need to copy dlls. But I do not recommend it because it is quite difficult to configure. Instead, I recommend the following manual.

[Window Compilations instructions](Manuals/compile_windows.md)