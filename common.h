#ifndef COMMON_H
#define COMMON_H

#include <QString>
#include <QDir>
#include <QStringList>

extern bool Debug;
extern bool Quiet;
extern bool Cmd;
extern bool GUIProgress;

extern QString FFMPEG;
extern QString FFPROBE;

typedef uint8_t haligned_t;
typedef uint8_t valigned_t;

enum haligval : uint8_t { aligned_left = 0, aligned_center = 50, aligned_right = 100 };
enum valigval : uint8_t { aligned_up = 0, aligned_middle = 50, aligned_down = 100 };

haligned_t String2HAling(QString pos);
valigned_t String2VAling(QString pos);

void PrintInfo(QString line);
void PrintDebug(QString line);
void PrintError(QString line);
void PrintProgress(int step, int total);

#define DEBUGVAR(var) DebugVarFunt(#var, var)
void DebugVarFunt(QString opt, QString var);
void DebugVarFunt(QString opt, uint64_t var);

QString ScapeFFFilters(QString in);

bool ffmpeg(QStringList params);
bool ffprobe(QString file, int stream, QString var, QString &out);
bool ffprobe(QString file, QString var, QString &out);
bool ffprobe(QString file, QString &out);
QString ReadProgram(QString program, QStringList params);

bool CreateSymlink(QString File, QString Link);
bool CreateHardlink(QString File, QString Link);
bool CreateShortcutU(QString File, QString Shortcut, bool Overwrite);

int StringTime2ms(QString time);

bool GetImageSize(QString image, uint64_t &width, uint64_t &height);

void WriteFile(QString Filename, QString Content);
void AppendFile(QString Filename, QString Content);
QString ReadFile(QString Filename);
#endif // COMMON_H
