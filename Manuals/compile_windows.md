# Windows Compilations instructions

Before we can compile, we have to install everything we need. That is a version of Qt and the necessary compilers. Everything can be installed from the Qt installer.

[Download Qt Installer](https://www.qt.io/download-qt-installer)

## Install with Qt Installer

Once downloaded, we run the installer and it will show us an installation wizard, we will have to create an account in Qt and accept the licenses. I am not going to explain this, sure in Qt they explain it much better than me. Continue until you have chosen the installation folder.

![qt_select_install_dir](Screens/qt_select_install_dir.jpg "Qt Installer Select Dir")

It does not matter in which folder it is installed, in this manual I am going to use the default one, `C:\Qt`. It is necessary to keep it in mind because later we will need to enter in this folder to look for the routes that we have to add to the PATH.

Select **Custom Installation** to allow us to select the version we want to install, and click on **Next**.

![qt_select_version](Screens/qt_select_version.jpg "Qt Installer Select Version")

Click on the arrow of the version you want to install, the program will probably work with all of them, and check the option starting with **MinGW** to install. 

![qt_select_mingw](Screens/qt_select_mingw.jpg "Qt Installer Select MinGW")

For this manual I am going to use **Qt 6.4** and **MinGW 11.2.0 64-bit**.

Now we have to install Qt Creator, MinGW (the same version we selected before). Expand the option **Developer and Designer Tools**. Select **MinGW 11.2.0 64-bit**. Qt Creator is installed by default, and the other options are fine as they are. I discard **Qt Design Studio** because I don't need it.

![qt_select_creator](Screens/qt_select_creator.jpg "Qt Installer Select Qt Creator")

Click Next and the wizard will continue accepting licenses, menu options, and with the installation.

![qt_install_finish](Screens/qt_install_finish.jpg "Qt Installer Finish")

## Configure Windows PATH

Now we have to add to the windows PATH environment variable the installation paths of Qt and MinGW.

### Locating the Qt's bin folder

Inside the `C:\Qt` folder we have to enter the folder with the Qt version we installed, and then the MinGW version. In this example is `C:\Qt\6.4.0\mingw_64\`

![qt_locate_bin](Screens/qt_locate_bin.jpg "Qt Locate Bin")

Inside this folder we can see the bin folder. We have to take note of the full path of this folder to add it to PATH. In this case `C:\Qt\6.4.0\mingw_64\bin`

### Locating MinGW's bin folder

To find the MinGW bin folder we have to look inside the **Tools** folder in `C\:Qt`. Inside this there will be a folder starting with **mingw** with the version number added. And inside will be the bin folder we are interested in.

![qt_locate_mingw_bin](Screens/qt_locate_mingw_bin.jpg "Qt Locate MinGW Bin")

As before, we take note of the full address of this folder, in this case `C:\Qt\Tools\mingw1120_64\bin`.

### Configure Windows PATH

I am not going to explain this step in detail, this may be done differently in different versions of windows, there are many manuals on the internet on how to do it in each specific version.

![path](Screens/path.jpg "Windows PATH")

## Download Application Source Code

I am not going to explain this in detail either, there are several options, clone the repository, download the zips from releases or download the zip of the main branch. Everyone can choose the method that best suits him.

To continue with the manual I am going to suppose that there is a **source** folder in **c:** (`c:\source`) with the source code of the program. This folder can be anywhere, it is just to simplify the paths in the manual.

## Compiling the application

To compile the application we can use Qt Creator or the command line.

### Open project in Qt Creator

![open_project](Screens/open_project.jpg "Open Qt Project")

To compile we have to open the .pro file in the source code folder with Qt Creator. When doing this, the project compilation options of the project will appear, where we can select the folder where the .exe of the program will be generated and many other options.

![configure_project](Screens/configure_project.jpg "Configure Qt Project")

By clicking on Details you can change the folder where the binaries and compilation modes that you need will be generated. The normal compilation is Release, Debug generates a larger binary ready to use the debug mode of Qt creator, and Profile is an intermediate option where the debug information is in a separate file. For this manual I will only use the Release mode and the default paths.

Press the **Configure Project** button. The normal Qt Creator window will appear with the project open. On the bottom right are the buttons to change the compile mode and compile.

![select_compilation_mode](Screens/select_compilation_mode.jpg "Select Mode")

To compile press the button below with the hammer.

The .exe file will be created in the folder we configured earlier in the release folder.

![compiled](Screens/compiled.jpg "Compiled")

### Compiling from command line

To compile in this way we have to open the command line in the folder where the source code is, in this example `C:\source`. There are several ways to do this, which I am not going to explain, if you do not know how to do it maybe this method is not the right one for you.

I am using Windows 11 for the manual and I will use the "Open in Terminal" option that opens PowerShell, but you can also use CMD or even the git command line, if you downloaded the repository with this method.

![open_power_shell](Screens/open_power_shell.jpg "PowerShell")

Now we run **qmake**.

```
PS C:\source> qmake
Info: creating stash file C:\source\.qmake.stash
```

And then **mingw-make**.

```
PS C:\source> mingw32-make
C:/Qt/Tools/mingw1120_64/bin/mingw32-make -f Makefile.Release
mingw32-make[1]: Entering directory 'C:/source'
g++ -c -fno-keep-inline-dllexport -O2 -std=gnu++1z -Wall -Wextra -Wextra -fexceptions -mthreads -DUNICODE -D_UNICODE -DWIN32 -DMINGW_HAS_SECURE_API=1 -DQT_NO_DEBUG -DQT_CORE_LIB -I. -I../Qt/6.4.0/mingw_64/include -I../Qt/6.4.0/mingw_64/include/QtCore -Irelease -I../Qt/6.4.0/mingw_64/mkspecs/win32-g++  -o release\batch.o batch.cpp
g++ -c -fno-keep-inline-dllexport -O2 -std=gnu++1z -Wall -Wextra -Wextra -fexceptions -mthreads -DUNICODE -D_UNICODE -DWIN32 -DMINGW_HAS_SECURE_API=1 -DQT_NO_DEBUG -DQT_CORE_LIB -I. -I../Qt/6.4.0/mingw_64/include -I../Qt/6.4.0/mingw_64/include/QtCore -Irelease -I../Qt/6.4.0/mingw_64/mkspecs/win32-g++  -o release\main.o main.cpp
g++ -Wl,-s -Wl,-subsystem,console -mthreads -o release\subtitles_contact_sheet.exe release/batch.o release/main.o C:\Qt\6.4.0\mingw_64\lib\libQt6Core.a
mingw32-make[1]: Leaving directory 'C:/source'
```

![powershell_compiled](Screens/powershell_compiled.jpg "PowerShell Compiled")

And that's it, the binary is in the folder `C:\source\release`

### Copying the required dlls

Using this method the exe it generates depends on Qt and other MinGW dlls. It will not work if we copy it to another computer that does not have the same version of Qt.

To get it to run on another computer without Qt we have to copy all the necessary dlls to the folder where the program exe is. We can do it manually, running the program without the folders that we added before in the PATH or in another computer. And copying all the dlls that show in the error.

Qt has an application to do this automatically **windeployqt**. It is necessary to execute it passing it as parameter the exe file, and it will copy to the folder all the necessary dlls. In the example I add the option *--no-translations* because this program does not use it.

```
PS C:\source> windeployqt --no-translations release\subtitles_contact_sheet.exe
C:\source\release\subtitles_contact_sheet.exe 64 bit, release executable
Direct dependencies: Qt6Core
All dependencies   : Qt6Core
To be deployed     : Qt6Core
Updating Qt6Core.dll.
Updating libgcc_s_seh-1.dll.
Updating libstdc++-6.dll.
Updating libwinpthread-1.dll.
```

![with_dlls](Screens/with_dlls.jpg "Dlls Copied")

The .o files are temporary files of the compilation, they are not necessary if you copy the program. you only have to copy the dll and exe files.
