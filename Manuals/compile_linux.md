# GNU/Linux Compilations instructions

Instructions for compiling on GNU/linux using the command line. These steps, with the exception of installing the necessary packages, should be valid for any linux distribution. In the commands the *user@test:~$* part is the prompt, it may be different from yours, the command starts after the **$**.

## Installing the necessary packages (Debian / Ubuntu)

    user@test:~$ sudo apt install git make g++ qt5-qmake qtbase5-dev

This command will install everything necessary. Many necessary packages are automatically installed from these due to their dependencies. If you are using the root account you can omit sudo.

***Pachages:***

- **git** Optional, it is only necessary to download the sources, but you can download the compressed file from gitlab, in that case you need the decompressor of the chosen format.
- **make** Standard compilation utility
- **g++** The c++ compiler
- **qt5-qmake** Qt 5 compilation utility
- **qtbase5-dev** Qt 5 base development files

## Installing the necessary packages (Others)

Using the distribution's own tool, either command line or graphical, the following tools must be installed. The package names may be different for each distribution.

- **git**
- **make**
- **g++**
- **qmake**
- **Qt development files**

## Download the sources (git)

Execute the following command to download the gitlab repository

	user@test:~$ git clone https://gitlab.com/vongooB9/subtitles_contact_sheet_qt.git

If everything goes correctly, we will see messages like these

	Cloning into 'subtitles_contact_sheet_qt'...
	remote: Enumerating objects: 45, done.
	remote: Counting objects: 100% (45/45), done.
	remote: Compressing objects: 100% (44/44), done.
	remote: Total 45 (delta 21), reused 0 (delta 0), pack-reused 0
	Receiving objects: 100% (45/45), 37.71 KiB | 9.43 MiB/s, done.
	Resolving deltas: 100% (21/21), done.

Enter in the downloaded folder to see if everything is correct

	user@test:~$ cd subtitles_contact_sheet_qt

Check if you have all the files inside

	user@test:~/subtitles_contact_sheet_qt$ ls
	help_frames_time.txt  help_params.txt     help_tb.txt  LICENSE   README.md
	help_header.txt       help_subtitles.txt  help_vr.txt  main.cpp  subtitles_contact_sheet.pro

As you can see there are the *main.cpp* and *subtitles_contact_sheet.pro* files which are the main files to compile. We will continue inside this folder for the following steps

## Download the sources (tar.gz)

We download the tar.gz file from gitlab, it can be through the browser or we copy the address and download it from the command line.

	user@test:~$ wget https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/archive/main/subtitles_contact_sheet_qt-main.tar.gz

If downloaded correctly we will see something like this

	--2022-05-10 16:50:26--  https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/archive/main/subtitles_contact_sheet_qt-main.tar.gz
	Resolving gitlab.com (gitlab.com)... 172.65.251.78, 2606:4700:90:0:f22e:fbec:5bed:a9b9
	Connecting to gitlab.com (gitlab.com)|172.65.251.78|:443... connected.
	HTTP request sent, awaiting response... 200 OK
	Length: unspecified [application/octet-stream]
	Saving to: 'subtitles_contact_sheet_qt-main.tar.gz'

	subtitles_contact_sheet_q     [ <=>                                  ]  30.45K  --.-KB/s    in 0.005s

	2022-05-10 16:50:26 (5.94 MB/s) - 'subtitles_contact_sheet_qt-main.tar.gz' saved [31182]

Uncompress the tar.gz file

	user@test:~$ tar -xvzf subtitles_contact_sheet_qt-main.tar.gz

It shows us the files that it decompresses and where it puts each one, we can see that it created the folder subtitles_contact_sheet_qt-main and put them inside.

	subtitles_contact_sheet_qt-main/
	subtitles_contact_sheet_qt-main/LICENSE
	subtitles_contact_sheet_qt-main/README.md
	subtitles_contact_sheet_qt-main/help_frames_time.txt
	subtitles_contact_sheet_qt-main/help_header.txt
	subtitles_contact_sheet_qt-main/help_params.txt
	subtitles_contact_sheet_qt-main/help_subtitles.txt
	subtitles_contact_sheet_qt-main/help_tb.txt
	subtitles_contact_sheet_qt-main/help_vr.txt
	subtitles_contact_sheet_qt-main/main.cpp
	subtitles_contact_sheet_qt-main/subtitles_contact_sheet.pro

We will continue inside this folder for the following steps

	user@test:~$ cd subtitles_contact_sheet_qt-main

## Compilling

To compile we first run qmake inside the folder with sources

	user@test:~/subtitles_contact_sheet_qt$ qmake

When it works correctly we will see the following message

	Info: creating stash file /home/redentor/subtitles_contact_sheet_qt/.qmake.stash

Now we execute make

	user@test:~/subtitles_contact_sheet_qt$ make

If it works we will see the following, warning messages may appear, but they should not be a problem for the application to work

	g++ -c -pipe -O2 -std=gnu++1z -Wall -Wextra -D_REENTRANT -fPIC -DQT_NO_DEBUG -DQT_CORE_LIB -I. -I/usr/include/x86_64-linux-gnu/qt5 -I/usr/include/x86_64-linux-gnu/qt5/QtCore -I. -I/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++ -o main.o main.cpp
	g++ -Wl,-O1 -o subtitles_contact_sheet main.o   /usr/lib/x86_64-linux-gnu/libQt5Core.so -lpthread

If we look at the contents of the folder we can see the *subtitles_contact_sheet* file which is the result of the compilation

	user@test:~/subtitles_contact_sheet_qt$ ls
	help_frames_time.txt  help_tb.txt  main.o                   subtitles_contact_sheet.pro
	help_header.txt       help_vr.txt  Makefile
	help_params.txt       LICENSE      README.md
	help_subtitles.txt    main.cpp     subtitles_contact_sheet

We can try running it directly to check that it works.

	user@test:~/subtitles_contact_sheet_qt$ ./subtitles_contact_sheet --help

If we look at the program help, it is ready

## Installing

### For current user (recomended)

This is the most secure method, it does not require root permissions but only allows the current user to use the application. It is the best way to install the application for testing.

The file must be copied to ~/.local/bin/, refers to the home folder of the current user. If this folder does not exist we have to create it

    user@test:~/subtitles_contact_sheet_qt$ mkdir -p ~/.local/bin

And copy there *subtitles_contact_sheet*

    user@test:~/subtitles_contact_sheet_qt$ cp subtitles_contact_sheet ~/local/bin/

It is possible that you do not have this folder in your PATH, to check it you can use the following command

    user@test:~/subtitles_contact_sheet_qt$ echo $PATH | tr ":" "\n"
    /usr/local/sbin
    /usr/local/bin
    /usr/sbin
    /usr/bin
    /sbin
    /bin
    /usr/games
    /usr/local/games
    /snap/bin

This will show a list with all the folders in the PATH, if */home/user/.local/bin* appears in the list, changing user for your current user, it is already correctly installed and you don't need to do anything else

If it is not there we have to add it with the following command

    user@test:~/subtitles_contact_sheet_qt$ echo "export PATH=\"$HOME/.local/bin:\$PATH\"" >> ~/.bashrc

To take effect we have to restart the terminal or close the session and log in again, once this is done we can check again if it worked correctly

    user@test:~/subtitles_contact_sheet_qt$ echo $PATH | tr ":" "\n"
    /home/user/.local/bin
    /usr/local/sbin
    /usr/local/bin
    /usr/sbin
    /usr/bin
    /sbin
    /bin
    /usr/games
    /usr/local/games
    /snap/bin
    /snap/bin

### In the system for all users (needs root)

To install it on the system and be able to use it on any user, we can copy the resulting file to one of the directories in the *PATH*. This operation needs to be done as root

For this example I will use */url/local/bin/*

    user@test:~/subtitles_contact_sheet_qt$ sudo cp subtitles_contact_sheet /usr/local/bin/

With this we can now simply run *subtitles_contact_sheet* from any folder on any user

You can take advantage of this step to change the name of the program, if you do not like it or if you think it is too long. For example: scs

    user@test:~/subtitles_contact_sheet_qt$ sudo cp subtitles_contact_sheet /usr/local/bin/scs
