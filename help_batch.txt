R"====(
%1$s --batch [File/Dir]... [-r] [-f <file_types>]... [-p <1-16>] [-c <program>] -- [Normal params]... [--] ...

In this mode executes the specified program for each of the input files,
if a program is not specified it is itself, if -r and a folder are specified it
does it for all files in the folder and subfolders.
It can work as a daemon or service searching for new files in the specified folders.

You can add the parameter %%file%% to the parameters to indicate the position of
the path to the file within the program parameters, otherwise it will be the first
parameter.

Other parameters are also available:
%%filename%%: Input file name, without path and extension
%%path%%: The path to the input file
%%ext%%: The extension of the input file

You can run several programs in parallel by specifying -p <n>, n can be a number
between 1 and 16.

If you use the -- parameter more than once, run the program as many times as
there are -- parameters. By passing the following parameters.

Example:
%1$s --batch file.mkv file2.mkv -- --config normal -- --screenshots
Is the same as execute:
%1$s file.mkv --config normal
%1$s file.mkv --screenshots
%1$s file2.mkv --config normal
%1$s file2.mkv --screenshots

Options:

  -r, --recursive             Recursive through the specified directories

  -f, --file <tile_types>     Select this files only, example "*.mkv"
                              It is possible to put as many -f as required

  --video                     Same as -f '*.mkv' -f '*.mp4' -f '*.avi' -f '*.mov'
                               -f '*.wmv'  -f '*.m4v'

  --subs                      Same as -f '*.srt' -f '*.vtt' -f '*.ass'

  -p, --parallel <1-16>       Launch this number of processes in parallel

  --no_fail                   Continue even if a process fail

  --show_output               Show the output of executed commands, only posible not using
                              parallel process

  --cmd                       Show commands without executing them

  -q, --quiet                 Hide all messages, except for errors

  --command <program>         Program to launch. If it is used before the first -- it
                              applies to all commands, if it is used after it, only
                              to those commands where it appears.

  --                          Separator, the following parameters are passed
                              to the program to be executed

  %%file%%                    Defines te position of file in the params
  %%filename%%                Input file name, without path and extension
  %%path%%                    The path to the input file
  %%ext%%                     The extension of the input file

Daemon options:

  -t, --time <seconds>        Searches for new files every <seconds>.
                              This means that the program never ends by itself

  -w, --watch                 Uses file system events to detect when there are changes
                              inside the folders

  -m, --modified <seconds>    Only select files with modification time older than this
                              This option avoids processing files that are currently
                              being written. Default for windows: 10, on linux 1

  --recheck <seconds>         Recheck incomplete files after this time. 0 to disable.
                              Default for windows: 10, on linux 0

  -d, --delete <seconds>      Time in seconds when it performs a check of the deleted files
                              to remove them from its internal list, default 3600

)===="
