#include "colors.h"
#include "common.h"

color_t String2Color(QString StrColor) {
	StrColor = StrColor.toLower();
	color_t ReturnColor = {0x00, 0x00, 0x00, 0xFF, false};
	bool ColorOK;

	//FFMpeg colors
	if (StrColor == "aliceblue") return {0xF0, 0xF8, 0xFF, 0xFF, true};
	if (StrColor == "antiquewhite") return {0xFA, 0xEB, 0xD7, 0xFF, true};
	if (StrColor == "aqua") return {0x00, 0xFF, 0xFF, 0xFF, true};
	if (StrColor == "aquamarine") return {0x7F, 0xFF, 0xD4, 0xFF, true};
	if (StrColor == "azure") return {0xF0, 0xFF, 0xFF, 0xFF, true};
	if (StrColor == "beige") return {0xF5, 0xF5, 0xDC, 0xFF, true};
	if (StrColor == "bisque") return {0xFF, 0xE4, 0xC4, 0xFF, true};
	if (StrColor == "black") return {0x00, 0x00, 0x00, 0xFF, true};
	if (StrColor == "blanchedalmond") return {0xFF, 0xEB, 0xCD, 0xFF, true};
	if (StrColor == "blue") return {0x00, 0x00, 0xFF, 0xFF, true};
	if (StrColor == "blueviolet") return {0x8A, 0x2B, 0xE2, 0xFF, true};
	if (StrColor == "brown") return {0xA5, 0x2A, 0x2A, 0xFF, true};
	if (StrColor == "burlywood") return {0xDE, 0xB8, 0x87, 0xFF, true};
	if (StrColor == "cadetblue") return {0x5F, 0x9E, 0xA0, 0xFF, true};
	if (StrColor == "chartreuse") return {0x7F, 0xFF, 0x00, 0xFF, true};
	if (StrColor == "chocolate") return {0xD2, 0x69, 0x1E, 0xFF, true};
	if (StrColor == "coral") return {0xFF, 0x7F, 0x50, 0xFF, true};
	if (StrColor == "cornflowerblue") return {0x64, 0x95, 0xED, 0xFF, true};
	if (StrColor == "cornsilk") return {0xFF, 0xF8, 0xDC, 0xFF, true};
	if (StrColor == "crimson") return {0xDC, 0x14, 0x3C, 0xFF, true};
	if (StrColor == "cyan") return {0x00, 0xFF, 0xFF, 0xFF, true};
	if (StrColor == "darkblue") return {0x00, 0x00, 0x8B, 0xFF, true};
	if (StrColor == "darkcyan") return {0x00, 0x8B, 0x8B, 0xFF, true};
	if (StrColor == "darkgoldenrod") return {0xB8, 0x86, 0x0B, 0xFF, true};
	if (StrColor == "darkgray") return {0xA9, 0xA9, 0xA9, 0xFF, true};
	if (StrColor == "darkgreen") return {0x00, 0x64, 0x00, 0xFF, true};
	if (StrColor == "darkkhaki") return {0xBD, 0xB7, 0x6B, 0xFF, true};
	if (StrColor == "darkmagenta") return {0x8B, 0x00, 0x8B, 0xFF, true};
	if (StrColor == "darkolivegreen") return {0x55, 0x6B, 0x2F, 0xFF, true};
	if (StrColor == "darkorange") return {0xFF, 0x8C, 0x00, 0xFF, true};
	if (StrColor == "darkorchid") return {0x99, 0x32, 0xCC, 0xFF, true};
	if (StrColor == "darkred") return {0x8B, 0x00, 0x00, 0xFF, true};
	if (StrColor == "darksalmon") return {0xE9, 0x96, 0x7A, 0xFF, true};
	if (StrColor == "darkseagreen") return {0x8F, 0xBC, 0x8F, 0xFF, true};
	if (StrColor == "darkslateblue") return {0x48, 0x3D, 0x8B, 0xFF, true};
	if (StrColor == "darkslategray") return {0x2F, 0x4F, 0x4F, 0xFF, true};
	if (StrColor == "darkturquoise") return {0x00, 0xCE, 0xD1, 0xFF, true};
	if (StrColor == "darkviolet") return {0x94, 0x00, 0xD3, 0xFF, true};
	if (StrColor == "deeppink") return {0xFF, 0x14, 0x93, 0xFF, true};
	if (StrColor == "deepskyblue") return {0x00, 0xBF, 0xFF, 0xFF, true};
	if (StrColor == "dimgray") return {0x69, 0x69, 0x69, 0xFF, true};
	if (StrColor == "dodgerblue") return {0x1E, 0x90, 0xFF, 0xFF, true};
	if (StrColor == "firebrick") return {0xB2, 0x22, 0x22, 0xFF, true};
	if (StrColor == "floralwhite") return {0xFF, 0xFA, 0xF0, 0xFF, true};
	if (StrColor == "forestgreen") return {0x22, 0x8B, 0x22, 0xFF, true};
	if (StrColor == "fuchsia") return {0xFF, 0x00, 0xFF, 0xFF, true};
	if (StrColor == "gainsboro") return {0xDC, 0xDC, 0xDC, 0xFF, true};
	if (StrColor == "ghostwhite") return {0xF8, 0xF8, 0xFF, 0xFF, true};
	if (StrColor == "gold") return {0xFF, 0xD7, 0x00, 0xFF, true};
	if (StrColor == "goldenrod") return {0xDA, 0xA5, 0x20, 0xFF, true};
	if (StrColor == "gray") return {0x80, 0x80, 0x80, 0xFF, true};
	if (StrColor == "green") return {0x00, 0x80, 0x00, 0xFF, true};
	if (StrColor == "greenyellow") return {0xAD, 0xFF, 0x2F, 0xFF, true};
	if (StrColor == "honeydew") return {0xF0, 0xFF, 0xF0, 0xFF, true};
	if (StrColor == "hotpink") return {0xFF, 0x69, 0xB4, 0xFF, true};
	if (StrColor == "indianred") return {0xCD, 0x5C, 0x5C, 0xFF, true};
	if (StrColor == "indigo") return {0x4B, 0x00, 0x82, 0xFF, true};
	if (StrColor == "ivory") return {0xFF, 0xFF, 0xF0, 0xFF, true};
	if (StrColor == "khaki") return {0xF0, 0xE6, 0x8C, 0xFF, true};
	if (StrColor == "lavender") return {0xE6, 0xE6, 0xFA, 0xFF, true};
	if (StrColor == "lavenderblush") return {0xFF, 0xF0, 0xF5, 0xFF, true};
	if (StrColor == "lawngreen") return {0x7C, 0xFC, 0x00, 0xFF, true};
	if (StrColor == "lemonchiffon") return {0xFF, 0xFA, 0xCD, 0xFF, true};
	if (StrColor == "lightblue") return {0xAD, 0xD8, 0xE6, 0xFF, true};
	if (StrColor == "lightcoral") return {0xF0, 0x80, 0x80, 0xFF, true};
	if (StrColor == "lightcyan") return {0xE0, 0xFF, 0xFF, 0xFF, true};
	if (StrColor == "lightgoldenrodyellow") return {0xFA, 0xFA, 0xD2, 0xFF, true};
	if (StrColor == "lightgreen") return {0x90, 0xEE, 0x90, 0xFF, true};
	if (StrColor == "lightgrey") return {0xD3, 0xD3, 0xD3, 0xFF, true};
	if (StrColor == "lightpink") return {0xFF, 0xB6, 0xC1, 0xFF, true};
	if (StrColor == "lightsalmon") return {0xFF, 0xA0, 0x7A, 0xFF, true};
	if (StrColor == "lightseagreen") return {0x20, 0xB2, 0xAA, 0xFF, true};
	if (StrColor == "lightskyblue") return {0x87, 0xCE, 0xFA, 0xFF, true};
	if (StrColor == "lightslategray") return {0x77, 0x88, 0x99, 0xFF, true};
	if (StrColor == "lightsteelblue") return {0xB0, 0xC4, 0xDE, 0xFF, true};
	if (StrColor == "lightyellow") return {0xFF, 0xFF, 0xE0, 0xFF, true};
	if (StrColor == "lime") return {0x00, 0xFF, 0x00, 0xFF, true};
	if (StrColor == "limegreen") return {0x32, 0xCD, 0x32, 0xFF, true};
	if (StrColor == "linen") return {0xFA, 0xF0, 0xE6, 0xFF, true};
	if (StrColor == "magenta") return {0xFF, 0x00, 0xFF, 0xFF, true};
	if (StrColor == "maroon") return {0x80, 0x00, 0x00, 0xFF, true};
	if (StrColor == "mediumaquamarine") return {0x66, 0xCD, 0xAA, 0xFF, true};
	if (StrColor == "mediumblue") return {0x00, 0x00, 0xCD, 0xFF, true};
	if (StrColor == "mediumorchid") return {0xBA, 0x55, 0xD3, 0xFF, true};
	if (StrColor == "mediumpurple") return {0x93, 0x70, 0xD8, 0xFF, true};
	if (StrColor == "mediumseagreen") return {0x3C, 0xB3, 0x71, 0xFF, true};
	if (StrColor == "mediumslateblue") return {0x7B, 0x68, 0xEE, 0xFF, true};
	if (StrColor == "mediumspringgreen") return {0x00, 0xFA, 0x9A, 0xFF, true};
	if (StrColor == "mediumturquoise") return {0x48, 0xD1, 0xCC, 0xFF, true};
	if (StrColor == "mediumvioletred") return {0xC7, 0x15, 0x85, 0xFF, true};
	if (StrColor == "midnightblue") return {0x19, 0x19, 0x70, 0xFF, true};
	if (StrColor == "mintcream") return {0xF5, 0xFF, 0xFA, 0xFF, true};
	if (StrColor == "mistyrose") return {0xFF, 0xE4, 0xE1, 0xFF, true};
	if (StrColor == "moccasin") return {0xFF, 0xE4, 0xB5, 0xFF, true};
	if (StrColor == "navajowhite") return {0xFF, 0xDE, 0xAD, 0xFF, true};
	if (StrColor == "navy") return {0x00, 0x00, 0x80, 0xFF, true};
	if (StrColor == "oldlace") return {0xFD, 0xF5, 0xE6, 0xFF, true};
	if (StrColor == "olive") return {0x80, 0x80, 0x00, 0xFF, true};
	if (StrColor == "olivedrab") return {0x6B, 0x8E, 0x23, 0xFF, true};
	if (StrColor == "orange") return {0xFF, 0xA5, 0x00, 0xFF, true};
	if (StrColor == "orangered") return {0xFF, 0x45, 0x00, 0xFF, true};
	if (StrColor == "orchid") return {0xDA, 0x70, 0xD6, 0xFF, true};
	if (StrColor == "palegoldenrod") return {0xEE, 0xE8, 0xAA, 0xFF, true};
	if (StrColor == "palegreen") return {0x98, 0xFB, 0x98, 0xFF, true};
	if (StrColor == "paleturquoise") return {0xAF, 0xEE, 0xEE, 0xFF, true};
	if (StrColor == "palevioletred") return {0xD8, 0x70, 0x93, 0xFF, true};
	if (StrColor == "papayawhip") return {0xFF, 0xEF, 0xD5, 0xFF, true};
	if (StrColor == "peachpuff") return {0xFF, 0xDA, 0xB9, 0xFF, true};
	if (StrColor == "peru") return {0xCD, 0x85, 0x3F, 0xFF, true};
	if (StrColor == "pink") return {0xFF, 0xC0, 0xCB, 0xFF, true};
	if (StrColor == "plum") return {0xDD, 0xA0, 0xDD, 0xFF, true};
	if (StrColor == "powderblue") return {0xB0, 0xE0, 0xE6, 0xFF, true};
	if (StrColor == "purple") return {0x80, 0x00, 0x80, 0xFF, true};
	if (StrColor == "red") return {0xFF, 0x00, 0x00, 0xFF, true};
	if (StrColor == "rosybrown") return {0xBC, 0x8F, 0x8F, 0xFF, true};
	if (StrColor == "royalblue") return {0x41, 0x69, 0xE1, 0xFF, true};
	if (StrColor == "saddlebrown") return {0x8B, 0x45, 0x13, 0xFF, true};
	if (StrColor == "salmon") return {0xFA, 0x80, 0x72, 0xFF, true};
	if (StrColor == "sandybrown") return {0xF4, 0xA4, 0x60, 0xFF, true};
	if (StrColor == "seagreen") return {0x2E, 0x8B, 0x57, 0xFF, true};
	if (StrColor == "seashell") return {0xFF, 0xF5, 0xEE, 0xFF, true};
	if (StrColor == "sienna") return {0xA0, 0x52, 0x2D, 0xFF, true};
	if (StrColor == "silver") return {0xC0, 0xC0, 0xC0, 0xFF, true};
	if (StrColor == "skyblue") return {0x87, 0xCE, 0xEB, 0xFF, true};
	if (StrColor == "slateblue") return {0x6A, 0x5A, 0xCD, 0xFF, true};
	if (StrColor == "slategray") return {0x70, 0x80, 0x90, 0xFF, true};
	if (StrColor == "snow") return {0xFF, 0xFA, 0xFA, 0xFF, true};
	if (StrColor == "springgreen") return {0x00, 0xFF, 0x7F, 0xFF, true};
	if (StrColor == "steelblue") return {0x46, 0x82, 0xB4, 0xFF, true};
	if (StrColor == "tan") return {0xD2, 0xB4, 0x8C, 0xFF, true};
	if (StrColor == "teal") return {0x00, 0x80, 0x80, 0xFF, true};
	if (StrColor == "thistle") return {0xD8, 0xBF, 0xD8, 0xFF, true};
	if (StrColor == "tomato") return {0xFF, 0x63, 0x47, 0xFF, true};
	if (StrColor == "turquoise") return {0x40, 0xE0, 0xD0, 0xFF, true};
	if (StrColor == "violet") return {0xEE, 0x82, 0xEE, 0xFF, true};
	if (StrColor == "wheat") return {0xF5, 0xDE, 0xB3, 0xFF, true};
	if (StrColor == "white") return {0xFF, 0xFF, 0xFF, 0xFF, true};
	if (StrColor == "whitesmoke") return {0xF5, 0xF5, 0xF5, 0xFF, true};
	if (StrColor == "yellow") return {0xFF, 0xFF, 0x00, 0xFF, true};
	if (StrColor == "yellowgreen") return {0x9A, 0xCD, 0x32, 0xFF, true};

	if (StrColor.length() == 6) {
		ReturnColor.R = StrColor.left(2).toInt(&ColorOK, 16);
		if (ColorOK) ReturnColor.G = StrColor.mid(2, 2).toInt(&ColorOK, 16);
		if (ColorOK) ReturnColor.B = StrColor.right(2).toInt(&ColorOK, 16);
		if (ColorOK) ReturnColor.Valid = true;
	} else if (StrColor.length() > 6 && StrColor.length() <= 8 ) {
		ReturnColor.R = StrColor.left(2).toInt(&ColorOK, 16);
		if (ColorOK) ReturnColor.G = StrColor.mid(2, 2).toInt(&ColorOK, 16);
		if (ColorOK) ReturnColor.B = StrColor.mid(4, 2).toInt(&ColorOK, 16);
		if (ColorOK) ReturnColor.A = StrColor.right(2).toInt(&ColorOK, 16);
		if (ColorOK) ReturnColor.Valid = true;
	}

	if (!ReturnColor.Valid) PrintError("Color not valid: " + StrColor);

	return ReturnColor;
}

QString Color2RGB(color_t Color) {
	if (!Color.Valid) return "";
	QString Result = ColorChannel2Hex(Color.R);
	Result +=  ColorChannel2Hex(Color.G);
	Result +=  ColorChannel2Hex(Color.B);
	if (Color.A != 0xFF) Result = Result + ColorChannel2Hex(Color.A);
	return Result;
}

QString Color2BGR(color_t Color) {
	if (!Color.Valid) return "";
	QString Result = ColorChannel2Hex(Color.B);
	Result +=  ColorChannel2Hex(Color.G);
	Result +=  ColorChannel2Hex(Color.R);

	if (Color.A != 0xFF) Result = ColorChannel2Hex(Color.A) + Result;

	return Result;
}

QString ColorChannel2Hex(uint8_t Channel) {
	QString Result = QString::number(Channel, 16);
	if (Result.length() == 1) Result = "0" + Result;
	return Result;
}

QString StringColor2BGR(QString InColor) {
	color_t Color = String2Color(InColor);
	return Color2BGR(Color);
}

QString StringColor2RGB(QString InColor) {
	color_t Color = String2Color(InColor);
	return Color2RGB(Color);
}

QString SetTransparentRGB(QString StrColor) {
	color_t Color = String2Color(StrColor);
	Color.A = 0x00;
	return Color2RGB(Color);
}
