#ifndef SUBTITLES_H
#define SUBTITLES_H

#include <QObject>

//Subtitle format variable and utilities
enum sub_format { sub_srt, sub_ass, sub_vtt, sub_none };
sub_format String2SubFormat(QString format);

struct sub_line_t {
	uint64_t start;
	uint64_t end;
	uint64_t len;
	int text_len;
	bool started;
	bool finished;
};

struct subtitle_totals_t {
	uint64_t Len = 0;
	uint64_t TextLen = 0;
	uint64_t Words = 0;
	uint64_t Sub = 0;
	uint64_t EmptyLines = 0;
};

QList<sub_line_t> ProcessSrt(QStringList &content, subtitle_totals_t &totals);
QList<sub_line_t> ProcessVtt(QStringList &content, subtitle_totals_t &totals);
QList<sub_line_t> ProcessAss(QStringList &content, subtitle_totals_t &totals);

QList<sub_line_t> ProcessSubtitles(sub_format Format, QStringList &content, subtitle_totals_t &totals);

//class subtitles : public QObject {
//		Q_OBJECT
//	public:
//		explicit subtitles(QObject *parent = nullptr);

//	signals:

//};

#endif // SUBTITLES_H
