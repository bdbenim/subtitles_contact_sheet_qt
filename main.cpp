#include <QCoreApplication>
#include <QCommandLineParser>
#include <QTextStream>
#include <QSettings>
#include <QVariant>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QTemporaryDir>
#include <QLocale>
#include <QProcess>
#include <QTime>
#include <QRegularExpression>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QQueue>
#include <QStandardPaths>
#include <batch.h>
#include <subtitles.h>

const char *help_batch =
#include "help_batch.txt"
			;

#include "colors.h"
#include "paramssettings.h"

bool Debug = false;
bool Quiet = false;
bool Cmd = false;
bool GUIProgress = false;

#include "common.h"
#include "textsustitution.h"
#include "textblock.h"

//Subtitle format variable
sub_format SubFormat = sub_none;

//Mode variable and utilities
enum modes {mode_time, mode_line, mode_video_time, mode_video_end};
modes Mode = mode_time;
modes String2Mode(QString mode) {
	mode = mode.toLower();
	if (mode == "time") return mode_time;
	if (mode == "line") return mode_line;
	if (mode == "video_time" ) return mode_video_time;
	if (mode == "video_end" ) return mode_video_end;
	return mode_time;
}

enum text_block_positions { text_top, text_bottom, text_hidden };
text_block_positions TextBlockPosition = text_top;
text_block_positions String2TextBlockPotion(QString pos) {
	pos = pos.toLower();
	if (pos == "top") return text_top;
	if (pos == "bottom") return text_bottom;
	if (pos == "hidden") return text_hidden;
	return text_top;
}

enum ft_positions { ft_up_left, ft_up_right, ft_down_left, ft_down_right, ft_up_center, ft_down_center, ft_middle_left, ft_middle_right };
ft_positions FrameTimePosition = ft_up_right;
ft_positions String2FrameTimePosition(QString pos) {
	pos = pos.toLower();
	if (pos == "up_left") return ft_up_left;
	if (pos == "up_right") return ft_up_right;
	if (pos == "down_left") return ft_down_left;
	if (pos == "down_right") return ft_down_right;
	if (pos == "up_center") return ft_up_center;
	if (pos == "down_center") return ft_down_center;
	if (pos == "middle_left") return ft_middle_left;
	if (pos == "middle_right") return ft_middle_right;
	return ft_up_right;
}

valigned_t BackgroundVAling = aligned_middle;
haligned_t BackgroundHAling = aligned_center;
valigned_t TextBlockBackgroundVAling = aligned_middle;
haligned_t TextBlockBackgroundHAling = aligned_center;

const QStringList VideoExtensions = {"mkv", "avi", "mp4", "mov", "m4v", "wmv"};
const QStringList SubtitleExtensions = {"srt", "vtt", "ass"};

QString check_video_file(QString dir, QString name, QString ext) {
	QString video = dir + QDir::separator() + name + "." + ext;
	if (QFile::exists(video))
		return video;
	else
		return "";
}

QString FFMPEG;
QString FFPROBE;

struct processqueue_t {
	QStringList Params;
	QString Message;
	QStringList DependentParams;
	QString DependentMessage;
};

struct instance_t {
	QProcess *Process;
	processqueue_t Queue;
	bool InProgress;
	QString Command;
};

struct format_definition_t {
	QString Ext;
	QString Options;
	QString DefaultQuality;
	bool IndependentCommand;
};

QHash<QString, format_definition_t> OUTPUT_AVAILABLE_FORMATS;

#define TERMINATE(error) for (int i = 0; i < CurrentInstances; i++) {\
delete Instances.at(i).Process;\
}\
if (Save) PS.UpdateConfigFile(true, SaveAll);\
a.exit(error); \
return error

void ExecuteCustomCommand(QString Command, TextSustitution *TS) {
	if (Command == "") return;
	QString CMD = Command;
	CMD = TS->ProcessCMD("", CMD);
	QStringList Params = QProcess::splitCommand(CMD);
	if (Params.isEmpty()) return;
	QString Program = Params.takeFirst();

	QProcess process;
	process.start(Program, Params);
	if (Cmd) PrintInfo(CMD);
	process.waitForFinished(-1);

	if (process.exitCode() != 0 ) {
		QString error = process.readAllStandardError();
		PrintError(CMD);
		PrintError(error);
	}
}

int main(int argc, char *argv[]) {

	OUTPUT_AVAILABLE_FORMATS.insert("jpg", {"jpg", "-q:v {quality}", "3", false});
	OUTPUT_AVAILABLE_FORMATS.insert("jpeg", {"jpeg", "-q:v {quality}", "3", false});
	OUTPUT_AVAILABLE_FORMATS.insert("png", {"png", "", "", false});
	OUTPUT_AVAILABLE_FORMATS.insert("apng", {"png", "-f apng -plays 0 -c:v apng -pred mixed", "", false});
	OUTPUT_AVAILABLE_FORMATS.insert("gif", {"gif", "-filter_complex [0:v]split[a][b];[a]palettegen=stats_mode=single[p];[b][p]paletteuse=new=1", "", true});
	OUTPUT_AVAILABLE_FORMATS.insert("giflow", {"gif", "-filter_complex [0:v]split[a][b];[a]palettegen[p];[b][p]paletteuse", "", true});
	OUTPUT_AVAILABLE_FORMATS.insert("webp", {"webp", "-c:v libwebp -q:v {quality} -compression_level 6", "40", false});
	OUTPUT_AVAILABLE_FORMATS.insert("webploop", {"webp", "-c:v libwebp -q:v {quality} -compression_level 6 -loop 0", "40", false});
	OUTPUT_AVAILABLE_FORMATS.insert("xvid", {"mkv", "-c:v libxvid -qscale:v {quality}", "3", false});
	OUTPUT_AVAILABLE_FORMATS.insert("x264", {"mkv", "-c:v libx264 -crf {quality} -preset medium", "24", false});
	OUTPUT_AVAILABLE_FORMATS.insert("x265", {"mkv", "-c:v libx265 -crf {quality} -x265-params log-level=warning -preset medium", "24", false});

	QCoreApplication a(argc, argv);

	#if defined (Q_OS_WIN)
	if (argc == 1) {
		QString GUI = QCoreApplication::applicationDirPath() + "SCSGUI.exe";
		if (QFile::exists(GUI)) {
			QProcess::startDetached(GUI, QStringList());
			return 0;
		}
	}
	#endif

	ParamsSettings PS(QCoreApplication::applicationDirPath());

	QStringList CONFIG;
	CONFIG.append("config");
	for (int i = 1; i < argc; i++) {
		QString param(argv[i]);
		if (param == "--config" || param == "-c") {
			CONFIG.append(QString::fromUtf8(argv[i + 1]));
		} else if (param == "--list_config") {
			QStringList ConfigList = PS.ConfigList();
			PrintInfo("");
			PrintInfo("Available configuration files:");
			if (ConfigList.count() > 0) {
				int maxlenght = 0;
				for (int i = 0; i < ConfigList.count(); ++i)
					if (ConfigList.at(i).length() > maxlenght) maxlenght = ConfigList.at(i).length();
				maxlenght += 1;
				for (int i = 0; i < ConfigList.count(); ++i) {
					QString Name = ConfigList.at(i).section(":", 0, 0);
					QString Desc = ConfigList.at(i).section(":", 1, -1);
					PrintInfo("  " + Name.leftJustified(maxlenght, ' ') + " | " + Desc.trimmed());
				}
			} else {
				PrintInfo("");
				PrintInfo("No configuration files available.");
			}
			return 0;
		} else if (param == "--list_config_gui") {
			QStringList ConfigList = PS.ConfigList();
			for (int i = 0; i < ConfigList.count(); ++i) {
				PrintInfo(ConfigList.at(i).section(":", 0, 0));
			}
			return 0;
		} else if (param == "--quiet") {
			Quiet = true;
		} else if (param == "-b" || param == "--batch") {
			Batch BatchMode;
			return BatchMode.Execute(argc, argv);
		} else if (param == "--debug") {
			Debug = true;
		} else if (param == "--version" || param  == "-v") {
			fprintf(stdout, "Subtitles Contact Sheet version %s\n", SCS_VERSION);
			return 0;
		} else if (param == "--help" || param == "-h") {
			PS.printhelp(QCoreApplication::applicationName(), "");
			return 0;
		} else if (param == "--help-sub") {
			PS.printhelp(QCoreApplication::applicationName(), "sub");
			return 0;
		} else if (param == "--help-text") {
			PS.printhelp(QCoreApplication::applicationName(), "tb");
			return 0;
		} else if (param == "--help-time") {
			PS.printhelp(QCoreApplication::applicationName(), "ft");
			return 0;
		} else if (param == "--help-vr") {
			PS.printhelp(QCoreApplication::applicationName(), "vr");
			return 0;
		} else if (param == "--help-rename") {
			PS.printhelp(QCoreApplication::applicationName(), "rename");
			return 0;
		} else if (param == "--help-batch") {
			fprintf(stdout, help_batch, QCoreApplication::applicationName().toUtf8().constData());
			return 0;
		} else if (param == "--help-all") {
			PS.printhelp(QCoreApplication::applicationName(), "");
			PS.printhelp(QCoreApplication::applicationName(), "sub");
			PS.printhelp(QCoreApplication::applicationName(), "tb");
			PS.printhelp(QCoreApplication::applicationName(), "ft");
			PS.printhelp(QCoreApplication::applicationName(), "vr");
			PS.printhelp(QCoreApplication::applicationName(), "rename");
			fprintf(stdout, help_batch, QCoreApplication::applicationName().toUtf8().constData());
			return 0;
		} else {
			if (!param.startsWith("--")) continue;
			QString ParamCut = param.remove(0, 2);
			if (PS.Exists(ParamCut)) continue;
			if (PS.ConfigExists(ParamCut)) {
				CONFIG.append(ParamCut);
			}
		}
	}

	for (int i = 0; i < CONFIG.size(); ++i) {
		PrintDebug("");
		PrintDebug("Loading config (" + CONFIG.at(i) + ") file variables");
		if ((CONFIG.at(i).endsWith(".conf") || CONFIG.at(i).endsWith(".ini")) && QFile::exists(CONFIG.at(i)))
			PS.SetConfigFile(CONFIG.at(i));
		else
			PS.SetConfig(CONFIG.at(i));
		PS.ReadFromConfigFile();
	}

	bool Save = false;
	bool SaveAll = false;

	//Comand line params only vars
	QString SUBFILE = "";
	QString VIDEOFILE = "";
	QString OUTPUTFILE = "";
	QString SUBLANGCODE = "";
	QStringList MANUALFRAMES;

	QStringList FFMPEG_COMPILE;
	bool ffmpeg_has_libfontconfig = false;

	//Subtitle vars
	bool SubtitleEnable = false;
	QString SUBDIR = "";
	QString SUBFILEMAME = "";
	QString SUBNAME = "";
	QString SUBEXT = "";
	uint64_t SubtitleFileSize = 0;
	QString SUBTITLE_FORMAT = "";
	QString SUBLANG = "";
	QString SUBNAME_LANG = "";

	//TMP VARS
	QString TMP_TEXT;
	QString TMP_TEXT_IMG;
	QString TMP_IMAGES;
	QString TMP_BACKGROUND;
	QString TMP_TRANSPARENT_SHEET;

	//Ffmpeg params vars
	QStringList FORMAT_OPTIONS;
	bool INDEPENDENT_COMMAND = false;

	//Video file vars
	QString VIDEOFILENAME;
	QString DURATION;
	int64_t Duration;
	int VideoWidth = -1;
	int VideoHeight = -1;
	int NewVideoWidth = -1;
	int NewVideoHeight = -1;
	bool VerticalVideo = false;
	double AspectRatio = 0;
	QString DURATIONFORMATED;
	QString VIDEO_JSON = "";
	QJsonDocument VideoMetadata;
	QJsonParseError JsonError;
	QJsonObject JsonRoot;
	QJsonArray VideoStreams;
	QJsonObject CurrentStream;
	bool VR;
	QString VR_IN = "";
	QString VR_IN_STEREO = "";
	int VR_IH_FOV = 0;
	int VR_IV_FOV = 0;

	//Output vars
	QString OUTPUT_FORMAT;
	QString LAYOUT_X;
	QString LAYOUT_Y;
	uint LayoutX;
	uint LayoutY;
	uint NumberOfFrames;

	//Processing vars
	QList<sub_line_t> Timestamps;
	QList<sub_line_t> Screenshots;
	subtitle_totals_t Totals;
	QString TOTALLENFORMATED = "";
	QStringList SCREENSHOT_FILTERS;
	QString SCREENSHOT_FILTER;
	QStringList SUBTITLE_FORCE_STYLE;
	QString GRID_FILTER;

	//Text block vars
	uint64_t Size;
	int MinSize;
	uint64_t TextBlockWidth;
	uint64_t TextBlockHeight;
	bool PrintVars = false;
	int64_t CoverTopCalculatedHeight = -2;

	//Tmp format options
	QString TMP_FRAME_FORMAT;
	QStringList TMP_FRAME_FORMAT_OPTIONS;

	//Progress
	int Steps = 0;
	int CurrentStep;

	PrintDebug("");
	PrintDebug("Loading parameter variables");

	//Moves the loop to the next element and displays an error if it does not exist
#define next_param(error) i++; \
	if (i == argc) {\
		qFatal(error);\
		return 1;\
	}

#define debug_par_str(var) PrintDebug("\t"+param + ": "+ QString(#var)+" = " +var)
#define debug_par_int(var) PrintDebug("\t"+param + ": "+ QString(#var)+" = " +QString::number(var))
#define debug_par_bool(var) PrintDebug("\t"+param + ": "+ QString(#var)+" = " + QString((var) ? "1" : "0"))

	for (int i = 1; i < argc; i++) {
		QString param(argv[i]);
		if (param == "--sub") {
			next_param("Incorrect use of --sub");
			SUBFILE = QString::fromUtf8(argv[i]);
			debug_par_str(SUBFILE);
			SubtitleEnable = true;
			debug_par_bool(SubtitleEnable);
		} else if (param == "--video") {
			next_param("Incorrect use of --video");
			VIDEOFILE = QString::fromUtf8(argv[i]);
			debug_par_str(VIDEOFILE);

		} else if (param == "--out") {
			next_param("Incorrect use of --out");
			OUTPUTFILE = QString::fromUtf8(argv[i]);
			debug_par_str(OUTPUTFILE);
			OUTPUT_FORMAT = QFileInfo(OUTPUTFILE).suffix().toLower();
			debug_par_str(OUTPUT_FORMAT);

		} else if (param == "--config" || param == "-c") {
			i++;
			continue;

		} else if (param == "--time") {
			PS.setValue("mode", "time");
		} else if (param == "--line") {
			PS.setValue("mode", "line");
		} else if (param == "--full") {
			PS.setValue("size", "");
		} else if (param == "--screenshots") {
			next_param("Incorrect use of --screenshots");
			PS.setValue("layout", "1x" + QString::fromUtf8(argv[i]));
			PS.setValue("format", "png");
			PS.setValueBool("no_grid", true);
			PS.setValueBool("ft_hide", true);
			PS.setValueBool("concat", false);
			PS.setValue("size", "");
			PS.setValue("suffix", ".screenshot_");

		} else if (param == "--srt") {
			SubFormat = sub_srt;
			PrintDebug(param + ": SubFormat = srt");

		} else if (param == "--ass") {
			SubFormat = sub_ass;
			PrintDebug(param + ": SubFormat = ass");

		} else if (param == "--vtt") {
			SubFormat = sub_vtt;
			PrintDebug(param + ": SubFormat = vtt");

		} else if (param == "--lang") {
			next_param("Incorrect use of --lang");
			SUBLANGCODE = QString::fromUtf8(argv[i]);
			debug_par_str(SUBLANGCODE);

		} else if (param == "--jpg") {
			PS.setValue("format", "jpg");

		} else if (param == "--jpeg") {
			PS.setValue("format", "jpg");

		} else if (param == "--png") {
			PS.setValue("format", "png");

		} else if (param == "--apng") {
			PS.setValue("format", "apng");

		} else if (param == "--webp") {
			PS.setValue("format", "webp");

		} else if (param == "--webploop") {
			PS.setValue("format", "webploop");

		} else if (param == "--gif") {
			PS.setValue("format", "gif");

		} else if (param == "--giflow") {
			PS.setValue("format", "giflow");

		} else if (param == "--xvid") {
			PS.setValue("format", "xvid");

		} else if (param == "--x264") {
			PS.setValue("format", "x264");

		} else if (param == "--x265") {
			PS.setValue("format", "x265");

		} else if (param == "--tb_comment_append") {
			next_param("Incorrect use of --tb_add_comment");
			QString Comments = PS.Value("tb_comments");
			Comments += "%N%%#%" + QString::fromUtf8(argv[i]);
			PS.setValue("tb_comments", Comments);

		} else if (param == "--tb_comment_prepend") {
			next_param("Incorrect use of --tb_add_comment");
			QString Comments = PS.Value("tb_comments");
			Comments = "%#%" + QString::fromUtf8(argv[i]) + "%N%" + Comments;
			PS.setValue("tb_comments", Comments);

		} else if (param == "--tb_list_vars" || param == "--list_vars") {
			PrintVars = true;
			debug_par_bool(PrintVars);

		} else if (param == "--tb_transparent") {
			PS.setValue("tb_bg_color", "00000000");
			PS.setValue("grid_border_color", "00000000");
			PS.setValue("format", "png");

		} else if (param == "--tb_left") {
			PS.setValue("tb_title_pos", "up_left");
			PS.setValue("tb_font_pos", "middle_left");
			PS.setValue("tb_comments_pos", "down_left");
			PS.setValue("tb_logo_pos", "right");

		} else if (param == "--tb_right") {
			PS.setValue("tb_title_pos", "up_right");
			PS.setValue("tb_font_pos", "middle_right");
			PS.setValue("tb_comments_pos", "down_right");
			PS.setValue("tb_logo_pos", "left");

			//TODO: Custom options for subtitles

		} else if (param == "--green") {
			PS.setValue("sub_color", "00FF00");
		} else if (param == "--yellow") {
			PS.setValue("sub_color", "FFEA00");
		} else if (param == "--small") {
			PS.setValue("sub_size", "12");
		} else if (param == "--big") {
			PS.setValue("sub_size", "24");
		} else if (param == "--verybig") {
			PS.setValue("sub_size", "32");
		} else if (param == "--box") {
			PS.setValue("sub_border", "4");
		} else if (param == "--tbox") {
			PS.setValue("sub_border", "4");
			PS.setValue("sub_bc", "00000050");

		} else if (param == "--fisheye200") {
			PS.setValueBool("vr", true);
			PS.setValue("vr_in", "fisheye");
			PS.setValueInt("vr_id_fov", 0);
			PS.setValueInt("vr_ih_fov", 200);
			PS.setValueInt("vr_iv_fov", 200);
			PS.setValue("vr_in_stereo", "sbs");

		} else if (param == "--vr180") {
			PS.setValueBool("vr", true);
			PS.setValue("vr_in", "hequirect");
			PS.setValueInt("vr_id_fov", 0);
			PS.setValueInt("vr_ih_fov", 180);
			PS.setValueInt("vr_iv_fov", 180);
			PS.setValue("vr_in_stereo", "sbs");

		} else if (param == "--contact_sheet") {
			next_param("Incorrect use of --contact_sheet");
			PS.setValueBool("concat", false);
			PS.setValueBool("no_grid", false);
			PS.setValueBool("ft_hide", false);
			PS.setValue("tb_pos", "up");
			PS.setValue("format", "jpg");
			PS.setValue("layout", QString::fromUtf8(argv[i]));
			PS.setValue("suffix", ".contact_sheet");

		} else if (param == "--gif_images") {
			next_param("Incorrect use of --gif_images");
			PS.setValueBool("concat", true);
			PS.setValueBool("no_grid", true);
			PS.setValueBool("ft_hide", true);
			PS.setValue("format", "gif");
			PS.setValue("layout", "1x" + QString::fromUtf8(argv[i]));
			PS.setValueInt("video_fps", 1);
			PS.setValue("suffix", ".images");

		} else if (param == "--webp_images") {
			next_param("Incorrect use of --webp_images");
			PS.setValueBool("concat", true);
			PS.setValueBool("no_grid", true);
			PS.setValueBool("ft_hide", true);
			PS.setValue("format", "webploop");
			PS.setValue("layout", "1x" + QString::fromUtf8(argv[i]));
			PS.setValueInt("video_fps", 1);
			PS.setValue("suffix", ".images");

		} else if (param == "--gif_clip") {
			next_param("Incorrect use of --gif_clip");
			PS.setValueBool("concat", true);
			PS.setValueBool("no_grid", true);
			PS.setValueBool("ft_hide", true);
			PS.setValue("format", "gif");
			PS.setValue("layout", "1x" + QString::fromUtf8(argv[i]));
			PS.setValueInt("video_preview", 1000);
			PS.setValueInt("size", 320);
			PS.setValueInt("min_size", 0);
			PS.setValueInt("video_fps", 12);
			PS.setValue("suffix", ".clip");

		} else if (param == "--gif_singles") {
			next_param("Incorrect use of --gif_clip");
			PS.setValueBool("concat", false);
			PS.setValueBool("no_grid", true);
			PS.setValueBool("ft_hide", true);
			PS.setValue("format", "gif");
			PS.setValue("layout", "1x" + QString::fromUtf8(argv[i]));
			PS.setValueInt("video_preview", 1000);
			PS.setValueInt("size", 320);
			PS.setValueInt("min_size", 0);
			PS.setValueInt("video_fps", 12);
			PS.setValue("suffix", ".clip");

		} else if (param == "--webp_clip") {
			next_param("Incorrect use of --webp_clip");
			PS.setValueBool("concat", true);
			PS.setValueBool("no_grid", true);
			PS.setValueBool("ft_hide", true);
			PS.setValue("format", "webploop");
			PS.setValue("layout", "1x" + QString::fromUtf8(argv[i]));
			PS.setValueInt("video_preview", 1000);
			PS.setValueInt("video_fps", 12);
			PS.setValueInt("min_size", 0);
			PS.setValue("suffix", ".clip");

		} else if (param == "--mf") {
			next_param("Incorrect use of --mf");
			MANUALFRAMES.append(QString::fromUtf8(argv[i]));
			PrintDebug(param + ": Add manual frame = " + QString::fromUtf8(argv[i]));
		} else if (param == "--mfs") {
			next_param("Incorrect use of --mfs");
			MANUALFRAMES = QString::fromUtf8(argv[i]).split(",");
			PrintDebug(param + ": Add manual frames = " + QString::fromUtf8(argv[i]));
		} else if (param == "--gui_progress") {
			GUIProgress = true;

		} else if (param == "--save") {
			Save = true;

		} else if (param == "--save_all") {
			Save = true;
			SaveAll = true;
		} else if (param == "--var") {
			next_param("Incorrect use of --var");
			PrintDebug("\t--var " + QString::fromUtf8(argv[i]));
			PS.UserDefinedVar(QString::fromUtf8(argv[i]));
		} else if (param == "--var_stream") {
			next_param("Incorrect use of --var_stream");
			PrintDebug("\t--var_stream " + QString::fromUtf8(argv[i]));
			PS.UserDefinedVar(QString::fromUtf8(argv[i]), ParamsSettings::context_stream);
		} else if (param == "--var_stream_video") {
			next_param("Incorrect use of --var_stream_video");
			PrintDebug("\t--var_stream_video " + QString::fromUtf8(argv[i]));
			PS.UserDefinedVar(QString::fromUtf8(argv[i]), ParamsSettings::context_video);
		} else if (param == "--var_stream_audio") {
			next_param("Incorrect use of --var_stream_audio");
			PrintDebug("\t--var_stream_audio " + QString::fromUtf8(argv[i]));
			PS.UserDefinedVar(QString::fromUtf8(argv[i]), ParamsSettings::context_audio);
		} else if (param == "--var_stream_subtitle") {
			next_param("Incorrect use of --var_stream_subtitle");
			PrintDebug("\t--var_stream_subtitle " + QString::fromUtf8(argv[i]));
			PS.UserDefinedVar(QString::fromUtf8(argv[i]), ParamsSettings::context_subtitle);
		} else if (param == "--var_stream_attachment") {
			next_param("Incorrect use of --var_stream_attachment");
			PrintDebug("\t--var_stream_attachment " + QString::fromUtf8(argv[i]));
			PS.UserDefinedVar(QString::fromUtf8(argv[i]), ParamsSettings::context_attachment);

		} else if (param == "--overwrite") {//TODO: Delete
			PS.setValueBool("no_overwrite", false);

		} else if (param == "--tb_use_mediainfo") {
			PS.setValueBool("enable_mediainfo", true);
			PS.setValue("tb_custom_header",
						"Container: %B1%%format.mediainfo.Format%%B0% Duration: %B1%%format.mediainfo.Duration_String%%B0% Size: %B1%%format.mediainfo.FileSize_String% (%numsep(format.mediainfo.FileSize)% Bytes)%B0%$ Bitrate: %B1%$%format.mediainfo.OverallBitRate_String%$%B0%$");

			PS.setValue("tb_custom_video_stream",
						"Stream %index% Video: %B1%%mediainfo.Width%x%mediainfo.Height% %mediainfo.ColorSpace% %mediainfo.ChromaSubsampling% %mediainfo.BitDepth_String% %mediainfo.FrameRate% fps$ $%mediainfo.BitRate_String%$$ %mediainfo.Format%%B0%");
			PS.setValue("tb_custom_audio_stream",
						"Stream %index% Audio: %B1%$$%mediainfo.Language_String%$$$ $%mediainfo.Channels_String%$$$ $%mediainfo.SamplingRate_String%$$$ $%mediainfo.BitRate_String%$$ %mediainfo.Format%%B0%");
			PS.setValue("tb_custom_subtitle_stream",
						"Stream %index% Subtitle: %B1%$$%mediainfo.Title%$ - $$$%mediainfo.Language_String%$ $%mediainfo.Format%%B0%");
		} else if (PS.ProcessParameter(i, argc, argv)) {
			continue;
		} else {
			if (CONFIG.contains(param.remove(0, 2))) continue;

			QFileInfo FileInfo(QString::fromUtf8(argv[i]));
			QString Ext = FileInfo.suffix().toLower();
			if (SubtitleExtensions.contains(Ext)) {
				SUBFILE = QString::fromUtf8(argv[i]);
				PrintDebug("SUBFILE = " + SUBFILE);
				SubtitleEnable = true;
				PrintDebug("SubtitleEnable = " + QString((SubtitleEnable) ? "1" : "0"));
			} else if (VideoExtensions.contains(Ext)) {
				VIDEOFILE = QString::fromUtf8(argv[i]);
				PrintDebug("\t--video = " + VIDEOFILE);
			} else {
				PrintError("Option incorrect: " + QString::fromUtf8(argv[i]));
				return 10;
			}
		}
	}

	FFMPEG = PS.Value("ffmpeg");
	FFPROBE = PS.Value("ffprobe");
	QString TMP = PS.Value("tmp");

	int CurrentInstances = PS.Int("process");
	//ffmpeg instances
	QList<instance_t> Instances;
	for (int i = 0; i < CurrentInstances; i++) {
		Instances.append({new QProcess(), {{}, "", {}, ""}, false, ""});
	}

	if (SubtitleEnable && !QFile::exists(SUBFILE)) {
		PrintError("Input subtitle file missing");
		TERMINATE(1);
	}

	//TODO: Test if works
	if (PS.Bool("quiet")) Quiet = true;
	if (PS.Bool("debug")) Debug = true;
	if (PS.Bool("cmd")) Cmd = true;

	//Default font for drawtext in windows a file in fonts directory
	#ifdef Q_OS_WIN
	if (PS.Value("ft_font") == "DejaVu Sans Mono") {
		QString DefaultFont = QCoreApplication::applicationDirPath() + "/Fonts/DejaVuSansMono.ttf";
		if (QFile::exists(DefaultFont)) {
			PS.setValue("ft_font", DefaultFont);
		}
	}
	#endif

	QString FT_FONT = PS.Value("ft_font");

	//Check if ffmpeg suports font names in drawtext
	QString FFMPEG_COMP_TMP = ReadProgram(FFMPEG, {"-hide_banner", "-loglevel", "error", "-buildconf"});
	if (FFMPEG_COMP_TMP == "") {
		PrintError("Error with ffmpeg check if the configuration is correct");
		TERMINATE(9);
	}

	FFMPEG_COMPILE = FFMPEG_COMP_TMP.split("\n");

	for (int i = 0; i < FFMPEG_COMPILE.count(); i++) {
		FFMPEG_COMPILE.replace(i, FFMPEG_COMPILE.at(i).trimmed());
	}

	#ifdef Q_OS_WIN
	if (FFMPEG_COMPILE.contains("--enable-fontconfig")) ffmpeg_has_libfontconfig = true;
	#else
	if (FFMPEG_COMPILE.contains("--enable-libfontconfig")) ffmpeg_has_libfontconfig = true;
	#endif

	bool FRAME_TIMESTAMP_FONT_IS_FILE = QFile::exists(FT_FONT);
	#ifdef Q_OS_WIN
	if (!FRAME_TIMESTAMP_FONT_IS_FILE) {
		FRAME_TIMESTAMP_FONT_IS_FILE = QFile::exists(a.applicationDirPath() + QDir::separator() + FT_FONT);
		if (FRAME_TIMESTAMP_FONT_IS_FILE) {
			FT_FONT = a.applicationDirPath() + QDir::separator() + FT_FONT;
		}
	}
	#else
	if (!ffmpeg_has_libfontconfig && !FRAME_TIMESTAMP_FONT_IS_FILE) {
		FT_FONT = ReadProgram("fc-match", {"-f", "'%{file}'", FT_FONT});
		FRAME_TIMESTAMP_FONT_IS_FILE = true;
		if (FT_FONT == "") {
			qFatal("Error reading font file, configure ft_font");
			return 10;
		}
	}
	#endif

	if (FRAME_TIMESTAMP_FONT_IS_FILE) FT_FONT = QDir::toNativeSeparators(FT_FONT);

	DEBUGVAR(FT_FONT);

	if (!ffmpeg_has_libfontconfig && !FRAME_TIMESTAMP_FONT_IS_FILE) {
		PrintError("The ft_font config option needs to be a valid file");
		TERMINATE(10);
	}

	//Generate tmpdir from options
	if (TMP == "")
		TMP = QDir::tempPath() + QDir::separator() + a.applicationName() + "_XXXXXX";
	else
		TMP = TMP + QDir::separator() + a.applicationName() + "_XXXXXX";

	QTemporaryDir TmpDir(TMP);
	if (TmpDir.isValid()) {
		TMP = QDir::toNativeSeparators(TmpDir.path());
		if (Debug) TmpDir.setAutoRemove(false);
	} else {
		PrintError("Error creting tmp directory");
		return 7;
	}

	TMP_TEXT = TMP + QDir::separator() + "head_subtitle.ass";
	TMP_TEXT_IMG = TMP + QDir::separator() + "text.png";
	TMP_IMAGES = TMP + QDir::separator() + "images";
	TMP_BACKGROUND = TMP + QDir::separator() + "background.png";
	TMP_TRANSPARENT_SHEET = TMP + QDir::separator() + "transparent.png";

	DEBUGVAR(TMP);
	DEBUGVAR(TMP_TEXT);
	DEBUGVAR(TMP_TEXT_IMG);
	DEBUGVAR(TMP_IMAGES);

	DEBUGVAR(SUBFILE);

	if (SubtitleEnable) {
		//Generate all varibles from subtitle filename
		QFileInfo SubFileInfo(SUBFILE);
		SUBDIR = QDir::toNativeSeparators(SubFileInfo.absolutePath());
		SUBFILEMAME = SubFileInfo.fileName();
		SUBNAME = SubFileInfo.completeBaseName();
		SUBEXT = SubFileInfo.suffix();
		if (SubFormat == sub_none) SubFormat = String2SubFormat(SUBEXT);
		SubtitleFileSize = SubFileInfo.size();

		DEBUGVAR(SUBDIR);
		DEBUGVAR(SUBFILEMAME);
		DEBUGVAR(SUBNAME);
		DEBUGVAR(SUBEXT);
		DEBUGVAR(SubtitleFileSize);

		//Generate text input format for header
		switch (SubFormat) {
			case sub_srt:
				SUBTITLE_FORMAT = "SubRip";
				DebugVarFunt("SUBFORMAT", "srt");
				break;
			case sub_ass:
				SUBTITLE_FORMAT = "Advanced SubStation Alpha";
				DebugVarFunt("SUBFORMAT", "ass");
				break;
			case sub_vtt:
				SUBTITLE_FORMAT = "WebVTT";
				DebugVarFunt("SUBFORMAT", "vtt");
				break;
			case sub_none:
				PrintError("Imposible determine subtitle format, use --srt or --ass or --vtt");
				return 2;
		}

		SUBLANG = "";
		SUBNAME_LANG = "";
		if (SUBLANGCODE == "") {
			SUBLANGCODE = QFileInfo(SUBNAME).suffix();
			if (SUBLANGCODE == SUBNAME) { SUBLANGCODE = ""; }
			if (SUBLANGCODE != "") {
				QLocale locale(SUBLANGCODE);
				SUBLANG = QLocale::languageToString(locale.language());
				if (SUBLANG == "" || SUBLANG == "C") {
					SUBLANGCODE = "";
					SUBLANG = "";
				} else {
					SUBNAME_LANG = QFileInfo(SUBNAME).completeBaseName();
				}
			}
		}
		DEBUGVAR(SUBLANGCODE);
		DEBUGVAR(SUBNAME_LANG);

		if (SUBLANGCODE != "") {
			QLocale locale(SUBLANGCODE);
			SUBLANG = QLocale::languageToString(locale.language());
			DEBUGVAR(SUBLANG);
		}

		PrintInfo("");
		PrintInfo("Using subtitle file: " + SUBFILE);
		PrintInfo("\tSubtitle format: " + SUBTITLE_FORMAT);
		if (SUBLANG != "") PrintInfo("\tSubtitle language: " + SUBLANG);
		PrintInfo("");
	}

	if (VIDEOFILE == "") {
		for (int i = 0; i < VideoExtensions.count(); i++) {
			VIDEOFILE = check_video_file(SUBDIR, SUBNAME, VideoExtensions.at(i));
			if (VIDEOFILE != "") break;
			if (SUBNAME_LANG != "") {
				VIDEOFILE = check_video_file(SUBDIR, SUBNAME_LANG, VideoExtensions.at(i));
				if (VIDEOFILE != "") break;
			}
		}
	}

	VIDEOFILE = QDir::toNativeSeparators(VIDEOFILE);

	TextSustitution TS;
	#ifdef Q_OS_WIN
	TS.SetReplaceDir(QCoreApplication::applicationDirPath());
	#else
	TS.SetReplaceDir(QStandardPaths::locate(QStandardPaths::ConfigLocation, "subtitles_contact_sheet",
											QStandardPaths::LocateDirectory));
	#endif

	QFileInfo VideoFileInfo(VIDEOFILE);
	VIDEOFILENAME = VideoFileInfo.fileName();
	TS.Insert("file.filename", VIDEOFILENAME);
	TS.Insert("file.size", QString::number(VideoFileInfo.size()));
	TS.Insert("file.basename", VideoFileInfo.completeBaseName());
	TS.Insert("file.extension", VideoFileInfo.suffix());
	TS.Insert("file.dir", QDir::toNativeSeparators(VideoFileInfo.absolutePath()));
	TS.Insert("file.absolute", QDir::toNativeSeparators(VideoFileInfo.absoluteFilePath()));

	if (SubtitleEnable) {
		TS.Insert("title", SUBFILEMAME);
		TS.Insert("in.filename", SUBFILEMAME);
		TS.Insert("in.basename", SUBNAME);
		TS.Insert("in.extension", SUBEXT);
		TS.Insert("in.dir", SUBDIR);
	} else {
		TS.Insert("title", VIDEOFILENAME);
		TS.Insert("in.filename", VIDEOFILENAME);
		TS.Insert("in.basename", VideoFileInfo.completeBaseName());
		TS.Insert("in.extension", VideoFileInfo.suffix());
		TS.Insert("in.dir", QDir::toNativeSeparators(VideoFileInfo.absolutePath()));
	}

	DEBUGVAR(VIDEOFILE);

	if (!QFile::exists(VIDEOFILE)) {
		PrintError("Input video file missing");
		TERMINATE(3);
	}

	QString OUTPUT_DIR = QDir::toNativeSeparators(PS.Value("out_dir"));
	if (OUTPUT_DIR == "" && PS.Value("out_relative_dir") != "") {
		QString Relative = PS.Value("out_relative_dir");
		QString Parent;
		if (SubtitleEnable)
			Parent = SUBDIR;
		else
			Parent = QFileInfo(VIDEOFILE).absolutePath();

		OUTPUT_DIR = QDir::toNativeSeparators(Parent + QDir::separator() + Relative);
		QDir ParentDir(Parent);
		if (!ParentDir.exists(Relative)) ParentDir.mkdir(Relative);
	}

	//TODO: Limit this to permited formats only
	if (OUTPUT_FORMAT == "") OUTPUT_FORMAT = PS.Value("format");
	if (OUTPUT_FORMAT == "mkv") OUTPUT_FORMAT = "x264";

	if (PS.NotEmpty("format_options")) {
		FORMAT_OPTIONS = PS.Value("format_options").split(" ");
	} else {
		format_definition_t CurrentFormatDefinition = OUTPUT_AVAILABLE_FORMATS.value(OUTPUT_FORMAT, {"", "", "", false});
		if (CurrentFormatDefinition.Ext != "") {
			OUTPUT_FORMAT = CurrentFormatDefinition.Ext;
			INDEPENDENT_COMMAND = CurrentFormatDefinition.IndependentCommand;
			if (CurrentFormatDefinition.Options != "") {
				FORMAT_OPTIONS = CurrentFormatDefinition.Options.split(" ");
				if (CurrentFormatDefinition.DefaultQuality != "") {
					int index = FORMAT_OPTIONS.indexOf("{quality}");
					if (PS.Int("output_quality") != -1) {
						FORMAT_OPTIONS.replace(index, PS.Value("output_quality"));
					} else {
						FORMAT_OPTIONS.replace(index, CurrentFormatDefinition.DefaultQuality);
					}
				}
			} else {
				FORMAT_OPTIONS.clear();
			}
		} else {
			OUTPUT_FORMAT = "";
		}
	}

	DEBUGVAR(OUTPUT_FORMAT);

	QString OUTPUTNAME = "";
	if (OUTPUTFILE == "") {
		OUTPUTNAME = TS.ProcessCMD("", PS.Value("out_name"));
		if (OUTPUT_DIR == "") {
			if (SubtitleEnable)
				OUTPUT_DIR = SUBDIR;
			else
				OUTPUT_DIR = QFileInfo(VIDEOFILE).absolutePath();
		}
		OUTPUTFILE = OUTPUT_DIR + QDir::separator() + OUTPUTNAME + PS.Value("suffix") + "." + OUTPUT_FORMAT;
	}

	OUTPUTFILE = QDir::toNativeSeparators(OUTPUTFILE);
	DEBUGVAR(OUTPUTFILE);
	TS.Insert("out.file", OUTPUTFILE);
	TS.Insert("out.basename", OUTPUTNAME + PS.Value("suffix"));
	TS.Insert("out.extension", OUTPUT_FORMAT);
	TS.Insert("out.dir", OUTPUT_DIR);
	TS.Insert("out.absolute", QDir::toNativeSeparators(OUTPUT_DIR + QDir::separator() + OUTPUTNAME + PS.Value("suffix") +
			  "." + OUTPUT_FORMAT));

	PrintInfo("Using output file: " + OUTPUTFILE);
	PrintInfo("\tOutput format: " + OUTPUT_FORMAT);
	PrintInfo("");

	if (!ffprobe(VIDEOFILE, VIDEO_JSON)) {
		PrintError("Error reading video file metadata");
		TERMINATE(7);
	}

	VideoMetadata = QJsonDocument::fromJson(VIDEO_JSON.toUtf8(), &JsonError);
	if (!VideoMetadata.isObject()) {
		PrintError("Error reading video file metadata");
		TERMINATE(7);
	}

	JsonRoot = VideoMetadata.object();
	VideoStreams = JsonRoot.value("streams").toArray();

	TS.LoadVars(JsonRoot);

	int FirstVideo = -1;
	int FirstAudio = -1;
	int VideoCount = 0;
	int AudioCount = 0;
	int SubCount = 0 ;
	int AttachCount = 0;

	for (int i = 0; i < VideoStreams.count(); i++) {
		QString Bitrate = "";
		QString Context = "streams." + QString::number(i) + ".";
		QString BitrateVar = Context + "bit_rate";
		Bitrate = TS.Value(BitrateVar, "");
		if (Bitrate == "N/A" || Bitrate == "")
			Bitrate = TS.Value(Context + "tags.BPS-eng", "");
		if (Bitrate == "N/A" || Bitrate == "")
			Bitrate = TS.Value(Context + "tags.BPS", "");

		if (Bitrate == "" && TS.Contains(BitrateVar))
			TS.Remove(BitrateVar);

		if (Bitrate != "") TS.Update(BitrateVar, Bitrate);

		QString StreamType = TS.Value(Context + "codec_type");

		if (TS.Value(Context + "tags.filename") != "") {
			TS.Update(Context + "codec_type", "attachment");
			StreamType = "attachment";
		}

		if (StreamType == "audio" &&
			TS.Value(Context + "channel_layout") == "unknown") {
			TS.Update(Context + "channel_layout",
					  TS.Value(Context + "channels") + "Channels");
		}

		if (StreamType == "video") {
			if (FirstVideo < 0) FirstVideo = i;
			VideoCount++;
		} else if (StreamType == "audio") {
			if (FirstAudio < 0) FirstAudio = i;
			AudioCount++;
		} else if (StreamType == "subtitle") {
			SubCount++;
		} else if (StreamType == "attachment") {
			AttachCount++;
		}
	}

	if (FirstVideo > -1) TS.LoadVars(VideoStreams[FirstVideo].toObject(), "streams.video.first");
	if (FirstAudio > -1) TS.LoadVars(VideoStreams[FirstAudio].toObject(), "streams.audio.first");
	if (VideoCount > 0) TS.Insert("streams.video.count", QString::number(VideoCount));
	if (AudioCount > 0) TS.Insert("streams.audio.count", QString::number(AudioCount));
	if (SubCount > 0) TS.Insert("streams.subtitles.count", QString::number(SubCount));
	if (AttachCount > 0) TS.Insert("streams.attachment.count", QString::number(AttachCount));
	if (VideoCount > 1 || AudioCount > 1 || SubCount > 1 ||
		AttachCount > 1) TS.Insert("streams.multiple_same_type", "1");

	if (TS.Value("chapters.count", "0") == "0") TS.Remove("chapters.count");

	if (PS.NotEmpty("mediainfo") && PS.Bool("enable_mediainfo")) {
		QString mediainfo_json = ReadProgram(PS.Value("mediainfo"), {"-f", "--Output=JSON", VIDEOFILE});
		if (mediainfo_json != "") {
			QJsonDocument MediainfoMetadata = QJsonDocument::fromJson(mediainfo_json.toUtf8(), &JsonError);
			if (MediainfoMetadata.isObject()) {
				QJsonObject rootobj = MediainfoMetadata.object();
				QJsonObject mediaobj = rootobj.value("media").toObject();
				QJsonArray tracksobj = mediaobj.value("track").toArray();
				bool video = true;
				bool audio = true;
				for (int i = 0; i < tracksobj.count(); ++i) {
					QJsonObject track = tracksobj.at(i).toObject();
					if (track.value("@type").toString() == "General") {
						TS.LoadVars(track, "format.mediainfo");
						continue;
					}
					if (!track.value("StreamOrder").isUndefined()) {
						TS.LoadVars(track, "streams." + track.value("StreamOrder").toString() + ".mediainfo");
						if (video && track.value("@type").toString() == "Video") {
							TS.LoadVars(track, "streams.video.first.mediainfo");
							video = false;
						}
						if (audio && track.value("@type").toString() == "Audio") {
							TS.LoadVars(track, "streams.audio.first.mediainfo");
							audio = false;
						}
					}
				}
			}
		}
	}

	QString FileNameUpper = VIDEOFILENAME.toUpper();
	if (FileNameUpper.contains("MKX200")) {
		TS.Insert("vr.format", "fisheye");
		TS.Insert("vr.mode", "sbs");
		TS.Insert("vr.fov", "200");
	} else if (FileNameUpper.contains("FISHEYE190")) {
		TS.Insert("vr.format", "fisheye");
		TS.Insert("vr.mode", "sbs");
		TS.Insert("vr.fov", "190");
	} else if (FileNameUpper.contains("360_SBS") || FileNameUpper.contains("SBS_360")) {
		TS.Insert("vr.format", "equirect");
		TS.Insert("vr.mode", "sbs");
		TS.Insert("vr.fov", "360");
	} else if (FileNameUpper.contains("TB_360") || FileNameUpper.contains("360_TB") || FileNameUpper.contains("360_OI") ||
			   FileNameUpper.contains("OI_360")) {
		TS.Insert("vr.format", "equirect");
		TS.Insert("vr.mode", "tb");
		TS.Insert("vr.fov", "360");
	} else if (FileNameUpper.contains("MONO_360") || FileNameUpper.contains("360_MONO") ||
			   FileNameUpper.contains("360MONO") || FileNameUpper.contains("MONO360")) {
		TS.Insert("vr.format", "equirect");
		TS.Insert("vr.mode", "2d");
		TS.Insert("vr.fov", "360");
	} else if (FileNameUpper.contains("TB_180") || FileNameUpper.contains("180_TB") || FileNameUpper.contains("180TB") ||
			   FileNameUpper.contains("TB180") || FileNameUpper.contains("180_3DV") || FileNameUpper.contains("3DV_180")) {
		TS.Insert("vr.format", "equirect");
		TS.Insert("vr.mode", "tb");
		TS.Insert("vr.fov", "180");
	} else if (FileNameUpper.contains("LR_180") || FileNameUpper.contains("180_LR") || FileNameUpper.contains("180LR") ||
			   FileNameUpper.contains("LR180") || FileNameUpper.contains("180_3DH") || FileNameUpper.contains("3DH_180")) {
		TS.Insert("vr.format", "equirect");
		TS.Insert("vr.mode", "sbs");
		TS.Insert("vr.fov", "180");
	} else if (FileNameUpper.contains("180X180")) {
		TS.Insert("vr.format", "equirect");
		if (FileNameUpper.contains("3DV") || FileNameUpper.contains("TB"))
			TS.Insert("vr.mode", "tb");
		else
			TS.Insert("vr.mode", "sbs");
		TS.Insert("vr.fov", "180");
	} else if (FileNameUpper.contains("3DV")) {
		TS.Insert("vr.format", "equirect");
		TS.Insert("vr.mode", "tb");
		TS.Insert("vr.fov", "360");
	} else if (FileNameUpper.contains("3DH")) {
		TS.Insert("vr.format", "equirect");
		TS.Insert("vr.mode", "sbs");
		TS.Insert("vr.fov", "360");
	}

	if (TS.Value("vr.format", "") != "")
		TS.Insert("vr.file", "1");
	else
		TS.Insert("vr.file", "0");


	DURATION = TS.Value("format.duration");

	DEBUGVAR(DURATION);
	Duration = DURATION.toFloat() * 1000;
	DEBUGVAR(Duration);
	DURATIONFORMATED = QTime::fromMSecsSinceStartOfDay(Duration).toString("hh:mm:ss");
	DEBUGVAR(DURATIONFORMATED);

	PrintInfo("Using video file: " + VIDEOFILE);
	PrintInfo("\tVideo file duration: " + DURATIONFORMATED);
	PrintInfo("");

	DEBUGVAR(Duration);

	if (SubtitleEnable) {
		QStringList SubtitleContents;
		QFile SubtitleFile(SUBFILE);

		if (!SubtitleFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
			PrintError("Error reading subtitle file");
			TERMINATE(4);
		}

		while (!SubtitleFile.atEnd()) {
			QByteArray line = SubtitleFile.readLine();
			SubtitleContents.append(line);
		}

		PrintInfo("\nProcessing subtitle file: " + SUBFILEMAME + "\n");

		Timestamps = ProcessSubtitles(SubFormat, SubtitleContents, Totals);

		TS.Insert("subtitle.filename", SUBFILEMAME);
		TS.Insert("subtitle.extension", SUBEXT);
		TS.Insert("subtitle.format", SUBTITLE_FORMAT);
		TS.Insert("subtitle.size", QString::number(SubtitleFileSize));
		TS.Insert("subtitle.language", SUBLANG);
		TS.Insert("subtitle.langcode", SUBLANGCODE);
		TS.Insert("subtitle.lines", QString::number(Totals.Sub));
		TS.Insert("subtitle.words", QString::number(Totals.Words));
		TS.Insert("subtitle.empty_lines", QString::number(Totals.EmptyLines));
		TS.Insert("subtitle.total_length", QString::number(Totals.TextLen));
		TS.Insert("subtitle.avg_lenght", QString::number(Totals.TextLen / Totals.Sub));
		TS.Insert("subtitle.total_duration", QString::number((double)Totals.Len / 1000));
		TS.Insert("subtitle.avg_duration_ms", QString::number(Totals.Len / Totals.Sub));

		TOTALLENFORMATED = QTime::fromMSecsSinceStartOfDay(Totals.Len).toString("hh:mm:ss");

		PrintInfo("Subtitle file statics: ");
		PrintInfo("\tTotals");
		PrintInfo("\t\tDuration: " + TOTALLENFORMATED);
		PrintInfo("\t\tTextLengt: " + QString::number(Totals.TextLen) + " Characters");
		PrintInfo("\t\tWords: " + QString::number(Totals.Words));
		PrintInfo("\t\tLines: " + QString::number(Totals.Sub));
		PrintInfo("\t\tEmpty Lines: " + QString::number(Totals.EmptyLines));
		PrintInfo("\tAverages");
		PrintInfo("\t\tDuration: " + QString::number(Totals.Len / Totals.Sub) + "ms");
		PrintInfo("\t\tTextLengt: " + QString::number(Totals.TextLen / Totals.Sub));
		PrintInfo("\t\tWords: " + QString::number(Totals.Words / Totals.Sub));
		PrintInfo("");
	}

	QStringList ListParams = PS.Params();
	ListParams.sort();
	for (int i = 0; i < ListParams.size(); ++i) {
		QString Param = ListParams.at(i);
		QString Value = PS.Value(Param);
		PrintDebug("---> " + Param + " = " + Value);
		QString FUNCTION = PS.Function(Param);

		if (PS.IsUserDefined(Param)) {
			ParamsSettings::context_type Context;
			QString VarName;
			if (!PS.GetVarAndContext(Param, VarName, Context)) continue;

			if (Context == ParamsSettings::context_global) {
				if (FUNCTION != "")
					Value = TS.DetectAndEjecuteFunction(FUNCTION, "");
				PrintDebug("\t" + VarName + " = " + Value);
				TS.Insert(VarName, Value, false);
				continue;
			}

			for (int i = 0; i < TS.Value("streams.count").toInt(); i++) {
				QString CONTEXT = "streams." + QString::number(i) + ".";
				QString STREAM_TYPE = TS.Value(CONTEXT + "codec_type");
				QString LocalVarName = VarName;
				switch (Context) {
					case ParamsSettings::context_stream:
						break;
					case ParamsSettings::context_video:
						if (STREAM_TYPE != "video") continue;
						break;
					case ParamsSettings::context_audio:
						if (STREAM_TYPE != "audio") continue;
						break;
					case ParamsSettings::context_subtitle:
						if (STREAM_TYPE != "subtitle") continue;
						break;
					case ParamsSettings::context_attachment:
						if (STREAM_TYPE != "attachment") continue;
						break;
					default:
						continue;
				}

				if (LocalVarName.startsWith("context.")) LocalVarName.replace("context.", CONTEXT);

				if (!TS.Contains(LocalVarName)) TS.Insert(LocalVarName, "", false);
				if (FUNCTION != "")
					Value = TS.DetectAndEjecuteFunction(FUNCTION, CONTEXT);
				PrintDebug("\t" + LocalVarName + " = " + Value);
				TS.Update(LocalVarName, Value, false);
			}
		} else {
			if (FUNCTION == "") continue;
			Value = TS.DetectAndEjecuteFunction(FUNCTION, "");
			PS.setValue(Param, Value, false);
		}
	}

	VR = PS.Bool("vr");
	VR_IN = PS.Value("vr_in");
	VR_IN_STEREO = PS.Value("vr_in_stereo");
	VR_IH_FOV = PS.Value("vr_ih_fov").toInt();
	VR_IV_FOV = PS.Value("vr_iv_fov").toInt();

	if (PS.Bool("vr_auto_enable")) {
		if (TS.Value("vr.file") == "1") {
			PrintInfo("VR Video detected, Format: " + TS.Value("vr.format") + ", Mode: " + TS.Value("vr.mode") + ", FOV:" +
					  TS.Value("vr.fov") + "\n");
			VR = true;
			VR_IN = TS.Value("vr.format");
			VR_IN_STEREO = TS.Value("vr.mode");
			VR_IH_FOV = TS.Value("vr.fov").toInt();
			VR_IV_FOV = TS.Value("vr.fov").toInt();
			if (VR_IH_FOV == 360) VR_IV_FOV = 180;
			if (VR_IN == "equirect" && VR_IH_FOV == 180)
				VR_IN = "hequirect";
		}
	}

	if (PrintVars || Debug) {
		PrintInfo("");
		PrintInfo("Variables available in text substitution");
		TS.PrintAll();
		if (PrintVars) {
			TERMINATE(0);
		}
	}

	QString SIZE = PS.Value("size");
	if (SIZE != "") {
		Size = PS.Int("size");
		MinSize	= PS.Int("min_size");

		for (int i = 0; i < VideoStreams.count(); i++) {
			CurrentStream = VideoStreams.at(i).toObject();
			if (CurrentStream.value("codec_type").toString() == "video") {
				VideoWidth = CurrentStream.value("width").toInt();
				PrintDebug("VideoWidth: " + QString::number(VideoWidth));
				VideoHeight = CurrentStream.value("height").toInt();
				PrintDebug("VideoHeight: " + QString::number(VideoHeight));
				break;
			}
		}

		if (VideoWidth > 0 && VideoHeight > 0) {
			VerticalVideo = (VideoHeight > VideoWidth);
			AspectRatio = (double)VideoWidth / (double)VideoHeight;
			if (VerticalVideo) {
				NewVideoHeight = Size;
				NewVideoWidth = qRound((Size / 2) * AspectRatio) * 2;
				if (NewVideoWidth < MinSize) {
					NewVideoWidth = MinSize;
					NewVideoHeight = qRound((MinSize / 2) / AspectRatio) * 2;
					SIZE = QString::number(NewVideoHeight);
					DEBUGVAR(SIZE);
				}
			} else {
				NewVideoWidth = Size;
				NewVideoHeight = qRound((Size / 2) / AspectRatio) * 2;
				if (NewVideoHeight < MinSize) {
					NewVideoHeight = MinSize;
					NewVideoWidth = qRound((MinSize / 2) * AspectRatio) * 2;
					SIZE = QString::number(NewVideoWidth);
					DEBUGVAR(SIZE);
				}
			}

			PrintDebug("NewVideoWidth: " + QString::number(NewVideoWidth));
			PrintDebug("NewVideoHeight: " + QString::number(NewVideoHeight));
		}
	}

	QString LAYOUT = PS.Value("layout").toLower();
	LAYOUT_X = LAYOUT.split("x").first();
	LAYOUT_Y = LAYOUT.split("x").last();
	DEBUGVAR(LAYOUT_X);
	DEBUGVAR(LAYOUT_Y);

	LayoutX = LAYOUT_X.toUInt();
	LayoutY = LAYOUT_Y.toUInt();
	if (LayoutX == 0) { LayoutX = 1; }
	if (LayoutY == 0) { LayoutY = 1; }
	DEBUGVAR(LayoutX);
	DEBUGVAR(LayoutY);

	NumberOfFrames = LayoutX * LayoutY;
	DEBUGVAR(NumberOfFrames);

	PrintInfo("Grid " + LAYOUT + ", Screenshots: " + QString::number(NumberOfFrames));

	Mode = String2Mode(PS.Value("mode"));
	if (!SubtitleEnable && (Mode == mode_time || Mode == mode_line)) Mode = mode_video_time;
	int VideoPreview = PS.Int("video_preview");
	bool NoGrid = PS.Bool("no_grid");
	bool Concat = PS.Bool("concat");
	if (Concat) NoGrid = true;
	TextBlockPosition = String2TextBlockPotion(PS.Value("tb"));
	if (NoGrid || VideoPreview > 0) TextBlockPosition = text_hidden;

	if (VideoPreview != 0) {
		TMP_FRAME_FORMAT = "mkv";
		TMP_FRAME_FORMAT_OPTIONS << "-c:v" << "libxvid" << "-qscale:v" << "3";
	} else {
		TMP_FRAME_FORMAT = "png";
	}

	if (GUIProgress) {
		CurrentStep = 0;
		Steps = NumberOfFrames;
		if (!(NoGrid && !Concat)) Steps++;
		if (TextBlockPosition != text_hidden) Steps += 2;
		if (PS.NotEmpty("bg_image") || PS.Value("bg_gradient") != "none") Steps++;
	}

	QString TB_TITLE = PS.Value("tb_title");
	QString TB_BG_COLOR = PS.Value("tb_bg_color");
	TextBlockBackgroundHAling = String2HAling(PS.Value("tb_bg_image_halign"));
	TextBlockBackgroundVAling = String2VAling(PS.Value("tb_bg_image_valign"));

	int GridBorder = PS.Int("grid_border");
	QString GRID_BORDER_COLOR = PS.Value("grid_border_color");
	BackgroundHAling = String2HAling(PS.Value("bg_image_halign"));
	BackgroundVAling = String2VAling(PS.Value("bg_image_valign"));

	FrameTimePosition = String2FrameTimePosition(PS.Value("ft_pos"));
	QString FT_FORMAT = PS.Value("ft_format");

	int StartTime = StringTime2ms(PS.Value("start"));
	int EndTime = StringTime2ms(PS.Value("end"));
	if (EndTime == -1) {
		EndTime = StringTime2ms(PS.Value("end").trimmed().remove(0, 1));
		EndTime = -EndTime;
	}
	int StartLine = PS.Int("start_line");
	int EndLine = PS.Int("end_line");

	switch (Mode) {
		case mode_time:
		case mode_video_time:
		case mode_video_end:
			if (StartTime > 0) Duration = Duration - StartTime;
			if (EndTime > 0) Duration = EndTime - StartTime;
			if (EndTime < 0) Duration = Duration + EndTime;
			if (Duration < 0) {
				PrintDebug("--start or --end too high for this file, setting them to 0.");
				StartTime = 0;
				EndTime = 0;
				Duration = DURATION.toFloat() * 1000;
			}
			DEBUGVAR(StartTime);
			DEBUGVAR(EndTime);
			PrintInfo("Screenshots range from " +
					  QTime::fromMSecsSinceStartOfDay(StartTime).toString("hh:mm:ss") + " to " +
					  QTime::fromMSecsSinceStartOfDay(StartTime + Duration).toString("hh:mm:ss"));
			break;
		case mode_line:
			DEBUGVAR(StartLine);
			DEBUGVAR(EndLine);
			PrintInfo("Screenshots range from line " + QString::number(StartLine) + " to " + QString::number(EndLine));
			break;
	}

	PrintInfo("");


	if (PS.Bool("rename") && PS.NotEmpty("rename_dest")) {
		QString Dest = QDir::toNativeSeparators(TS.ProcessCMD("", PS.Value("rename_dest")));
		QFileInfo DestInfo(Dest);
		QDir Dir(DestInfo.path());
		if (DestInfo.path() != "" && !Dir.exists()) {
			if (PS.Bool("rename_mkdir")) {
				PrintInfo("Rename creating path: \"" + DestInfo.path() + "\"");
				if (QDir().mkpath(DestInfo.path())) {
					PrintInfo("Done");
				} else {
					PrintError("Error");
					TERMINATE(20);
				}
			} else {
				PrintError("Error, parent output directory not exist");
				TERMINATE(21);
			}
		}
		if (!PS.Bool("rename_overwrite") && QFile::exists(Dest)) {
			PrintError("Error, the destination file \"" + Dest + "\" anready exists");
			TERMINATE(22);
		}

		bool Result = false;
		if (PS.Value("rename_type") == "move") {
			PrintInfo("Moving file \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
			Result = QFile::rename(VIDEOFILE, Dest);
		} else if (PS.Value("rename_type") == "copy") {
			PrintInfo("Copying file \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
			Result = QFile::copy(VIDEOFILE, Dest);
		} else if (PS.Value("rename_type") == "symlink") {
			PrintInfo("Creating a symlink from \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
			Result = CreateSymlink(VIDEOFILE, Dest);
		} else if (PS.Value("rename_type") == "hardlink") {
			PrintInfo("Creating a hardlink from \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
			Result = CreateHardlink(VIDEOFILE, Dest);
		} else if (PS.Value("rename_type") == "shortcut") {
			PrintInfo("Creating a shortcut from \"" + VIDEOFILE + "\" to \"" + Dest + ".lnk\"");
			Result = CreateShortcutU(VIDEOFILE, Dest, PS.Bool("rename_overwrite"));
		} else if (PS.Value("rename_type") == "link") {
			PrintInfo("Attempt to create a hardlink from \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
			Result = CreateHardlink(VIDEOFILE, Dest);
			if (!Result) {
				#ifdef Q_OS_WIN
				PrintInfo("Attempt to create a symlink from \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
				#else
				PrintInfo("Creating a symlink from \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
				#endif
				Result = CreateSymlink(VIDEOFILE, Dest);
			}
			#ifdef Q_OS_WIN
			if (!Result) {
				PrintInfo("Creating a shortcut from \"" + VIDEOFILE + "\" to \"" + Dest + ".lnk\"");
				Result = CreateShortcutU(VIDEOFILE, Dest, PS.Bool("rename_overwrite"));
			}
			#endif
		} else if (PS.Value("rename_type") == "link") {
			PrintInfo("Attempt to create a hardlink from \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
			Result = CreateHardlink(VIDEOFILE, Dest);
			if (!Result) {
				PrintInfo("Copying file \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
				Result = QFile::copy(VIDEOFILE, Dest);
			}
		} else {
			PrintInfo("Rename simulation: \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
		}
		if (Result) {
			PrintInfo("Done");
			TERMINATE(0);
		} else {
			PrintError("Error");
			TERMINATE(23);
		}

	}

	if (PS.NotEmpty("template") && QFile::exists(PS.Value("template"))) {
		QString Outfile = QDir::toNativeSeparators(OUTPUT_DIR + QDir::separator() + OUTPUTNAME + "." +
						  PS.Value("template_ext"));
		PrintInfo("Generating template from " + PS.Value("template") + " to " + Outfile);
		QString Content = TS.ProcessTemplate(PS.Value("template"));
		WriteFile(Outfile, Content);
		TERMINATE(0);
	}

	ExecuteCustomCommand(PS.Value("exec_before"), &TS);

	if (PS.Bool("no_overwrite") && QFile::exists(OUTPUTFILE)) {
		ExecuteCustomCommand(PS.Value("exec_after"), &TS);
		TERMINATE(0);
	}

	if (Mode == mode_time) {
		PrintInfo("Selecting frames, Time mode:");
		uint64_t MsBetweenFrames = Duration / NumberOfFrames;
		DEBUGVAR(MsBetweenFrames);
		DEBUGVAR(PS.Int("min"));
		int CurrentTimestamp = 0;

		for (uint i = 0; i < NumberOfFrames; i++) {
			uint64_t SubtitleStartTime = StartTime + (i * MsBetweenFrames);
			uint64_t SubtitleNextTime = StartTime + ((i + 1) * MsBetweenFrames);
			if (SubtitleNextTime > (uint64_t)StartTime + (uint64_t)Duration) SubtitleNextTime = StartTime + Duration;

			const sub_line_t *line_not_size_selected = nullptr;

			bool outok = false;

			for (int timestamp = CurrentTimestamp; timestamp < Timestamps.count(); timestamp++) {
				CurrentTimestamp = timestamp;
				const sub_line_t *line = &Timestamps.at(timestamp);

				if (line->start >= SubtitleNextTime) {
					if (line_not_size_selected != nullptr) {
						PrintInfo("\tSelected frame at " +
								  QTime::fromMSecsSinceStartOfDay(line_not_size_selected->start).toString("hh:mm:ss.zzz") +
								  " Less text size than especified");
						Screenshots.append(*line_not_size_selected);
						outok = true;
						break;
					}
					PrintInfo("\tSelected frame at " +
							  QTime::fromMSecsSinceStartOfDay(SubtitleStartTime).toString("hh:mm:ss.zzz") +
							  " No subtitle found");
					Screenshots.append({SubtitleStartTime, 0, 0, 0, false, false});
					outok = true;
					break;
				}

				if (line->start > SubtitleStartTime) {
					if (line->text_len < PS.Int("min")) {
						if (line_not_size_selected == nullptr) {
							line_not_size_selected = line;
						} else {
							if (line->text_len > line_not_size_selected->text_len)
								line_not_size_selected = line;
						}
						continue;
					}
					PrintInfo("\tSelected frame at " +
							  QTime::fromMSecsSinceStartOfDay(line->start).toString("hh:mm:ss.zzz") +
							  " OK");
					Screenshots.append(*line);
					outok = true;
					break;
				}
			}
			if (!outok) {
				PrintInfo("\tSelected frame at " +
						  QTime::fromMSecsSinceStartOfDay(SubtitleStartTime).toString("hh:mm:ss.zzz") +
						  " No subtitle found");
				Screenshots.append({SubtitleStartTime, 0, 0, 0, false, false});
			}
		}
		PrintInfo("\tOK\n");
	}

	if (Mode == mode_line) {
		PrintInfo("Selecting frames, Line mode:");

		//Removes the lines that do not meet the minimum
		QList<sub_line_t> LineFiltered;
		int MinLen = PS.Int("min");
		for (int i = 0; i < Timestamps.count(); ++i) {
			if (Timestamps.at(i).text_len < MinLen) continue;
			LineFiltered.append(Timestamps.at(i));
		}

		if (EndLine == 0) EndLine = LineFiltered.count();
		if (EndLine < 0) EndLine = LineFiltered.count() - EndLine;

		uint EveryLines = (EndLine - StartLine) / NumberOfFrames;

		DEBUGVAR(StartLine);
		DEBUGVAR(EndLine);
		DEBUGVAR(EveryLines);

		uint Count = 0;
		for (int i = StartLine; i < EndLine; i += EveryLines) {
			Count++;
			if (Count > NumberOfFrames) break;
			PrintInfo("\tSelected frame at " +
					  QTime::fromMSecsSinceStartOfDay(LineFiltered.at(i).start).toString("hh:mm:ss.zzz") +
					  " OK");
			Screenshots.append(LineFiltered.at(i));
		}
		PrintInfo("\tOK\n");
	}

	if (Mode == mode_video_time) {
		uint64_t MsBetweenFrames = Duration / NumberOfFrames;
		for (uint i = 0; i < NumberOfFrames; i++) {
			uint64_t SnapStartTime = StartTime + (i * MsBetweenFrames);
			PrintInfo("\tSelected frame at " +
					  QTime::fromMSecsSinceStartOfDay(SnapStartTime).toString("hh:mm:ss.zzz") +
					  " OK");
			Screenshots.append({SnapStartTime, 0, 0, 0, false, false});
		}
	}

	if (Mode == mode_video_end) {
		uint64_t MsBetweenFrames = Duration / (NumberOfFrames - 1);
		for (uint i = 0; i < NumberOfFrames - 1; i++) {
			uint64_t SnapStartTime = StartTime + (i * MsBetweenFrames);
			PrintInfo("\tSelected frame at " +
					  QTime::fromMSecsSinceStartOfDay(SnapStartTime).toString("hh:mm:ss.zzz") +
					  " OK");
			Screenshots.append({SnapStartTime, 0, 0, 0, false, false});
		}
		PrintInfo("\tSelected frame at " +
				  QTime::fromMSecsSinceStartOfDay(StartTime + Duration).toString("hh:mm:ss.zzz") +
				  " OK");
		Screenshots.append({((uint64_t)StartTime + (uint64_t)Duration), 0, 0, 0, false, false});
	}

	if (!MANUALFRAMES.isEmpty()) {
		int FrameIndex = -1;
		int64_t FrameTime = -1;
		QRegularExpression FrameNumber("^([0-9]+),([0-9:\\.ms]+)$");
		for (int i = 0; i < MANUALFRAMES.count(); i++) {
			QString ManualFrame = MANUALFRAMES.at(i);
			QRegularExpressionMatch Match = FrameNumber.match(ManualFrame);
			if (Match.hasMatch()) {
				FrameIndex = Match.captured(1).toInt() - 1;
				FrameTime = StringTime2ms(Match.captured(2));
			} else {
				FrameIndex = i;
				FrameTime = StringTime2ms(ManualFrame);
			}
			if (FrameIndex >= Screenshots.count() || FrameTime < 0) {
				PrintError("Invalid option --mf " + ManualFrame);
				continue;
			}
			PrintInfo("Replacing selected frame " +
					  QTime::fromMSecsSinceStartOfDay(Screenshots.at(FrameIndex).start).toString("hh:mm:ss.zzz") +
					  " For " + QTime::fromMSecsSinceStartOfDay(FrameTime).toString("hh:mm:ss.zzz"));
			Screenshots.replace(FrameIndex, {(uint64_t)FrameTime, 0, 0, 0, false, false});
			continue;
		}
	}

	QStringList SelectFramesList;
	for (int i = 0; i < Screenshots.count(); i++) {
		SelectFramesList.append(QTime::fromMSecsSinceStartOfDay(Screenshots.at(i).start).toString("hh:mm:ss.zzz"));
	}
	PrintInfo("Params to select frames:");
	PrintInfo("\t--mfs " + SelectFramesList.join(","));

	if (PS.Int("video_fps") > 0 && VideoPreview > 0) {
		SCREENSHOT_FILTERS.append("fps=fps=" + PS.Value("video_fps"));
	}

	if (PS.Int("rep_frames") > 0 && Mode == mode_video_time) {
		SCREENSHOT_FILTERS.append("thumbnail=n=" + PS.Value("rep_frames"));
	}

	if (VR) {
		if (VR_IN_STEREO == "sbs") {
			if (PS.Bool("vr_right_eye"))
				SCREENSHOT_FILTERS.append("crop=w=iw/2:h=ih:x=iw/2:y=0");
			else
				SCREENSHOT_FILTERS.append("crop=w=iw/2:h=ih:x=0:y=0");
			VR_IN_STEREO = "2d";
		} else if (VR_IN_STEREO == "tb") {
			if (PS.Bool("vr_right_eye"))
				SCREENSHOT_FILTERS.append("crop=w=iw:h=ih/2:x=0:y=ih/2");
			else
				SCREENSHOT_FILTERS.append("crop=w=iw:h=ih/2:x=0:y=0");
			VR_IN_STEREO = "2d";
		}

		if (!PS.Bool("vr_only_crop")) {
			QString VR_H = PS.Value("vr_h");
			QString VR_W = PS.Value("vr_w");
			if (VR_H == "") VR_H = SIZE;
			if (VR_H == "") VR_H = "800";
			if (VR_W == "") VR_W = SIZE;
			if (VR_W == "") VR_W = "800";

			QStringList VR_FILTER = {
				"v360=" + PS.Value("vr_in"),
				PS.Value("vr_out"),
				"in_stereo=" + VR_IN_STEREO,
				"out_stereo=" + PS.Value("vr_out_stereo"),
				"yaw=" + PS.Value("vr_yaw"),
				"pitch=" + PS.Value("vr_pitch"),
				"roll=" + PS.Value("vr_roll"),
				"w=" + VR_W,
				"h=" + VR_H,
				"interp=" + PS.Value("vr_interp")
			};

			if (PS.Int("vr_ih_fov") != 0) VR_FILTER << "ih_fov=" + QString::number(VR_IH_FOV);
			if (PS.Int("vr_iv_fov") != 0) VR_FILTER << "iv_fov=" + QString::number(VR_IV_FOV);

			if (PS.Int("vr_d_fov") != 0) VR_FILTER << "d_fov=" + PS.Value("vr_d_fov");
			if (PS.Int("vr_h_fov") != 0) VR_FILTER << "h_fov=" + PS.Value("vr_h_fov");
			if (PS.Int("vr_v_fov") != 0) VR_FILTER << "v_fov=" + PS.Value("vr_v_fov");

			if (PS.Bool("vr_h_flip")) VR_FILTER << "h_flip=1";
			if (PS.NotEmpty("vr_aditional")) VR_FILTER << PS.Value("vr_aditional");

			SCREENSHOT_FILTERS.append(VR_FILTER.join(':'));
		}
	}

	if (SubtitleEnable) {
		QString TMPSUBTITLE = SUBFILE;
		if (SUBFILE.contains("'")) {
			TMPSUBTITLE = TMP + QDir::separator() + "subtitle." + SUBEXT;
			QFile CopySub(SUBFILE);
			CopySub.copy(TMPSUBTITLE);
		}

		if (PS.NotEmpty("sub_font")) SUBTITLE_FORCE_STYLE.append("Fontname=" + PS.Value("sub_font"));
		if (PS.NotEmpty("sub_color")) SUBTITLE_FORCE_STYLE.append("PrimaryColour=&H" + StringColor2BGR(PS.Value("sub_color")));
		if (PS.NotEmpty("sub_size")) SUBTITLE_FORCE_STYLE.append("Fontsize=" + PS.Value("sub_size"));
		if (PS.NotEmpty("sub_border")) SUBTITLE_FORCE_STYLE.append("BorderStyle=" + PS.Value("sub_border"));
		if (PS.NotEmpty("sub_bc")) SUBTITLE_FORCE_STYLE.append("BackColour=&H" + StringColor2BGR(PS.Value("sub_bc")));
		if (PS.NotEmpty("sub_oc")) SUBTITLE_FORCE_STYLE.append("OutlineColour=&H" + StringColor2BGR(PS.Value("sub_oc")));

		QStringList SUBTITLES_FILTER = {"subtitles=f='" + ScapeFFFilters(TMPSUBTITLE) + "'"};

		if (PS.NotEmpty("sub_fonts_dir")) {
			SUBTITLES_FILTER.append("fontsdir='" + ScapeFFFilters(QDir::toNativeSeparators(PS.Value("sub_fonts_dir"))) + "'");
		}

		if ((SubFormat != sub_ass || PS.Bool("sub_force_ass")) && !SUBTITLE_FORCE_STYLE.empty())
			SUBTITLES_FILTER.append("force_style='" + SUBTITLE_FORCE_STYLE.join(',') + "'");
		SCREENSHOT_FILTERS.append(SUBTITLES_FILTER.join(':'));
	}

	if (SIZE != "") {
		if (VerticalVideo)
			SCREENSHOT_FILTERS.append("scale=-2:" + SIZE + ":flags=lanczos");
		else
			SCREENSHOT_FILTERS.append("scale=" + SIZE + ":-2:flags=lanczos");
	}

	if (!PS.Bool("ft_hide")) {

		if (PS.Value("ft_vertical") != "disabled") {
			if (PS.Value("ft_vertical") == "upward") {
				SCREENSHOT_FILTERS.append("transpose=clock");
				switch (FrameTimePosition) {
					case ft_up_left:
						FrameTimePosition = ft_up_right;
						break;
					case ft_up_right:
						FrameTimePosition = ft_down_right;
						break;
					case ft_down_left:
						FrameTimePosition = ft_up_left;
						break;
					case ft_down_right:
						FrameTimePosition = ft_down_left;
						break;
					case ft_up_center:
						FrameTimePosition = ft_middle_left;
						break;
					case ft_down_center:
						FrameTimePosition = ft_middle_right;
						break;
					case ft_middle_left:
						FrameTimePosition = ft_up_center;
						break;
					case ft_middle_right:
						FrameTimePosition = ft_down_center;
						break;
				}

			} else if (PS.Value("ft_vertical") == "downward") {
				SCREENSHOT_FILTERS.append("transpose=cclock");
				switch (FrameTimePosition) {
					case ft_up_left:
						FrameTimePosition = ft_down_left;
						break;
					case ft_up_right:
						FrameTimePosition = ft_up_left;
						break;
					case ft_down_left:
						FrameTimePosition = ft_down_right;
						break;
					case ft_down_right:
						FrameTimePosition = ft_up_right;
						break;
					case ft_up_center:
						FrameTimePosition = ft_middle_right;
						break;
					case ft_down_center:
						FrameTimePosition = ft_middle_left;
						break;
					case ft_middle_left:
						FrameTimePosition = ft_down_center;
						break;
					case ft_middle_right:
						FrameTimePosition = ft_up_center;
						break;
				}
			}
		}

		QStringList TIME_FILTER;
		if (FRAME_TIMESTAMP_FONT_IS_FILE) {
			TIME_FILTER.append("drawtext=fontfile='" + ScapeFFFilters(FT_FONT) + "'");
		} else {
			if (ffmpeg_has_libfontconfig) {
				TIME_FILTER.append("drawtext=font='" + ScapeFFFilters(FT_FONT) + "'");
			} else {
				PrintError("The ft_font config option needs to be a valid ttf file");
				TERMINATE(10);
			}
		}

		if (FT_FORMAT == "" || FT_FORMAT == "HH:MM:SS.mmm") {
			TIME_FILTER.append("text='%{pts\\:hms}'");
			//TODO: Try to output only seconds
			//		} else if (FT_FORMAT == "SS.mmmmmm") {
			//			TIME_FILTER.append("text='%{pts\\:flt}");
		} else if (FT_FORMAT == "HH:MM:SS" || FT_FORMAT == "short") {
			TIME_FILTER.append("text='%{pts\\:gmtime\\:0\\:%H\\\\\\:%M\\\\\\:%S}'");
		} else {
			TIME_FILTER.append("text='%{pts\\:gmtime\\:0\\:" + ScapeFFFilters(FT_FORMAT.replace(":", "\\:")) + "}'");
		}

		TIME_FILTER.append("fontsize=" + PS.Value("ft_size"));
		TIME_FILTER.append("fontcolor=0x" + StringColor2RGB(PS.Value("ft_color")));
		TIME_FILTER.append("box=1");
		TIME_FILTER.append("boxborderw=" + PS.Value("ft_border"));
		TIME_FILTER.append("boxcolor=0x" + StringColor2RGB(PS.Value("ft_bg")));
		TIME_FILTER.append("borderw=" + PS.Value("ft_font_border"));
		TIME_FILTER.append("bordercolor=" + StringColor2RGB(PS.Value("ft_font_border_color")));
		TIME_FILTER.append("shadowcolor=" + StringColor2RGB(PS.Value("ft_shadow_color")));
		TIME_FILTER.append("shadowx=" + PS.Value("ft_shadow_x"));
		TIME_FILTER.append("shadowy=" + PS.Value("ft_shadow_y"));

		QString Ditance = QString::number(PS.Int("ft_border") + PS.Int("ft_margin"));

		switch (FrameTimePosition) {
			case ft_up_left:
				TIME_FILTER.append("x=" + Ditance);
				TIME_FILTER.append("y=" + Ditance);
				break;
			case ft_up_right:
				TIME_FILTER.append("x=w-tw-" + Ditance);
				TIME_FILTER.append("y=" + Ditance);
				break;
			case ft_down_left:
				TIME_FILTER.append("x=" + Ditance);
				TIME_FILTER.append("y=h-th-" + Ditance);
				break;
			case ft_down_right:
				TIME_FILTER.append("x=w-tw-" + Ditance);
				TIME_FILTER.append("y=h-th-" + Ditance);
				break;
			case ft_up_center:
				TIME_FILTER.append("x=(w-tw)/2");
				TIME_FILTER.append("y=" + Ditance);
				break;
			case ft_down_center:
				TIME_FILTER.append("x=(w-tw)/2");
				TIME_FILTER.append("y=h-th-" + Ditance);
				break;
			case ft_middle_left:
				TIME_FILTER.append("x=" + Ditance);
				TIME_FILTER.append("y=(h-th)/2");
				break;
			case ft_middle_right:
				TIME_FILTER.append("x=w-tw-" + Ditance);
				TIME_FILTER.append("y=(h-th)/2");
				break;
		}


		if (PS.NotEmpty("ft_custom")) TIME_FILTER.append(PS.Value("ft_custom"));

		SCREENSHOT_FILTERS.append(TIME_FILTER.join(':'));

		if (PS.Value("ft_vertical") != "disabled") {
			if (PS.Value("ft_vertical") == "upward")
				SCREENSHOT_FILTERS.append("transpose=cclock");
			else if (PS.Value("ft_vertical") == "downward")
				SCREENSHOT_FILTERS.append("transpose=clock");
		}
	}

	if (PS.NotEmpty("logo")) {
		QString OVERLAY = "";
		QString pos = PS.Value("logo_pos");
		QString margin = PS.Value("logo_margin");
		if (pos == "up_left") {
			OVERLAY = "overlay=" + margin + ":" + margin;
		} else if (pos == "up_right") {
			OVERLAY = "overlay=main_w-overlay_w-" + margin + ":" + margin;
		} else if (pos == "down_right") {
			OVERLAY = "overlay=main_w-overlay_w-" + margin + ":main_h-overlay_h-" + margin;
		} else {
			OVERLAY = "overlay=" + margin + ":main_h-overlay_h-" + margin;
		}
		SCREENSHOT_FILTERS.append(OVERLAY);
	}

	SCREENSHOT_FILTER = SCREENSHOT_FILTERS.join(',');
	PrintInfo("Generating screenshots:");

	QQueue<processqueue_t> ProcessQueue;

	for (int i = 0; i < Screenshots.count(); i++) {
		const sub_line_t *ActualScreen = &Screenshots.at(i);

		QStringList sc_params;
		QString Message;
		QStringList dependent_params;
		QString dependent_message;
		sc_params << "-hide_banner" << "-loglevel" << "error" << "-y"
				  << "-ss" << QString::number(ActualScreen->start) + "ms";

		if (ActualScreen->end == 0 && PS.Bool("only_keyframes")) {
			if (Mode == mode_video_end && i == Screenshots.count() - 1)
				sc_params << "-noaccurate_seek";
			else
				sc_params << "-skip_frame" << "nokey";
		}

		sc_params << "-copyts"
				  << "-i" << VIDEOFILE;

		if (PS.NotEmpty("logo")) sc_params << "-i" << PS.Value("logo") << "-map" << "1:v";

		sc_params << "-map" << "0:v:0";

		if (VideoPreview > 0) {
			sc_params << "-to" << QString::number(ActualScreen->start + VideoPreview) + "ms";
		}

		if (!SCREENSHOT_FILTERS.isEmpty()) {
			if (PS.NotEmpty("logo"))
				sc_params << "-filter_complex";
			else
				sc_params << "-vf";
			sc_params << SCREENSHOT_FILTER;
		}

		if (VideoPreview != 0) {
			sc_params << TMP_FRAME_FORMAT_OPTIONS;
		} else {
			sc_params << "-vframes" << "1";
		}

		if (NoGrid && !Concat && !INDEPENDENT_COMMAND) {
			sc_params << FORMAT_OPTIONS
					  << OUTPUT_DIR + QDir::separator() + OUTPUTNAME + PS.Value("suffix") + QString::number(i + 1) + "." + OUTPUT_FORMAT;
			Message = "\tScreenshot at " + QTime::fromMSecsSinceStartOfDay(ActualScreen->start).toString("hh:mm:ss.zzz") + " File: "
					  + sc_params.last();
		} else {
			sc_params << TMP + QDir::separator() + "screenshot" + QString::number(i + 1) + "." + TMP_FRAME_FORMAT;
			Message = "\tScreenshot at " + QTime::fromMSecsSinceStartOfDay(ActualScreen->start).toString("hh:mm:ss.zzz");

			if (NoGrid && !Concat && INDEPENDENT_COMMAND) {

				dependent_params << "-hide_banner" << "-loglevel" << "error" << "-y" <<
								 "-i" << TMP + QDir::separator() + "screenshot" + QString::number(i + 1) + "." + TMP_FRAME_FORMAT <<
								 FORMAT_OPTIONS << OUTPUT_DIR + QDir::separator() + OUTPUTNAME + PS.Value("suffix") + QString::number(
											 i + 1) + "." + OUTPUT_FORMAT;
				dependent_message = "\tFile: " + dependent_params.last();
			}
		}

		ProcessQueue.enqueue({sc_params, Message, dependent_params, dependent_message});
	}

	if (CurrentInstances > 1) {
		bool Running = true;

		while (Running) {
			Running = false;

			for (int i = 0; i < CurrentInstances; i++) {
				instance_t *Instance = &Instances[i];
				bool launch = false;

				if (Instance->Process->state() == QProcess::Running ) {
					Running = true;
					Instance->Process->waitForFinished(20);
				}

				if (Instance->Process->state() == QProcess::NotRunning) {

					if (Instance->InProgress) {
						if (Instance->Process->exitStatus() == QProcess::CrashExit) {
							QString error = Instance->Process->readAllStandardError();
							PrintError(Instance->Command);
							PrintError(error);
							TERMINATE(5);
						}
						if (Instance->Queue.DependentParams.isEmpty()) PrintProgress(++CurrentStep, Steps);
						Instance->Queue.Params.clear();
						Instance->Queue.Message = "";
						Instance->Command = "";
						Instance->InProgress = false;
					}

					if (!Instance->Queue.DependentParams.isEmpty()) {
						Instance->Queue.Params = Instance->Queue.DependentParams;
						Instance->Queue.Message = Instance->Queue.DependentMessage;
						Instance->Queue.DependentParams.clear();
						Instance->Queue.DependentMessage = "";
						launch = true;
					} else if (!ProcessQueue.isEmpty()) {
						Instance->Queue = ProcessQueue.dequeue();
						launch = true;
					}

					if (launch) {
						Instance->InProgress = true;
						Instance->Command = FFMPEG + " " + Instance->Queue.Params.join(' ');
						Running = true;

						PrintInfo(Instance->Queue.Message + " instance " + QString::number(i + 1));

						Instance->Process->start(FFMPEG, Instance->Queue.Params);
						if (Cmd) PrintInfo(Instance->Command);
						if (!Instance->Process->waitForStarted(-1)) {
							PrintError("Executing command: " + Instance->Command);
							PrintError(Instance->Process->errorString());
							TERMINATE(5);
						}
					}
				}
			}
			if (!ProcessQueue.isEmpty()) Running = true;
		}
	} else {
		while (ProcessQueue.count() > 0) {
			processqueue_t Process = ProcessQueue.dequeue();
			PrintInfo(Process.Message);
			if (!ffmpeg(Process.Params)) {
				TERMINATE(5);
			}
			if (!Process.DependentParams.isEmpty()) {
				PrintInfo(Process.DependentMessage);
				if (!ffmpeg(Process.DependentParams)) {
					TERMINATE(5);
				}
			}
			PrintProgress(++CurrentStep, Steps);
		}
	}

	PrintInfo("\tOK\n");

	if (NoGrid && !Concat) {
		ExecuteCustomCommand(PS.Value("exec_after"), &TS);
		TERMINATE(0);
	}

	if (!Concat)
		PrintInfo("Generating grid:");
	else
		PrintInfo("Generating video:");

	uint64_t height;
	GetImageSize(TMP + QDir::separator() + "screenshot1." + TMP_FRAME_FORMAT, Size, height);

	if (PS.NotEmpty("bg_image") || PS.Value("bg_gradient") != "none") {
		TB_BG_COLOR = SetTransparentRGB(TB_BG_COLOR);
		if (GRID_BORDER_COLOR != "") GRID_BORDER_COLOR = SetTransparentRGB(GRID_BORDER_COLOR);
	}

	if (GRID_BORDER_COLOR == "") GRID_BORDER_COLOR = TB_BG_COLOR;

	QStringList grid_params;

	if (VideoPreview > 0) {
		grid_params << "-hide_banner" << "-loglevel" << "error" << "-y";

		if (Concat) {
			GRID_FILTER = "concat=n=" + QString::number(NumberOfFrames);
			for (uint i = 0; i < NumberOfFrames; i++) {
				grid_params << "-i" << TMP + QDir::separator() + "screenshot" + QString::number(i + 1) + "." + TMP_FRAME_FORMAT;
			}
		} else {
			GRID_FILTER = "xstack=inputs=" + QString::number(NumberOfFrames) + ":fill=" + GRID_BORDER_COLOR + ":layout=";

			QStringList XstackLayout;
			for (uint i = 0; i < NumberOfFrames; i++) {
				grid_params << "-i" << TMP + QDir::separator() + "screenshot" + QString::number(i + 1) + "." + TMP_FRAME_FORMAT;
				int column = i / LayoutX;
				int row = i % LayoutX;
				XstackLayout << QString::number(row * Size) + "_" + QString::number(column * height);
			}

			GRID_FILTER = GRID_FILTER + XstackLayout.join('|');
		}

		grid_params << "-filter_complex" << GRID_FILTER;

		if (TextBlockPosition == text_hidden && OUTPUT_FORMAT != "gif") {
			grid_params << FORMAT_OPTIONS << OUTPUTFILE;
		} else {
			grid_params << TMP_FRAME_FORMAT_OPTIONS << TMP_IMAGES + "." + TMP_FRAME_FORMAT;

			if (INDEPENDENT_COMMAND) {

				if (!ffmpeg(grid_params)) return 6;

				grid_params.clear();

				grid_params << "-hide_banner" << "-loglevel" << "error" << "-y" << "-i" << TMP_IMAGES + "." + TMP_FRAME_FORMAT <<
							FORMAT_OPTIONS << OUTPUTFILE;
			}
		}

	} else if (Concat) {

		grid_params << "-hide_banner" << "-loglevel" << "error" << "-y";
		grid_params << "-r" << PS.Value("video_fps");
		grid_params << "-i" << TMP + QDir::separator() + "screenshot%d." + TMP_FRAME_FORMAT;

		if (INDEPENDENT_COMMAND) {
			grid_params << "-c:v" << "libxvid" << "-qscale:v" << "3" << TMP_IMAGES + ".mkv";
			if (!ffmpeg(grid_params)) return 6;

			grid_params.clear();
			grid_params << "-hide_banner" << "-loglevel" << "error" << "-y" << "-i" << TMP_IMAGES + ".mkv";
		}

		grid_params << FORMAT_OPTIONS << OUTPUTFILE;

	} else {

		GRID_FILTER = "format=rgba,tile=layout=" + LAYOUT;
		if (GridBorder > 0) {
			GRID_FILTER += ":padding=" + QString::number(GridBorder) +
						   ":margin=" + QString::number(GridBorder) +
						   ":color=" + StringColor2RGB(GRID_BORDER_COLOR);
		}

		grid_params << "-hide_banner" << "-loglevel" << "error" << "-y"
					<< "-r" << "1"
					<< "-i" << TMP + QDir::separator() + "screenshot%d." + TMP_FRAME_FORMAT
					<< "-vf" << GRID_FILTER;

		if (TextBlockPosition == text_hidden && !(PS.NotEmpty("bg_image") || PS.Value("bg_gradient") != "none"))
			grid_params << FORMAT_OPTIONS << OUTPUTFILE;
		else
			grid_params << TMP_FRAME_FORMAT_OPTIONS << TMP_IMAGES + "." + TMP_FRAME_FORMAT;

	}

	if (!ffmpeg(grid_params)) return 6;
	PrintInfo("\tOk\n");
	PrintProgress(++CurrentStep, Steps);

	if (TextBlockPosition != text_hidden) {

		PrintInfo("Generating text block:");

		int Border = PS.Int("tb_border");
		TextBlockWidth = Size * LayoutX;
		if (GridBorder > 0) TextBlockWidth += (GridBorder * (LayoutX + 1));
		TextBlockHeight = Border * 2 + PS.Int("tb_add_height");

		DEBUGVAR(TextBlockWidth);
		DEBUGVAR(TextBlockHeight);

		TextBlock TB(TMP);
		TB.SetBGColor(TB_BG_COLOR);
		if (PS.Value("tb_bg_gradient") != "none")
			TB.SetGradient(PS.Value("tb_bg_gradient"));
		TB.SetBorder(Border);
		if (PS.NotEmpty("tb_bg_image")) {
			TB.SetBGImage(PS.Value("tb_bg_image"),
						  TextBlockBackgroundHAling,
						  TextBlockBackgroundVAling,
						  PS.Bool("tb_bg_image_scale"),
						  PS.Bool("tb_bg_image_tile"),
						  PS.Int("tb_bg_image_blur"),
						  PS.Int("tb_bg_image_exposure"),
						  PS.Int("tb_bg_image_saturation"),
						  PS.Int("tb_bg_image_opacity"),
						  PS.Value("tb_bg_image_mask")
						 );
		}
		if (PS.NotEmpty("tb_fonts_dir")) {
			TB.SetFontDir(PS.Value("tb_fonts_dir"));
		}

		text_sub_style_t DefaultStyle("Default", nullptr, &PS, "font");
		DefaultStyle.BorderStyle = border_outline_shadow;
		DefaultStyle.MarginL = Border;
		DefaultStyle.MarginR = Border;
		DefaultStyle.MarginV = Border;
		TB.AddStyle(DefaultStyle);

		text_sub_style_t TitleStyle("Title", &DefaultStyle, &PS, "title");
		TB.AddStyle(TitleStyle);

		text_sub_style_t CommentsStyle("Comments", &DefaultStyle, &PS, "comments");
		TB.AddStyle(CommentsStyle);

		TB_TITLE = TS.ProcessText("", TB_TITLE, &TitleStyle);

		if (TB_TITLE != "") {
			TB.AddLine(TB_TITLE, "Title",
					   PS.Int("tb_title_left_margin"),
					   PS.Int("tb_title_right_margin"),
					   PS.Int("tb_title_vertical_margin"));
		}

		if (SubtitleEnable && !PS.Bool("tb_hide_sub_info")) {
			QString SubText = TS.ProcessText("", PS.Value("tb_custom_subtitle"), &DefaultStyle);
			TB.AddLine(SubText, "Default",
					   PS.Int("tb_font_left_margin"),
					   PS.Int("tb_font_right_margin"),
					   PS.Int("tb_font_vertical_margin"));
		}

		if ((!SubtitleEnable || PS.Bool("tb_video_info")) && !PS.Bool("tb_hide_video_info")) {
			QString FinalText = TS.ProcessText("", PS.Value("tb_custom_header"), &DefaultStyle);
			int line = TB.AddLine(FinalText, "Default",
								  PS.Int("tb_font_left_margin"),
								  PS.Int("tb_font_right_margin"),
								  PS.Int("tb_font_vertical_margin"));


			for (int i = 0; i < TS.Value("streams.count").toInt(); i++) {
				QString Text;
				QString STREAM_TYPE = TS.Value("streams." + QString::number(i) + ".codec_type");

				if (STREAM_TYPE == "video")
					Text = TS.ProcessText("streams." + QString::number(i) + ".", PS.Value("tb_custom_video_stream"), &DefaultStyle);
				else if (STREAM_TYPE == "audio")
					Text = TS.ProcessText("streams." + QString::number(i) + ".", PS.Value("tb_custom_audio_stream"), &DefaultStyle);
				else if (STREAM_TYPE == "subtitle")
					Text = TS.ProcessText("streams." + QString::number(i) + ".", PS.Value("tb_custom_subtitle_stream"), &DefaultStyle);
				else if (STREAM_TYPE == "attachment")
					Text = TS.ProcessText("streams." + QString::number(i) + ".", PS.Value("tb_custom_attachment_stream"), &DefaultStyle);

				TB.AppendLine(Text, line);
			}

			if (PS.Bool("tb_chapters_list")) {
				for (int i = 0; i < TS.Value("chapters.count", "0").toInt(); i++) {
					QString Chapter = TS.ProcessText("chapters." + QString::number(i) + ".", PS.Value("tb_custom_chapters_list"),
													 &DefaultStyle);
					TB.AppendLine(Chapter, line);
				}
			} else {
				QString Chapter = TS.ProcessText("", PS.Value("tb_custom_chapters"), &DefaultStyle);
				TB.AppendLine(Chapter, line);
			}
		}

		int coments_line = -1;

		if (PS.NotEmpty("tb_comments")) {
			QString COMMENTS = TS.ProcessText("", PS.Value("tb_comments"), &CommentsStyle);
			coments_line = TB.AddLine(COMMENTS, "Comments",
									  PS.Int("tb_comments_left_margin"),
									  PS.Int("tb_comments_right_margin"),
									  PS.Int("tb_comments_vertical_margin"));
		}

		if (VR && !PS.Bool("tb_hide_vr_warning")) {
			if (coments_line >= 0)
				TB.AppendLine("VR Video: {\\b1}These screenshots do not represent the exact content of the video file{\\b0}",
							  coments_line);
			else
				TB.AddLine("VR Video: {\\b1}These screenshots do not represent the exact content of the video file{\\b0}",
						   "Comments",
						   PS.Int("tb_comments_left_margin"),
						   PS.Int("tb_comments_right_margin"),
						   PS.Int("tb_comments_vertical_margin"));
		}

		TextBlockHeight += TB.TotalTextHeight();

		if (PS.NotEmpty("tb_logo")) {
			TB.SetLogo(PS.Value("tb_logo"), PS.Value("tb_logo_pos") == "left", PS.Bool("tb_logo_text_overlay"));
			if (PS.Int("tb_logo_height") > (int)TextBlockHeight) TextBlockHeight = PS.Int("tb_logo_height");
		}

		if (PS.NotEmpty("bg_image") || PS.Value("bg_gradient") != "none") {
			uint64_t FinalWidth, FinalHeight;
			GetImageSize(TMP_IMAGES + "." + TMP_FRAME_FORMAT, FinalWidth, FinalHeight);
			FinalHeight += TextBlockHeight;

			if (PS.NotEmpty("tb_cover_top_image")) {
				uint64_t CoverTopWidth, CoverTopHeight;
				GetImageSize(PS.Value("tb_cover_top_image"), CoverTopWidth, CoverTopHeight);
				double CoverTopAS = (double)CoverTopHeight / (double)CoverTopWidth;
				CoverTopCalculatedHeight = qRound((CoverTopWidth / 2) * CoverTopAS) * 2;
				FinalHeight += CoverTopCalculatedHeight;
			}

			if (PS.NotEmpty("bg_image")) {

				if (!TextBlock::GenerateBackgroundImage(PS.Value("bg_image"),
														TMP_BACKGROUND,
														FinalWidth,
														FinalHeight,
														BackgroundHAling,
														BackgroundVAling,
														PS.Bool("bg_image_scale"),
														PS.Bool("bg_image_tile"),
														PS.Int("bg_image_blur"),
														PS.Int("bg_image_exposure"),
														PS.Int("bg_image_saturation"),
														100,
														GRID_BORDER_COLOR)) {
					PrintError("Error procesing background image");
					TERMINATE(5);
				}
			} else {
				if (!TextBlock::GenerateGradient(
								TMP_BACKGROUND,
								FinalWidth,
								FinalHeight,
								PS.Value("bg_gradient"))) {
					PrintError("Error generating background gradient image");
					TERMINATE(5);
				}
			}
			if (PS.NotEmpty("tb_cover_top_image") && TextBlockPosition == text_top)
				TB.SetBGInputImage(TMP_BACKGROUND, CoverTopCalculatedHeight);
			else if (TextBlockPosition == text_bottom)
				TB.SetBGInputImage(TMP_BACKGROUND, FinalHeight - TextBlockHeight);
			else
				TB.SetBGInputImage(TMP_BACKGROUND, 0);
		}

		if (!TB.Generate(TextBlockWidth, TextBlockHeight, TMP_TEXT_IMG)) return 5;

		PrintInfo("\tOk\n");
		PrintProgress(++CurrentStep, Steps);

		QStringList Output_params = {"-hide_banner",
									 "-loglevel",
									 "error",
									 "-y",
									};

		QStringList IndividualFilters;
		QString vstackInputs;
		int vstacki = 0;

		if (PS.NotEmpty("tb_cover_top_image")) {
			Output_params << "-i" << PS.Value("tb_cover_top_image");
			vstackInputs = "[fil" + QString::number(vstacki) + "]";
			IndividualFilters.append("[" + QString::number(vstacki) + "] format=yuva444p,scale=" +
									 QString::number(TextBlockWidth) + ":" + QString::number(CoverTopCalculatedHeight) +
									 ":flags=lanczos " + vstackInputs);
			vstacki++;
		}

		if (TextBlockPosition == text_top) {
			Output_params << "-i" << TMP_TEXT_IMG << "-i" << TMP_IMAGES + "." + TMP_FRAME_FORMAT;
		} else if (TextBlockPosition == text_bottom) {
			Output_params << "-i" << TMP_IMAGES + "." + TMP_FRAME_FORMAT << "-i" << TMP_TEXT_IMG;
		}

		IndividualFilters.append("[" + QString::number(vstacki) + "] format=yuva444p [fil" + QString::number(vstacki) + "]");
		vstackInputs += "[fil" + QString::number(vstacki) + "]";
		vstacki++;
		IndividualFilters.append("[" + QString::number(vstacki) + "] format=yuva444p [fil" + QString::number(vstacki) + "]");
		vstackInputs += "[fil" + QString::number(vstacki) + "]";
		vstacki++;

		Output_params << "-filter_complex" << IndividualFilters.join(";") + "; " + vstackInputs + " vstack=inputs=" +
					  QString::number(vstacki);

		if (PS.NotEmpty("bg_image") || PS.Value("bg_gradient") != "none") {
			PrintInfo("Generating transparent sheet: " + TMP_TRANSPARENT_SHEET);
			Output_params << TMP_FRAME_FORMAT_OPTIONS << TMP_TRANSPARENT_SHEET;
		} else {
			PrintInfo("Generating output: " + OUTPUTFILE);
			Output_params << FORMAT_OPTIONS << OUTPUTFILE;
		}

		if (!ffmpeg(Output_params)) return 5;

		PrintInfo("\tOk\n");
		PrintProgress(++CurrentStep, Steps);
	}

	if (PS.NotEmpty("bg_image") || PS.Value("bg_gradient") != "none") {
		if (TextBlockPosition == text_hidden) {
			TMP_TRANSPARENT_SHEET = TMP_IMAGES + "." + TMP_FRAME_FORMAT;
		}

		PrintInfo("Generating output: " + OUTPUTFILE);

		uint64_t FinalWidth;
		uint64_t FinalHeight;
		GetImageSize(TMP_TRANSPARENT_SHEET, FinalWidth, FinalHeight);

		QStringList bg_param;
		bg_param << "-hide_banner" << "-loglevel" << "error" << "-y";
		bg_param << "-i" << TMP_BACKGROUND;
		bg_param << "-i" << TMP_TRANSPARENT_SHEET <<
				 "-filter_complex" << "[0][1]overlay=format=rgb" << "-vframes" << "1" <<
				 FORMAT_OPTIONS << OUTPUTFILE;

		if (!ffmpeg(bg_param)) return 5;
		PrintInfo("\tOk\n");
		PrintProgress(++CurrentStep, Steps);
	}
	ExecuteCustomCommand(PS.Value("exec_after"), &TS);
	TERMINATE(0);
}
