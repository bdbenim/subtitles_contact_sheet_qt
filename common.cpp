#include "common.h"
#include <stdio.h>
#include <stdlib.h>
#include <QProcess>
#include <filesystem>
#include <QRegularExpression>
#include <QTime>
#include <QTextStream>
#ifdef Q_OS_WIN
	#include <windows.h>
	#include <winbase.h>
#endif


void PrintInfo(QString line) {
	if (!Quiet) {
		fprintf(stdout, "%s\n", line.toUtf8().data());
		fflush(stdout);
	}
}

void PrintDebug(QString line) {
	if (!Quiet && Debug) {
		fprintf(stdout, "%s\n", line.toUtf8().data());
		fflush(stdout);
	}
}

void PrintError(QString line) {
	fprintf(stderr, "Error: %s\n", line.toUtf8().data());
	fflush(stderr);
}

void PrintProgress(int step, int total) {
	if (GUIProgress) {
		fprintf(stdout, "%i/%i\n", step, total);
		fflush(stdout);
	}
}

void DebugVarFunt(QString opt, QString var) {
	PrintDebug(opt + "=" + var);
	//	qDebug("%-30s %s", opt.toUpper().toUtf8().data(), var.toUtf8().data());
}

void DebugVarFunt(QString opt, uint64_t var) {
	DebugVarFunt(opt + "(int)", QString::number(var));
}

QString ScapeFFFilters(QString in) {
	return QDir::toNativeSeparators(in).replace("\\", "\\\\").replace(":", "\\:").replace("%", "\\%").replace(",", "\\,");
}

bool ffmpeg(QStringList params) {
	QProcess process;
	process.start(FFMPEG, params);
	if (Cmd) PrintInfo(FFMPEG + " " + params.join(' '));
	process.waitForFinished(-1);

	if (process.exitCode() != 0 ) {
		QString error = process.readAllStandardError();
		PrintError(FFMPEG + " " + params.join(' '));
		PrintError(error);
		return false;
	}

	return true;
}

bool ffprobe(QString file, int stream, QString var, QString &out) {
	QProcess process;
	QStringList params = {"-i", file};
	if (stream >= 0) params << "-select_streams" << QString::number(stream);
	params << "-show_entries" << var << "-v" << "quiet" << "-of" << "csv=p=0";
	process.start(FFPROBE, params);
	process.waitForFinished(-1);
	if (Cmd) PrintInfo(FFPROBE + " " + params.join(' '));

	if (process.exitCode() != 0 ) {
		QString error = process.readAllStandardError();
		PrintError(FFMPEG + " " + params.join(' '));
		PrintError(error);
		return false;
	}
	out = process.readAll().trimmed();
	return true;
}

bool ffprobe(QString file, QString var, QString &out) {
	return ffprobe(file, -1, var, out);
}

bool ffprobe(QString file, QString &out) {
	QProcess process;
	QStringList params = {"-i", file, "-print_format", "json", "-show_format", "-show_streams", "-show_chapters"};
	process.start(FFPROBE, params);
	process.waitForFinished(-1);
	if (Cmd) PrintInfo(FFPROBE + " " + params.join(' '));

	if (process.exitCode() != 0 ) {
		QString error = process.readAllStandardError();
		PrintError(FFMPEG + " " + params.join(' '));
		PrintError(error);
		return false;
	}
	out = process.readAll().trimmed();
	return true;
}

QString ReadProgram(QString program, QStringList params) {
	QProcess process;
	process.start(program, params);
	process.waitForFinished(-1);
	if (Cmd) PrintInfo(FFPROBE + " " + params.join(' '));

	if (process.exitCode() != 0 ) {
		QString error = process.readAllStandardError();
		PrintInfo(error);
		return "";
	}

	return process.readAll().trimmed();
}

bool CreateSymlink(QString File, QString Link) {
	#ifdef Q_OS_WIN
	LPCSTR o = File.toUtf8();
	LPCSTR d = Link.toUtf8();
	return CreateSymbolicLinkA(d, o, 0);
	#else
	std::error_code ec;
	std::filesystem::create_symlink(File.toUtf8().data(), Link.toUtf8().data(), ec);
	if (ec)
		return false;
	else
		return true;
	#endif
}

bool CreateHardlink(QString File, QString Link) {
	std::error_code ec;
	std::filesystem::create_hard_link(File.toUtf8().data(), Link.toUtf8().data(), ec);
	if (ec)
		return false;
	else
		return true;
}

#ifdef Q_OS_WIN
bool CreateShortcutU(QString File, QString Shortcut, bool Overwrite) {
	QFile F(File);
	QFile DestFile(Shortcut + ".lnk");

	if (!Overwrite && DestFile.exists()) {
		PrintError("Error, the destination file \"" + Shortcut + ".lnk\" anready exists");
		return false;
	}
	if (Overwrite && DestFile.exists()) DestFile.remove();

	return F.link(Shortcut + ".lnk");
}
#else
bool CreateShortcutU(QString /*File*/, QString /*Shortcut*/, bool /*Overwrite*/) {
	PrintError("Error, Shortcuts links are not supported in linux use symlink instead");
	return false;
}
#endif

haligned_t String2HAling(QString pos) {
	pos = pos.toLower();
	if (pos == "left") return aligned_left;
	if (pos == "right") return aligned_right;
	if (pos == "center") return aligned_center;
	return pos.toInt();
}

valigned_t String2VAling(QString pos) {
	pos = pos.toLower();
	if (pos == "up") return aligned_up;
	if (pos == "middle") return aligned_middle;
	if (pos == "down") return aligned_down;
	return pos.toInt();
}

int StringTime2ms(QString time) {
	QRegularExpression MS("^([0-9]+)ms$");
	QRegularExpression hhmmssms("^([0-9]{1,2}):([0-9][0-9]):([0-9][0-9])\\.([0-9][0-9][0-9])$");
	QRegularExpression hhmmss("^([0-9]{1,2}):([0-9][0-9]):([0-9][0-9])$");
	QRegularExpression mmss("^([0-9]{1,2}):([0-9][0-9])$");
	QRegularExpression mmssms("^([0-9]{1,2}):([0-9][0-9])\\.([0-9][0-9][0-9])$");
	QRegularExpression S("^[0-9.]+$");

	QRegularExpressionMatch Match = MS.match(time);
	if (Match.hasMatch()) {
		return Match.captured(1).toInt();
	}

	Match = S.match(time);
	if (Match.hasMatch()) {
		return time.toFloat() * 1000;
	}

	Match = mmss.match(time);
	if (Match.hasMatch()) {
		return QTime::fromString(time, "m:ss").msecsSinceStartOfDay();
	}

	Match = mmssms.match(time);
	if (Match.hasMatch()) {
		return QTime::fromString(time, "m:ss.zzz").msecsSinceStartOfDay();
	}

	Match = hhmmss.match(time);
	if (Match.hasMatch()) {
		return QTime::fromString(time, "H:mm:ss").msecsSinceStartOfDay();
	}

	Match = hhmmssms.match(time);
	if (Match.hasMatch()) {
		return QTime::fromString(time, "H:mm:ss.zzz").msecsSinceStartOfDay();
	}

	return -1;
}

bool GetImageSize(QString image, uint64_t &width, uint64_t &height) {
	QString Output;
	if (!ffprobe(image, "stream=width,height", Output)) return false;
	width = Output.section(',', 0, 0).toInt();
	height = Output.section(',', 1, 1).toInt();
	return true;
}

void WriteFile(QString Filename, QString Content) {
	QFile file(Filename);
	file.open(QIODevice::WriteOnly | QIODevice::Text);
	QTextStream out(&file);
	out << Content;
	file.close();
}

void AppendFile(QString Filename, QString Content) {
	QFile file(Filename);
	file.open(QIODevice::Append);
	QTextStream out(&file);
	out << Content;
	file.close();
}

QString ReadFile(QString Filename) {
	QFile file(Filename);
	if (!file.open(QFile::ReadOnly | QFile::Text)) return "";
	QTextStream in(&file);
	return in.readAll();
}
