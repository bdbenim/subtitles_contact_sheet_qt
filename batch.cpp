#include <QCoreApplication>
#include <stdio.h>
#include <stdlib.h>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QProcess>
#include <QTime>
#include <QThread>
#include <QFileSystemWatcher>
#include "batch.h"

Batch::Batch() {}

int Batch::Execute(int argc, char *argv[]) {
	QCoreApplication a(argc, argv);
	Watcher = new QFileSystemWatcher(&a);

	Command = a.applicationFilePath();
	ExtCurrent = 0;
	ExtParams.append(QStringList());
	ExtCommand.append(Command);

	QTime LastTime = QTime::currentTime();
	QTime LastDeleted = QTime::currentTime();
	QTime LastIncompleteCheck = QTime::currentTime();
	QTime CurrentTime = LastTime;

	if (argc < 2) {
		return 1;
	}

	for (int param_i = 1; param_i < argc; ++param_i) {
		QString Param = argv[param_i];
		if (OwnParams) {
			if (Param == "-r" || Param == "--recursive" ) {
				Recursive = true;

			} else if (Param == "-f" || Param == "--file") {
				param_i++;
				if (param_i == argc) {
					PrintError("Incorrect use of --file or -f");
					return 1;
				}
				FileFilters << argv[param_i];

			} else if (Param == "--video") {
				FileFilters << "*.mkv" << "*.avi" << "*.mp4" << "*.mov" << "*.m4v" << "*.wmv";

			} else if (Param == "--subs") {
				FileFilters << "*.srt" << "*.vtt" << "*.ass";

			} else if (Param == "--no_fail") {
				NoFail = true;

			} else if (Param == "--commnad") {
				param_i++;
				if (param_i == argc) {
					PrintError("Incorrect use of --command");
					return 1;
				}
				Command = argv[param_i];
			} else if (Param == "--gui_progress") {
				GUIProgress = true;
			} else if (Param == "--") {
				OwnParams = false;
			} else if (Param == "-p" || Param == "--parallel") {
				param_i++;
				if (param_i == argc) {
					PrintError("Incorrect use of --parallel or -p");
					return 1;
				}
				CurrentInstances = QString(argv[param_i]).toInt();
			} else if (Param == "-t" || Param == "--time") {
				param_i++;
				if (param_i == argc) {
					PrintError("Incorrect use of --time or -t");
					return 1;
				}
				ScanDirsEvery = QString(argv[param_i]).toInt();
				if (ScanDirsEvery > 0) Daemon = true;

			} else if (Param == "-d" || Param == "--delete") {
				param_i++;
				if (param_i == argc) {
					PrintError("Incorrect use of --delete or -d");
					return 1;
				}
				CleanDeletedFilesEvery = QString(argv[param_i]).toInt();
			} else if (Param == "-w" || Param == "--watch") {
				Watch = true;
				Daemon = true;
			} else if (Param == "-m" || Param == "--modified") {
				param_i++;
				if (param_i == argc) {
					PrintError("Incorrect use of --modified or -m");
					return 1;
				}
				ModifiedTime = QString(argv[param_i]).toInt();
			} else if (Param == "--recheck") {
				param_i++;
				if (param_i == argc) {
					PrintError("Incorrect use of --recheck");
					return 1;
				}
				CheckIncompleteEvery = QString(argv[param_i]).toInt();
			} else if (Param == "--cmd") {
				CMD = true;
			} else if (Param == "--show_output") {
				ShowOutput = true;
			} else if (Param == "-q" || Param == "--quiet") {
				Quiet = true;
			} else {
				QFileInfo CheckInputFile(Param);
				if (CheckInputFile.isDir()) {
					StartDirList.append(Param);
					continue;
				} else if (CheckInputFile.isFile()) {
					FileListQueue.append(QDir::toNativeSeparators(Param));
					continue;
				}
			}

		} else {
			if (Param == "--") {
				ExtParams.append(QStringList());
				ExtCommand.append(Command);
				ExtCurrent++;
				continue;
			} else if (Param == "-c" || Param == "--commnad") {
				param_i++;
				if (param_i == argc) {
					PrintError("Incorrect use of --command or -c");
					return 1;
				}
				ExtCommand[ExtCurrent] = argv[param_i];
			} else {
				ExtParams[ExtCurrent].append(Param);
			}
		}
	}

	if (StartDirList.isEmpty() && FileListQueue.isEmpty()) {
		PrintError("No input directories or files");
		return 1;
	}

	if (CurrentInstances < 1) CurrentInstances = 1;
	if (CurrentInstances > 16) CurrentInstances = 16;

	if (CMD) {
		Quiet = true;
		ShowOutput = false;
		CurrentInstances = 1;
	}

	if (ShowOutput) CurrentInstances = 1;

	for (int i = 0; i < CurrentInstances; i++) {
		Instances.append({new QProcess(), {"", false, "", {""}, ""}, false});
	}

	DirListQueue = StartDirList;

	QObject::connect(Watcher, &QFileSystemWatcher::directoryChanged, &a, [ this ](QString Dir) {
		PrintInfo("Dir change: " + Dir);
		QStringList currentwatches = Watcher->directories();
		for (int i = 0; i < currentwatches.count() ; i++) {
			QDir dir(currentwatches.at(i));
			if (dir.exists()) continue;
			Watcher->removePath(currentwatches.at(i));
			PrintInfo("Remove Watch: " + currentwatches.at(i) + " Dir " + Dir);
		}
		if (!currentwatches.contains(Dir)) return;
		DirListQueue.append(Dir);
	});

	bool Running = true;

	while (Running) {

		if (!FileListQueue.isEmpty()) {
			QString File = FileListQueue.takeFirst();
			FileListProcessing.append(File);
			AddFileToCommandQueue(File);
		}

		Running = false;
		for (int i = 0; i < CurrentInstances; i++) {
			batch_instance_t *Instance = &Instances[i];

			if (Instance->Process->state() == QProcess::Running ) {
				Running = true;
				if (ShowOutput) {
					QByteArray read = Instance->Process->readAllStandardError();
					if (!read.isEmpty()) {
						fprintf(stderr, "%s", read.data());
						fflush(stderr);
					}
					read = Instance->Process->readAllStandardOutput();
					if (!read.isEmpty()) {
						fprintf(stdout, "%s", read.data());
						fflush(stdout);
					}
				}
				Instance->Process->waitForFinished(20);
			}

			if (Instance->Process->state() == QProcess::NotRunning) {

				if (Instance->InProgress) {
					if (ShowOutput) {
						QByteArray read = Instance->Process->readAllStandardOutput();
						if (!read.isEmpty()) {
							fprintf(stdout, "%s", read.data());
							fflush(stdout);
						}
					}
					if (Instance->Process->exitStatus() == QProcess::CrashExit) {
						QString error = Instance->Process->readAllStandardError();
						PrintError("Executing command: " + Instance->Command.Text);
						PrintError(error);
						if (!NoFail) return 1;
					} else {
						if (Instance->Process->exitCode() != 0) {
							QString error = Instance->Process->readAllStandardError();
							PrintError("Executing command: " + Instance->Command.Text);
							PrintError(error);
							if (!NoFail) return 1;
						}
					}
					FileListDone.append(Instance->Command.File);
					FileListProcessing.removeAll(Instance->Command.File);
					Instance->Command.File = "";
					Instance->Command.Text = "";
					Instance->Command.InProgress = false;
					Instance->InProgress = false;
				}

				if (!CommandQueue.isEmpty()) {
					Instance->Command = CommandQueue.dequeue();
					Instance->InProgress = true;
					Running = true;
					PrintInfo("Process " + QString::number(i + 1) + ": " + Instance->Command.Text + "\n");

					if (GUIProgress && TotalSteps > 1) {
						fprintf(stdout, "%i/%i\n", ++Steps, TotalSteps);
						fflush(stdout);
					}

					if (CMD) {
						PrintCommand(Instance->Command.Text);
					} else {
						Instance->Process->start(Instance->Command.Program, Instance->Command.Params);
						if (!Instance->Process->waitForStarted(-1)) {
							PrintError("Executing command: " + Instance->Command.Text);
							PrintError(Instance->Process->errorString());
							if (!NoFail) return 1;
						}
					}
				}
			}
		}

		if (Daemon) {
			Running = true;

			if (CleanDeletedFilesEvery > 0) {
				CurrentTime = QTime::currentTime();
				if (LastDeleted.secsTo(CurrentTime) >= CleanDeletedFilesEvery) {
					LastDeleted = CurrentTime;
					for (int i = FileListDone.count() - 1; i >= 0; i--) {
						if (!QFile::exists(FileListDone.at(i))) {
							PrintInfo("Cleaning deleted file: " + FileListDone.at(i));
							FileListDone.removeAt(i);
						}
					}
				}
			}

			if (CheckIncompleteEvery > 0 && FileListIncomplete.count() > 0) {
				CurrentTime = QTime::currentTime();
				if (LastIncompleteCheck.secsTo(CurrentTime) >= CheckIncompleteEvery) {
					LastIncompleteCheck = CurrentTime;
					for (int i = FileListIncomplete.count() - 1; i >= 0; i--) {
						PrintInfo("Rechecking file: " + FileListIncomplete.at(i));
						QFileInfo info(FileListIncomplete.at(i));
						if (!info.exists()) {
							FileListIncomplete.removeAt(i);
							continue;
						}
						if (info.size() == 0) continue;
						if (ModifiedTime > 0 && info.lastModified().secsTo(QDateTime::currentDateTime()) < ModifiedTime) continue;
						FileListQueue.append(FileListIncomplete.at(i));
						FileListIncomplete.removeAt(i);
					}
				}
			}
		}

		if (ScanDirsEvery > 0) {
			CurrentTime = QTime::currentTime();
			if (LastTime.secsTo(CurrentTime) >= ScanDirsEvery) {
				LastTime = CurrentTime;
				DirListQueue.append(StartDirList);
			}
		}

		if (!DirListQueue.isEmpty()) {
			DirSelect(DirListQueue.takeFirst());
			Running = true;
		}

		if (Watch) a.processEvents(QEventLoop::AllEvents, 100);
		QThread::msleep(100);
	}

	for (int i = 0; i < CurrentInstances; i++) {
		delete Instances[i].Process;
	}

	return 0;
}

void Batch::DirSelect(const QString Dir) {
	QDir DirObj(Dir);

	if (!DirObj.exists()) return;

	QStringList watched_dirs = Watcher->directories();
	if (Watch && !watched_dirs.contains(Dir)) {
		PrintInfo("Add Watch: " + Dir);
		Watcher->addPath(Dir);
	}

	QStringList CurrentList = DirObj.entryList(FileFilters, QDir::Files);
	for (int i = 0; i < CurrentList.count(); ++i) {
		QString filename = QDir::toNativeSeparators(DirObj.absolutePath() + QDir::separator() + CurrentList.at(i));
		if (FileListDone.contains(filename) ||
			FileListQueue.contains(filename) ||
			FileListIncomplete.contains(filename) ||
			FileListProcessing.contains(filename)) continue;
		QFileInfo info(filename);
		if (info.size() == 0) {
			PrintInfo("File Ignored size 0: " + filename);
			if (CheckIncompleteEvery > 0) FileListIncomplete.append(filename);
			continue;
		}
		if (ModifiedTime > 0 && info.lastModified().secsTo(QDateTime::currentDateTime()) < ModifiedTime) {
			PrintInfo("File Ignored modification time (" +
					  QString::number(info.lastModified().secsTo(QDateTime::currentDateTime())) + "): " + filename);
			if (CheckIncompleteEvery > 0) FileListIncomplete.append(filename);
			continue;
		}
		FileListQueue.append(filename);
	}
	if (Recursive) {
		CurrentList = DirObj.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
		for (int i = 0; i < CurrentList.count(); ++i) {
			DirListQueue.append(QDir::toNativeSeparators(DirObj.absolutePath() + QDir::separator() + CurrentList.at(i)));
		}
	}
}

void Batch::PrintInfo(QString line) {
	if (!Quiet) {
		fprintf(stdout, "%s\n", line.toUtf8().data());
		fflush(stdout);
	}
}

void Batch::PrintError(QString line) {
	fprintf(stderr, "Error: %s\n", line.toUtf8().data());
	fflush(stderr);
}

void Batch::PrintCommand(QString command) {
	fprintf(stdout, "%s\n", command.toUtf8().data());
	fflush(stdout);
}

void Batch::AddFileToCommandQueue(QString File) {
	QFileInfo FileInfo(File);
	QString FileName = FileInfo.completeBaseName();
	QString Path = FileInfo.path();
	QString Ext = FileInfo.suffix();

	for (int i = 0; i < ExtParams.count(); ++i) {
		command_queue_item item;
		item.Params = ExtParams.at(i);
		item.File = File;
		item.InProgress = false;
		item.Program = ExtCommand.at(i);
		bool FoundFile = false;
		for (int i = 0; i < item.Params.count(); ++i) {
			QString Param = item.Params.at(i);
			if (Param.contains("%file%")) {
				FoundFile = true;
				item.Params.replace(i, Param.replace("%file%", File));
			}
			if (Param.contains("%filename%")) {
				item.Params.replace(i, Param.replace("%filename%", FileName));
			}
			if (Param.contains("%path%")) {
				item.Params.replace(i, Param.replace("%path%", Path));
			}
			if (Param.contains("%ext%")) {
				item.Params.replace(i, Param.replace("%ext%", Ext));
			}
		}

		if (!FoundFile) item.Params.prepend(File);

		item.Text = item.Program;
		for (int i = 0; i < item.Params.count(); ++i) {
			if (item.Params.at(i).contains(" ")) {
				item.Text.append(" \"" + item.Params.at(i) + "\"");
			} else {
				item.Text.append(" " + item.Params.at(i));
			}
		}

		CommandQueue.enqueue(item);
		TotalSteps++;
	}

	if (GUIProgress && TotalSteps > 1) {
		fprintf(stdout, "%i/%i\n", Steps, TotalSteps);
		fflush(stdout);
	}
}
