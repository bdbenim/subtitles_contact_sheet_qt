#ifndef TEXTBLOCK_H
#define TEXTBLOCK_H

#include "common.h"
#include "paramssettings.h"
#include <QObject>

enum wrap_style_t {
	wrap_smart = 0,			// 0: smart wrapping, lines are evenly broken
	wrap_end_line = 1,		// 1: end-of-line word wrapping, only \N breaks
	wrap_no = 2,			// 2: no word wrapping, \n \N both breaks
	wrap_smart_lower = 3	// 3: same as 0, but lower line gets wider.
};

enum border_style_t {
	border_outline_shadow = 1,	// 1: Outline + drop shadow
	border_undefined = 2,		// 2: ...
	border_box = 3,				// 3: Box per line
	border_tbox = 4				// 4: One Box for all lines
};

enum aligment_t {			//Same position as numpad keyboard
	align_down_left = 1,
	align_down_center = 2,
	align_down_right = 3,
	align_middle_left = 4,
	align_middle_center = 5,
	align_middle_right = 6,
	align_up_left = 7,
	align_up_center = 8,
	align_up_right = 9,
};

aligment_t String2Alingment(QString pos);

struct text_sub_style_t {
	text_sub_style_t(QString name, text_sub_style_t *copy = nullptr, ParamsSettings *PS = nullptr, QString style = "");
	QString Name;
	QString Fontname;
	uint Fontsize;
	QString PrimaryColour;
	QString SecondaryColour;
	QString OutlineColour;
	QString BackColour;
	bool Bold;
	bool Italic;
	bool Underline;
	bool StrikeOut;
	border_style_t BorderStyle;
	int Outline;
	int Shadow;
	aligment_t Alignment;
	int MarginL;
	int MarginR;
	int MarginV;
	int BorderBlur;
	void SetAlignment(QString align);
};

struct text_sub_line_t {
	QString Text;
	text_sub_style_t *Style;
	int MarginL;
	int MarginR;
	int MarginV;
	int Lines;
};

class TextBlock : public QObject {
		Q_OBJECT
	public:
		explicit TextBlock(QString TmpDir, QObject *parent = nullptr);

		void AddStyle(text_sub_style_t &Style);
		int AddLine(QString text, QString StyleName = "", int MarginL = 0, int MarginR = 0, int MarginV = 0);
		void AppendLine(QString text, int line);

		void SetFontDir(QString Dir);
		void SetBGImage(QString File, haligned_t ha = aligned_left, valigned_t va = aligned_up, bool scale = false,
						bool tile = true,
						int blur = 0, int exposure = 0, int saturation = 100, int opacity = 100, QString mask = "");
		void SetBGColor(QString Color);
		void SetGradient(QString grad);
		void SetLogo(QString logo, bool left = false, bool overlay = false);
		void SetBorder(int border);
		void SetBGInputImage(QString image, uint64_t pos);

		static bool GenerateBackgroundImage(QString In, QString Out, int w, int h, haligned_t ha, valigned_t va, bool scale,
											bool tile, int blur = 0, int exposure = 0, int saturation = 100, int opacity = 100,
											QString fillcolor = "FFFFFF00", QString bgimage = "", QString bg = "", QString mask = "", int pos = 0);

		static QString String2GradientFilter(int width, int height, QString gradient);
		static QString GenerateGradientFilter(int width, int height, QString type, QString colors, QString direction);
		static QString GenerateSolidColorFilter(int width, int height, QString color);
		static bool GenerateGradient(QString Out, int width, int height, QString gradient);

		int Generate(int Width, int Height, QString Output);
		int TotalTextHeight();
	private:
		QString Tmp;
		QString TmpSub;
		QString TmpBackground;
		int WrapStyle;
		int Width;
		int Height;
		QList<text_sub_style_t> Styles;
		QList<text_sub_line_t> Lines;
		QString FontDir;
		QString BGImageFile;
		QString BGInputImage;
		uint64_t BGInputImagePos;
		haligned_t BGImageHAlign;
		valigned_t BGImageVAlign;
		bool BGImageScale;
		bool BGImageTile;
		int BGImageBlur;
		int BGImageExposure;
		int BGImageSaturation;
		int BGImageOpacity;
		QString BGInputMask;
		QString BGColor;
		bool BGGradient;
		QString BGGrad;
		QString Logo;
		int LogoWidth;
		double LogoAR;
		bool LogoLeft;
		bool LogoOverlay;
		int Border;

		void GenerateSubTmpFile(int Width, int Height, wrap_style_t Wrap = wrap_no);
		QStringList GenerateParams(int Width, int Height, QString Output);

	signals:

};

#endif // TEXTBLOCK_H
