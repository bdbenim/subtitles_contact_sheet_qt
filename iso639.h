#ifndef ISO639_H
#define ISO639_H
#include <QString>

QString Iso639ToLangName(QString iso639);
QString Iso639ToShortName(QString iso639);

#endif // ISO639_H
