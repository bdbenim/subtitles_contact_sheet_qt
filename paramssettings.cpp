#include "paramssettings.h"
#include "common.h"
#include "textsustitution.h"
#include <QDir>
#include <QRegularExpression>
#include <QStandardPaths>
#include <QFileInfo>
#include <QFileInfoList>
#ifdef Q_OS_WIN
	#include <windows.h>
#else
	#include <sys/ioctl.h>
	#include <stdio.h>
#endif

QRegularExpression RegexpUserDefinedVar("^VAR_(\\d+)_(SV_|SA_|SS_|SF_|S_|)(.+)|VAR_((.+))$");

ParamsSettings::ParamsSettings(QString applicationDirPath, QObject *parent)
	: QObject{parent}, UserDefinedVarOrderCount(0) {

	AppPath = applicationDirPath;
	#ifdef Q_OS_WIN
	ConfigPath = applicationDirPath;
	ConfigExt = "ini";
#define DEFAULT_FFMPEG "ffmpeg.exe"
#define DEFAULT_FFPROBE "ffprobe.exe"
	#else
	ConfigPath = QStandardPaths::locate(QStandardPaths::ConfigLocation,
										"subtitles_contact_sheet",
										QStandardPaths::LocateDirectory);
	ConfigExt = "conf";
#define DEFAULT_FFMPEG "ffmpeg"
#define DEFAULT_FFPROBE "ffprobe"
	#endif

	QString Fonts = "";
	#ifdef Q_OS_WIN
	if (QDir("Fonts").exists()) Fonts = "Fonts";
	#endif

	HelpPrefixText.insert("", "\nUsage: %1$s [subtitle_or_video_file] [options]\n"
						  "\n"
						  "This program generate a contact sheet for the subtitle or video file in the same directory of it, "
						  "with the same name attaching '.contact_sheet.<format>' at the end. "
						  "Can also run in batch mode see \"--help-batch\" for instructions.\n"
						  "\n"
						  "Simple examples: \n"
						  "\n"
						  "%1$s input_sub_file.en.srt\n"
						  "Only works if the video file has the name input_sub_file, "
						  "the extensions is mkv, avi, mp4, mov, wmv and they are in the same folder\n"
						  "\n"
						  "%1$s input_sub_file.mkv\n"
						  "Generates the contact sheet of the video\n"
						  "\n"
						  "All options that have a color as input use the RRGGBB[AA] format. "
						  "You can also use the color names from ffmpeg. https://ffmpeg.org/ffmpeg-utils.html#Color\n"
						  "\n"
						  "Boolean parameters support forcing a value by adding 0 or 1. "
						  "The parameter without specifying the value changes the defaults, "
						  "if the default value is 1 it sets it to 0 and vice versa.\n"
						  "Example \"--ft_hide\" is the same as \"--ft_hide 1\" and can be disabled with \"--ft_hide 0\"\n\n"
						  "General Options:\n");

	AddHelp("", "config", "<name|file>", "Name or file of the config file used.", "c", "Configutarion");
	AddHelp("", "list_config", "", "Shows a list of the names of the available configuration files.", "", "", param_bool);
	AddStr("config_desc", "", "", false, "", "<text>", "Small text describing what this configuration file do.",
		   "Configuration description");
	AddHelp("", "save", "", "Save the actual parameters in configuration file.", "", "", param_bool);
	AddHelp("", "save_all", "",
			"Same as --save but creates the complete config file, all the unset options with the default value.", "", "",
			param_bool);
	AddHelp("", "batch", "",
			"Enables batch mode, allows to process multiple input files or multiple options for one file, has its own special parameters and systax, by running \"--help-batch\".",
			"b", "", param_bool);

	AddHelp("", "sub", "<file>", "Subtitle file input.", "", "Subtitle File", param_file);
	AddHelp("", "video", "<file>", "Video file input.", "", "Video File", param_file);
	AddHelp("", "out", "<file>", "Output file. This option has priority over the other output options.", "", "Output File",
			param_file);
	AddDir("out_dir", "", false, "", "Output directory.", "Output directory");
	AddStr("out_relative_dir", "", "", false, "", "<name>", "Output directory relative to input file.",
		   "Relative Output Directory");
	AddStr("out_name", "%in.filename%", "", false, "", "<name>", "Modify output file name using text substitution.",
		   "Output Name");
	AddStr("suffix", ".contact_sheet", "", false, "", "<text>",
		   "Text appended to the output file name before the format extension. Default .contact_sheet.", "Suffix");
	AddBool("no_overwrite", false, false, "",
			"Does not overwrite the output file, if it already exists it simply does nothing.", "No Overwrite");
	AddInt("process", 1, "min=1,max=16", false, "", "<number>", "Nunber of parallel processes. Default 1.",
		   "Parallel processes");
	AddLayout("layout", "3x7", true, "", "Number of captured frames in the format COLUMSxROWS.", "Grid layout");
	AddInt("size", 640, "min=16", false, "", "<px>",
		   "Size in pixels of the largest dimension of the video, the width in horizontal videos, the height in vertical videos.",
		   "Screenshots Size");
	AddInt("min_size", 300, "min=16", false, "", "<px>",
		   "The smallest dimension of a video will never be smaller than this.", "Screenshots Minimum Size");
	AddStr("format", "jpg", "jpg|png|apng|webp|webploop|gif|giflow|xvid|x264|x265", true, "", "<ext>", "Output format.",
		   "Output Format");
	AddStr("format_options", "", "", false, "", "<options>", "Output format options, ffmpeg output parameters.",
		   "Output Format Options");
	AddInt("output_quality", -1, "min=-1,max=51", false, "", "<number>",
		   "For formats that support quality settings, each format has different supported values. Default -1 (use format default value).",
		   "Output Quality");

	AddHelp("", "jpg", "", "Output format jpg (default).", "", "", param_bool);
	AddHelp("", "jpeg", "", "Output format jpg (default).", "", "", param_bool);
	AddHelp("", "png", "", "Output format png.", "", "", param_bool);
	AddHelp("", "apng", "", "Output format animated png (Only video).", "", "", param_bool);
	AddHelp("", "webp", "", "Output format webp.", "", "", param_bool);
	AddHelp("", "webploop", "", "Output format webp loop video.", "", "", param_bool);
	AddHelp("", "gif", "", "Output format gif. Each frame has its own color palette.", "", "", param_bool);
	AddHelp("", "giflow", "", "Output format gif. Same color palette for the whole file, has less quality.", "", "",
			param_bool);
	AddHelp("", "xvid", "", "Output format mkv with xvid codec.", "", "", param_bool);
	AddHelp("", "x264", "", "Output format mkv with x264 codec.", "", "", param_bool);
	AddHelp("", "x265", "", "Output format mkv with x265 codec.", "", "", param_bool);

	AddStr("start", "30", "", false, "", "<time>",
		   "Exclude the first part, in time mode is in seconds or HH:MM:SS, in line mode is in lines.", "Start Time");
	AddStr("end", "-30", "", false, "", "<time>",
		   "In time mode is the time in second on HH:MM:SS to end, If it is negative, it is subtracted from the duration.",
		   "End Time");
	AddStr("mode", "time", "time|line|video_time|video_end", true, "", "<mode>",
		   "Mode of operation, searches for frames to capture using different methods. "
		   "It can be: time, line, video_time, video_end. time and line are for subtitles, "
		   "video_time, and video_end are for video only. More details in man page.", "Mode");

	AddBool("only_keyframes", false, false, "", "Try to campure only keyframes, sometimes this is faster.",
			"Only Keyframes");
	AddInt("rep_frames", 0, "min=1", false, "", "<number>",
		   "selects the most representative frame considering the specified number of frames.", "Most Representative Frames");

	AddHelp("", "mf", "[nunber,]time",
			"Allows manual selection of screenshots. Replaces the automatically selected time with the indicated in time. "
			"You can add as many --mf as you want, if no number is indicated the order of --mf in the parameters is used, "
			"the first parameter would be 1 and successively.", "", "Manual Frame", param_string);

	AddHelp("", "mfs", "<time_list>",
			"Comma-separated time list with all selected frames, the same as the previous command but in one go, "
			"the current command is always displayed in the output messages.", "", "Multiple Manual Frames", param_string);

	AddBool("no_grid", false, false, "",
			"Don't generate the screenshot grid, generates an individual file for each screenshot.", "Disable Grid");
	AddInt("grid_border", 2, "min=0", false, "", "<px>", "Set border for the grid in pixels.", "Grid Border");
	AddColor("grid_border_color", "", false, "",
			 "Set the background for the borders of the grid in RRGGBBAA format. By default uses the same value as --tb_bg_color.",
			 "Grid Border Color");
	AddStr("bg_image", "", "", false, "", "<file>", "Set the background image for the entire contsct sheet. "
		   "Implies that grid and text block backgrounds become transparent.", "Background Image");
	AddStr("bg_image_halign", "center", "right|center|left|%", true, "", "<haling>",
		   "Horizontal alignment of background image, can be right, center, left or a percentage.",
		   "Horizontal Align for Bacground Image");
	AddStr("bg_image_valign", "middle", "up|middle|down|%", true, "", "<valing>",
		   "Vertical alignment of background image, can be up, middle, down or a percentage.",
		   "Vertical Align for Bacground Image");
	AddBool("bg_image_scale", false, false, "", "Scale the background image before crop it.", "Autoscale Background Image");
	AddBool("bg_image_tile", true, false, "", "Repeat the image as many times as necessary to fill the space.",
			"Tile Background Image");
	AddInt("bg_image_blur", 0, "min=0,max=100", false, "", "<number>",
		   "Applies a blur filter to the background image. the higher the number the more blurred. Default 0 (disabled).",
		   "Blur Background Image");
	AddInt("bg_image_exposure", 0, "min=-300,max=300", false, "", "<number>",
		   "Applies an exposure filter to the background image. "
		   "allows a number between -300 and 300, negative numbers decrease the exposure making the image darker, "
		   "positive numbers make the image lighter. Default 0 (disabled).", "Change Exposure for Background Image");
	AddInt("bg_image_saturation", 100, "min=0,max=300", false, "", "<number>",
		   "Applies a saturation filter to the background image. "
		   "Accepts a number between 0 and 300, 0 converts the image to black and white. Default 100 (disabled).",
		   "Change Exposure for Background Image");
	AddStr("bg_gradient", "none", "", false, "", "<gradient>",
		   "Generates a gradient as a background image with the indicated parameters. Example \"linear:h:white,black\".",
		   "Gradient as Background");
	AddStr("logo", "", "", false, "", "<file>", "Add this logo to each screenshot.", "Frames Logo");
	AddStr("logo_pos", "down_left", "up_left|up_right|down_left|down_right", true, "", "<position>",
		   "Position of the logo, can be up_left, up_right, down_left, down_right. Default down_left.", "Frames Logo Position");
	AddInt("logo_margin", 10, "min=0", false, "", "<px>", "Pixel separation of logo from borders. Default 10.",
		   "Frames Logo Margin");

	AddInt("video_preview", 0, "min=0", false, "", "<milliseconds>",
		   "Instead of capturing images, it captures videos of the specified duration.", "Video Preview");
	AddInt("video_fps", 0, "min=0", false, "", "<fps>",
		   "When capturing videos or generating videos from images, these are the frames per second of the result.", "Video FPS");
	AddBool("concat", false, false, "",
			"Instead of putting the screenshots in a grid, puts them one after the other, requires an output format that supports video.",
			"Concat");

	AddHelp("", "full", "", "Screenshots at input resolution, same as --size \"\".", "", "Full Resolution", param_bool);
	AddHelp("", "screenshots", "<number>",
			"Full size unaltered screentshots, same as --no_grid --size \"\" --format png --ft_hide --sufix \".screenshot_\"", "",
			"Unaltered Screenshots", param_shortcut);

	AddHelp("", "gif_images", "<images>",
			"Short gif video with images. Same as --format gif - l 1x<images> --ft_hide --concat --video_fps 1 --suffix \".images\"",
			"", "Gif Images", param_shortcut);
	AddHelp("", "webp_images", "<images>",
			"Short webp video with images. Same as --format webploop - l 1x<images> --ft_hide --concat --video_fps 1 --suffix \".images\"",
			"", "Webp Images", param_shortcut);
	AddHelp("", "gif_clip", "<clips>",
			"Gif video with short one - second videos. Same as --format gif - l 1x<clips> --ft_hide --concat --video_preview 1000 --video_fps 12 --size 320 --suffix \".clip\"",
			"", "Gif Clips", param_shortcut);
	AddHelp("", "webp_clip", "<clips>",
			"Gif video with short one - second videos. Same as --format webploop - l 1x<clips> --ft_hide --concat --video_preview 1000 --video_fps 12 --suffix \".clip\"",
			"", "Webp Clips", param_shortcut);
	AddHelp("", "gif_singles", "<clips>",
			"Same as --gif_clip but does not join the fragments together, it generates as many independent gifs as indicated. "
			"Same as --format gif - l 1x<clips> --ft_hide --video_preview 1000 --video_fps 12 --size 320 --suffix \".single\"",
			"", "Gif Independent clips", param_shortcut);

	AddStr("ffmpeg", DEFAULT_FFMPEG, "", true, "", "<file>", "ffmpeg executable file.", "Ffmpeg");
	AddStr("ffprobe", DEFAULT_FFPROBE, "", true, "", "<file>", "ffprobe executable file.", "Ffprobe");
	AddStr("mediainfo", "", "", false, "", "<file>", "mediainfo cli executable file.", "mediainfo");
	AddBool("enable_mediainfo", false, false, "", "Imports to variables the information provided by mediainfo.",
			"Enable mediainfo");
	AddDir("tmp", "", false, "", "Temporal directory.", "Tmp Directory");

	AddStr("exec_before", "", "", false, "", "<command>",
		   "Execute this command before start. Supports text substitution to customize the command.", "Command to Execute Before");
	AddStr("exec_after", "", "", false, "", "<command>",
		   "Execute this command at the end. Supports text substitution to customize the command. This command is executed even if no_overwrite is enabled.",
		   "Command to Execute After");

	AddHelp("", "var", "<var=value>",
			"Allows to create variables to be used within text substitution, these variables are also saved in the configuration file "
			"prefixed with \"VAR_\" and a number to execute them in order.", "", "User Defined Variable", param_string);
	AddHelp("", "var_stream", "<var=value>",
			"Same as var but it is executed in each stream, the value of the variable can start with \"context.\" "
			"to define it within that context, if it is not a normal variable.", "", "User Defined Variable Stream context",
			param_string);
	AddHelp("", "var_stream_video", "<var=value>", "Same as \"--var_stream\" but are executed only on video streams.", "",
			"User Defined Variable Video context", param_string);
	AddHelp("", "var_stream_audio", "<var=value>", "Same as \"--var_stream\" but are executed only on audio streams.", "",
			"User Defined Variable Audio context", param_string);
	AddHelp("", "var_stream_subtitle", "<var=value>",
			"Same as \"--var_stream\" but are executed only on subtitles streams.", "", "User Defined Variable Subtitles context",
			param_string);
	AddHelp("", "var_stream_attachment", "<var=value>",
			"Same as \"--var_stream\" but are executed only on attachment streams.", "",
			"User Defined Variable Attachment context", param_string);

	AddFile("template", "", false, "",
			"Generate a text using this file as a template using text substitution.", "Template File");
	AddStr("template_ext", "txt", "", false, "", "<extension>",
		   "Extension added to the video name to generate the file with the substituted values.", "Template Extension");

	AddBool("quiet", false, false, "", "Disable messages, for batch processing.", "Quiet");
	AddBool("debug", false, false, "", "Enable debug mode.", "Debug");
	AddBool("cmd", false, false, "", "Show ffmpeg and ffprobe commands.", "Show CMD");

	AddHelp("", "gui_progress", "", "Displays messages that the GUI interprets to show the progress bar.");
	AddHelp("", "version", "", "Displays the current version of this program.", "v");
	AddHelp("", "help", "", "Displays help on commandline options.", "h");
	AddHelp("", "help-sub", "", "Displays subtitles options.");
	AddHelp("", "help-text", "", "Displays text block options.");
	AddHelp("", "help-time", "", "Displays frame times options.");
	AddHelp("", "help-vr", "", "Displays virtual reality options.");
	AddHelp("", "help-rename", "", "Displays rename options.");
	AddHelp("", "help-batch", "", "Displays batch mode options.");
	AddHelp("", "help-all", "", "Displays all the options.");

	HelpPrefixText.insert("sub", "\nSubtitles Options:\n");

	AddHelp("sub", "mode", "<time|line>", "Mode of operation, searches for frames to capture using different methods.");
	AddHelp("sub", "time", "", "Set time mode of operation.");
	AddHelp("sub", "line", "", "Set line mode of operation.");

	AddHelp("sub", "srt", "", "Force srt subtitle file format.", "", "Force srt", param_bool);
	AddHelp("sub", "ass", "", "Force ass subtitle file format.", "", "Force ass", param_bool);
	AddHelp("sub", "vtt", "", "Force vtt subtitle file format.", "", "Force vtt", param_bool);
	AddHelp("sub", "lang", "<code>", "Force subtitle language in ISO 639 language code.", "", "Force Subtitle Language",
			param_string);

	AddInt("min", 4, "min=0", false, "sub", "<number>",
		   "Minimum subtitle size in characters. Works differently depending on the mode.", "Min Subtitle Lenght");
	AddInt("start_line", 1, "min=1", false, "sub", "<number>",
		   "Subtitle line in which to start taking screenshots, only used in line mode", "Start Line");
	AddInt("end_line", -1, "", false, "sub", "<number>",
		   "Subtitle line in which to finish taking screenshots, if it is negative it is deducted from the total number of lines, "
		   "it is only used in line mode", "End Line");

	AddFont("sub_font", "DejaVu Sans", false, "sub", "Set the subtitles font name.", "Subtitle Font");
	AddStr("sub_fonts_dir", Fonts, "", false, "sub", "<dir>", "Set the aditional directory to search for fonts.",
		   "Subtitle Font Directory");
	AddColor("sub_color", "FFFFFF", false, "sub",
			 "Set the subtitles font color in RRGGBBAA format. Default: FFFFFF.", "Subtitle Color");
	AddInt("sub_size", 16, "min=4", false, "sub", "<px>", "Set the subtitles font size. Default: 16.",
		   "Subtitle Font Size");
	AddInt("sub_border", 1, "0=None|1=Outline|3=Line Box|4=Box", false, "sub", "<type>",
		   "Set the subtitles border:  0 - none, "
		   "1 - Outline, Default --sub_oc for outline color, "
		   "3 - Each line in one box, --sub_oc for background color, "
		   "4 - All lines in a box, --sub_bc for background color", "Subtitle Border");
	AddColor("sub_bc", "00000000", false, "sub", "Set the background color on border=4 in RRGGBBAA format. "
			 "Example: '-sub_oc 00000055' for tranparent black background. Default: 00000000.", "Subtitle Background Color");
	AddColor("sub_oc", "00000000", false, "sub",
			 "Set the outline and background color on border=3 in RRGGBBAA format. Default: 00000000.", "Subtitle Outline Color");
	AddBool("sub_force_ass", false, false, "sub", "Ignores the style inside the ass and force the configured style",
			"Force Style on ass");

	AddHelp("sub", "green", "", "Simple style, green subtitles text. Same as --sub_color 00FF00.", "", "Green Subtitles",
			param_bool);
	AddHelp("sub", "yellow", "", "Simple style, yellow subtitles text. Same as --sub_color FFEA00.", "",
			"Yellow Subtitles", param_bool);
	AddHelp("sub", "small", "", "Simple style, small subtitle text. Same as -sub_size 12.", "", "Small Subtitles",
			param_bool);
	AddHelp("sub", "big", "", "Simple style, big subtitle text. Same as --sub_size 24.", "", "Big Subtitles", param_bool);
	AddHelp("sub", "verybig", "", "Simple style, very big subtitle text. Same as --sub_size 32.", "", "Bigger Subtitles",
			param_bool);
	AddHelp("sub", "box", "", "Simple style, subtitle text inside a black box. Same as --sub_border 4.", "",
			"Box Background Subtitles", param_bool);
	AddHelp("sub", "tbox", "", "Simple style, subtitle text inside a black transparent box. "
			"Same as --sub_border 4 --sub_bc 00000050.", "", "Transparent Box Subtitles", param_bool);

	HelpPrefixText.insert("tb", "\nText block Options:\nThis options controls the aspect of the text block.\n\n");

	AddStr("tb", "top", "top|bottom|hidden", true, "tb", "<pos>", "Location of text block, top, bottom or hidden.",
		   "Text Blob Position");
	AddStr("tb_title", "%title%", "", false, "tb", "<text>",
		   "Set the title, default use the video or subtitle file name, can use text substitution.", "Text Block Title");
	AddInt("tb_border", 10, "min=0", false, "tb", "<px>", "Border size in pixels around the text and the logo.",
		   "Text Block Border");
	AddInt("tb_add_height", 0, "min=0", false, "tb", "<px>",
		   "Adds this number of pixels to the height automatically calculated based on the text.", "Text Block Add Height");
	AddStr("tb_comments",
		   "Made with %B1%Subtitles Contact Sheet%B0% "
		   "(https://gitlab.com/vongooB9/subtitles_contact_sheet_qt)", "", false, "tb",
		   "<text>",
		   "Set the comment text. It is a text that appears at the end of the text "
		   "block and in which you can put anything. Can use text substitution.", "Text Block Comments");

	AddHelp("tb", "tb_comment_append", "<text>", "Adds the specified text after the current comment text.", "",
			"Text Block Comments Append", param_string);
	AddHelp("tb", "tb_comment_prepend", "<text>", "Adds the specified text before the current comment text.", "",
			"Text Block Comments Prepend", param_string);

	AddFont("tb_font", "DejaVu Sans", false, "tb", "Set the font for text header.", "Text Block Font");
	AddDir("tb_fonts_dir", Fonts, false, "tb", "Set the additional directory to search for fonts",
		   "Text Block Fonts Directory");
	AddInt("tb_font_size", 24, "min=4", false, "tb", "<px>", "Set the font height for text header in pixels. Default 24.",
		   "Text Block Font Size");
	AddColor("tb_font_color", "909090", false, "tb",
			 "Set the color of the font for text header in RRGGBBAA format or color name. Default 909090.", "Text Block Font Color");
	AddInt("tb_font_border", 0, "min=0", false, "tb", "<px>", "Set the width in pixels of the border around the font.",
		   "Text Block Font Border");
	AddColor("tb_font_border_color", "000000", false, "tb",
			 "Set the color of the border around the font. Default 000000.", "Text Block Font Border Color");
	AddInt("tb_font_border_blur", 0, "min=0", false, "tb", "<amount>",
		   "When it is greater than 0 it blurs the border of text. Default 0.", "Text Block Font Border Blur");
	AddInt("tb_font_shadow", 0, "", false, "tb", "<px>",
		   "Shadow offset in pixels with respect to the text, Default 0 (disabled).", "Text Block Font Shadow");
	AddColor("tb_font_shadow_color", "000000", false, "tb", "Color of shadow, Default 000000.",
			 "Text Block Font Shadow Color");
	AddBool("tb_font_italic", true, false, "tb", "Italicizes the text. Default 1.", "Text Block Font Italic");
	AddBool("tb_font_bold", false, false, "tb", "Sets the text to bold. Default 0.", "Text Block Font Bold");
	AddStr("tb_font_pos", "up_left",
		   "up_left|up_right|down_left|down_right|up_center|down_center|middle_left|middle_right|middle_center", true, "tb",
		   "<position>",
		   "Alignment of the text within the block, it can be: up_left, up_right, down_left, "
		   "down_right, up_center, down_center, middle_left, middle_right, middle_center. Default up_left.",
		   "Text Block Font Position");
	AddInt("tb_font_left_margin", 0, "min=0", false, "tb", "<px>", "Margin to the left in pixels of the text. Default 0.",
		   "Text Block Font Left Margin");
	AddInt("tb_font_right_margin", 0, "min=0", false, "tb", "<px>", "Margin to the right in pixels of the text. Default 0.",
		   "Text Block Font Right Margin");
	AddInt("tb_font_vertical_margin", 0, "min=0", false, "tb", "<px>",
		   "Margin in pixels applied to the text, when the text is aligned up it is the "
		   "top margin, when aligned down it is the bottom margin. Default 0.", "Text Block Font Vertical Margin");

	AddFont("tb_title_font", "", false, "tb", "Set the font for the title. The default value is empty. "
			"When empty use the value of --tb_font.", "Title Font");
	AddInt("tb_title_font_size", 0, "min=0", false, "tb", "<px>",
		   "Set the font height for title in px. The default value is 0. "
		   "When 0 use the value of --tb_font_size.", "Title Font Size");
	AddColor("tb_title_color", "FFFFFF", false, "tb",
			 "Set the color of the font for the title. Default FFFFFF.", "Title Font Color");
	AddInt("tb_title_border", 0, "min=0", false, "tb", "<px>",
		   "Set the width in pixels of the border around the title. Default 0.", "Title Font Border");
	AddColor("tb_title_border_color", "000000", false, "tb",
			 "Set the color of the border around the title. Default 000000.", "Title Font Border Color");
	AddInt("tb_title_border_blur", 0, "min=0", false, "tb", "<amount>",
		   "When it is greater than 0 it blurs the border of title. Default 0.", "Title Font Border Blur");
	AddInt("tb_title_shadow", 0, "min=0", false, "tb", "<px>",
		   "Shadow offset in pixels for the title with respect to the text. "
		   "Default 0 (disabled).", "Title Font Shadow");
	AddColor("tb_title_shadow_color", "000000", false, "tb", "Color of shadow for the title, Default 000000.",
			 "Title Font Shadow Color");
	AddBool("tb_title_italic", false, false, "tb", "Italicizes the title. Default 0.", "Title Font Italic");
	AddBool("tb_title_bold", true, false, "tb", "Sets the title to bold. Default 1.", "Title Font Bold");
	AddStr("tb_title_pos", "up_left",
		   "up_left|up_right|down_left|down_right|up_center|down_center|middle_left|middle_right|middle_center", true, "tb",
		   "<position>",
		   "Alignment of the title within the block, it can be: up_left, up_right, "
		   "down_left, down_right, up_center, down_center, middle_left, middle_right, middle_center. Default up_left.",
		   "Title Position");
	AddInt("tb_title_left_margin", 0, "min=0", false, "tb", "<px>",
		   "Margin to the left in pixels of the title text. Default 0.", "Title Left Margin");
	AddInt("tb_title_right_margin", 0, "min=0", false, "tb", "<px>",
		   "Margin to the right in pixels of the title text. Default 0.", "Title Right Margin");
	AddInt("tb_title_vertical_margin", 0, "min=0", false, "tb", "<px>",
		   "Margin in pixels applied to the title, when the title is aligned "
		   "up it is the top margin, when aligned down it is the bottom margin. Default 0.", "Title Vertical Margin");

	AddFont("tb_comments_font", "", false, "tb", "Set the font for the comments. The default value is empty. "
			"When empty use the value of --tb_font.", "Comments Font");
	AddInt("tb_comments_font_size", 0, "min=0", false, "tb", "<px>",
		   "Set the font height for comments in px. The default value is 0. "
		   "When 0 use the value of --tb_font_size.", "Comments Font Size");
	AddColor("tb_comments_color", "505050", false, "tb",
			 "Set the color of the font for the comments. Default 505050.", "Comments Font Color");
	AddInt("tb_comments_border", 0, "min=0", false, "tb", "<px>",
		   "Set the size in px of the border around the comments. Default 0.", "Comments Font Border");
	AddColor("tb_comments_border_color", "000000", false, "tb",
			 "Set the color of the border around the comments. Default 000000.", "Comments Font Border Color");
	AddInt("tb_comments_border_blur", 0, "min=0", false, "tb", "<amount>",
		   "When it is greater than 0 it blurs the border of comments. Default 0.", "Comments Font Border Blur");
	AddInt("tb_comments_shadow", 0, "min=0", false, "tb", "<px>",
		   "Shadow offset in pixels for the comments with respect to the text. "
		   "Default 0 (disabled).", "Comments Font Shadow");
	AddColor("tb_comments_shadow_color", "000000", false, "tb",
			 "Color of shadow for the comments, Default 000000.", "Comments Font Shadow Color");
	AddBool("tb_comments_italic", true, false, "tb", "Italicizes the comments. Default 1.", "Comments Font Italic");
	AddBool("tb_comments_bold", false, false, "tb", "Sets the comments to bold. Default 0.", "Comments Font Bold");
	AddStr("tb_comments_pos", "up_left",
		   "up_left|up_right|down_left|down_right|up_center|down_center|middle_left|middle_right|middle_center", true, "tb",
		   "<position>", "Alignment of the comments within the block, it can be: "
		   "up_left, up_right, down_left, down_right, up_center, down_center, middle_left, middle_right, middle_center. Default up_left.",
		   "Comments Position");
	AddInt("tb_comments_left_margin", 0, "min=0", false, "tb", "<px>",
		   "Margin to the left in pixels of the comments text. Default 0.", "Comments Left Margin");
	AddInt("tb_comments_right_margin", 0, "min=0", false, "tb", "<px>",
		   "Margin to the right in pixels of the comments text. Default 0.", "Comments Right Margin");
	AddInt("tb_comments_vertical_margin", 0, "min=0", false, "tb", "<px>",
		   "Margin in pixels applied to the comments, when the comments is aligned "
		   "up it is the top margin, when aligned down it is the bottom margin. Default 0.", "Comments Vertical Margin");

	AddColor("tb_bg_color", "000000", false, "tb",
			 "Set the background color for text header in RRGGBBAA format or color name.", "Text Block Background Color");
	AddHelp("tb", "tb_transparent", "", "Set the background transparent. "
			"Same as --tb_bg_color 00000000 --grid_border_color 00000000 --format png", "", "Transparent Background", param_bool);
	AddStr("tb_bg_gradient", "none", "", false, "tb", "<gradient>",
		   "Generates a gradient as a background image for the text block with the indicated parameters.",
		   "Gradient as Text Block Background");

	AddHelp("tb", "tb_left", "", "Aligns text to the left. "
			"Same as --tb_title_pos up_left --tb_font_pos middle_left --tb_comments_pos down_left --tb_logo_pos right", "",
			"Text Block aligned to left", param_bool);
	AddHelp("tb", "tb_right", "", "Aligns text to the right. "
			"Same as --tb_title_pos up_right --tb_font_pos middle_right --tb_comments_pos down_right --tb_logo_pos left", "",
			"Text Block aligned to right", param_bool);

	AddFile("tb_bg_image", "", false, "tb", "Set the background image for the text header. "
			"if the image is smaller than the space to be occupied, it is repeated, if it is larger, it is cropped.",
			"Text Block Background Image");
	AddStr("tb_bg_image_halign", "center", "right|center|left|%", true, "tb", "<haling>",
		   "Horizontal alignment of background image, can be right, center, left or a percentage. Default center.",
		   "Horizontal Align for Text Block Bacground Image");
	AddStr("tb_bg_image_valign", "middle", "up|middle|down|%", true, "tb", "<valing>",
		   "Vertical alignment of background image, can be up, middle, down or a percentage. Default middle.",
		   "Vertical Align for Text Block Bacground Image");
	AddBool("tb_bg_image_scale", false, false, "tb", "Scale the background image before crop it. Default 0.",
			"Autoscale Text Block Background Image");
	AddBool("tb_bg_image_tile", true, false, "tb",
			"When the background image is smaller than the result it repeats it in tiles to fill the necessary space. Default 1.",
			"Tile Text Block Background Image");
	AddInt("tb_bg_image_blur", 0, "min=0", true, "tb", "<number>",
		   "Applies a blur filter to the background image. the higher the number the more blurred. Default 0 (disabled).",
		   "Blur Text block Background Image");
	AddInt("tb_bg_image_exposure", 0, "min=-300,max=300", true, "tb", "<number>",
		   "Applies an exposure filter to the background image. allows a number between -300 and 300, "
		   "negative numbers decrease the exposure making the image darker, positive numbers make the image lighter. "
		   "Default 0 (disabled).", "Exposure for Text block Background Image");
	AddInt("tb_bg_image_saturation", 100, "min=0,max=300", true, "tb", "<number>",
		   "Applies a saturation filter to the background image. Accepts a number between 0 and 300, "
		   "0 converts the image to black and white. Default 100 (disabled).", "Saturation for Text block Background Image");
	AddInt("tb_bg_image_opacity", 100, "min=0,max=100", true, "tb", "<number>",
		   "Percentage of transparency of the background image with respect to "
		   "the other backgrounds (solid color, gradient...). 100 is opaque, 0 totally transparent. Default 100",
		   "Opacity for Text block Background Image");
	AddStr("tb_bg_image_mask", "", "", false, "tb", "<color|gradient|file>",
		   "Mask applied to the background image to define the transparency of each part of the image. "
		   "It can be a solid color (the whole image has the same opacity), a gradient. Or an image file (untested). "
		   "In this option, white means totally opaque and black means totally transparent.",
		   "Mask for Text block Background Image");

	AddFile("tb_logo", "", false, "tb",
			"Put this logo in the header aligned to the right and rescaled to the header height.", "Text Block Logo");
	AddInt("tb_logo_height", 0, "min=0", false, "tb", "<px>",
		   "Force the logo to be this height in pixels, enlarge the entire text block if necessary.",
		   "Force Text Block Logo Height");
	AddStr("tb_logo_pos", "right", "left|right", false, "tb", "<left|right>",
		   "Position of the logo in the text block, it can be right or left. Default right.", "Text Block Logo Position");
	AddBool("tb_logo_text_overlay", false, false, "tb", "Allows the text to overlay the logo. Default 0.",
			"Text Block Logo Permit Overlay");

	AddFile("tb_cover_top_image", "", false, "tb",
			"Displays this cover on top of the text block, automatically adjusts it to the width respecting the aspect ratio.",
			"Text Block Cover Top Image");

	AddBool("tb_video_info", false, false, "tb",
			"Force video info in header, when show subtitles info, video info is hidden. This option force to show it.",
			"Show Video Info");
	AddBool("tb_chapters_list", false, false, "tb",
			"Enables the display of the list of chapters in the video file, disabled only shows its number.", "Show Chapters List");
	AddBool("tb_hide_video_info", false, false, "tb", "Don't show video information in text header.", "Hide Video Info");
	AddBool("tb_hide_sub_info", false, false, "tb", "Don't show subtitle information in text header.",
			"Hide Subtitles Info");
	AddBool("tb_hide_vr_warning", false, false, "tb", "Don't show the warning when enable VR options.", "Hide VR Warning");

	AddStr("tb_custom_subtitle",
		   "Format: %B1%%subtitle.format%%B0% Size: %B1%%human_size(subtitle.size)%%B0%$ Language: %B1%$%subtitle.language%$%B0%$"
		   "%N%Lines: %B1%%subtitle.lines%%B0% Length, Total: %B1%%subtitle.total_length%%B0% Avegare: %B1%%subtitle.avg_lenght%%B0%"
		   "%N%Duration, Video: %B1%%time(format.duration)%%B0% Subtitles: %B1%%time(subtitle.total_duration)%%B0%"
		   " Average Line: %B1%%subtitle.avg_duration_ms%ms%B0%", "", false, "tb", "<text>",
		   "Personalize the text displayed when the input is a subtitle.", "Custom Subtitles Text");

	AddStr("tb_custom_header",
		   "Duration: %B1%%time(format.duration)%%B0% "
		   "Size: %B1%%human_size(format.size)% (%numsep(format.size)% Bytes)%B0%"
		   "$ Bitrate: %B1%$%human_bitrate(format.bit_rate)%$%B0%$"
		   " Container: %B1%%format.format_long_name%%B0% ", "", false, "tb", "<text>",
		   "Personalize the text when the input is a video, only the basic part.", "Custom Video Header Text");

	AddStr("tb_custom_video_stream",
		   "$$%if(streams.multiple_same_type,=,1,Stream,)%$ %index% $"
		   "Video: %B1%%width%x%height%$ $%human_pixformat(pix_fmt)%$ bits$ %human_framerate(r_frame_rate)% fps"
		   "$ $%human_bitrate(bit_rate)%$$ %codec_long_name%%B0%", "", false, "tb", "<text>",
		   "Personalize video stream text lines.", "Custom Video Stream Text");

	AddStr("tb_custom_audio_stream",
		   "$$%if(streams.multiple_same_type,=,1,Stream,)%$ %index% $"
		   "Audio: %B1%$$%language_shortname(tags.language)%$$$ $%capitalize(channel_layout)%$$"
		   "$ $%human_sample_rate(sample_rate)%$$$ $%human_bitrate(bit_rate)%$$ %codec_long_name%%B0%", "", false, "tb", "<text>",
		   "Personalize audio stream text lines.", "Custom Audio Stream Text");

	AddStr("tb_custom_subtitle_stream",
		   "$$%if(streams.multiple_same_type,=,1,Stream,)%$ %index% $"
		   "Subtitle: %B1%$$%capitalize(tags.title)%$ - $$$%language_shortname(tags.language)%$ $%codec_long_name%%B0%", "", false,
		   "tb", "<text>", "Personalize subtitles stream text lines.", "Custom Suntitle Stream Text");

	AddStr("tb_custom_attachment_stream",
		   "$$%if(streams.multiple_same_type,=,1,Stream,)%$ %index% $"
		   "Attachment: %B1%%tags.filename%$ $%human_size(extradata_size)%$$$ ($%numsep(extradata_size)%$ Bytes)$"
		   " %tags.mimetype%%B0%", "", false, "tb", "<text>", "Personalize attachments text lines.",
		   "Custom Attachment Stream Text");

	AddStr("tb_custom_chapters", "$Chapters: %B1%$%chapters.count%$%B0%$", "", false, "tb", "<text>",
		   "Personalize the chapters count text.", "Custom Chapters Text");

	AddStr("tb_custom_chapters_list", "Chapter %B0%%time(start_time)%: %B1%%tags.title%%B0%", "", false, "tb", "<text>",
		   "Personalize the list of chapters text lines.", "Custom Chapters List Text");

	HelpPrefixText.insert("ft", "\nFrame Time Options:\n"
						  "This options controls the position and aspect of the time inside each frame.\n\n");

	AddBool("ft_hide", false, false, "ft", "Hide the time of frames.", "Hide Frame Time");
	AddFont("ft_font", "DejaVu Sans Mono", false, "ft", "Font for the time of frames.",
			"Frame Time Font");
	AddColor("ft_color", "white", false, "ft",
			 "Color of text for the time of frames, RRGGBBAA format or color name. Default white.", "Frame Time Font Color");
	AddInt("ft_font_border", 0, "min=0", false, "ft", "<px>",
		   "Set the width in pixels of the border around font. Default: 0.", "Frame Time Font Border");
	AddColor("ft_font_border_color", "black", false, "ft",
			 "Set the color of the border around the font. Default: black.", "Frame Time Font Border Color");
	AddColor("ft_shadow_color", "black", false, "ft",
			 "Set the color of the shadow of the font. Default: black.", "Frame Time Shadow Color");
	AddInt("ft_shadow_x", 0, "min=0", false, "ft", "<px>",
		   "Set the offset on the horizontal axis of the shadow with respect to the font. Default: 0.",
		   "Frame Time Shadow Horizontal Displacement");
	AddInt("ft_shadow_y", 0, "min=0", false, "ft", "<px>",
		   "Set the offset on the vertical axis of the shadow with respect to the font. Default: 0.",
		   "Frame Time Shadow Vertical Displacement");
	AddColor("ft_bg", "00000090", false, "ft", "Background color for the time of frames. Default 00000090.",
			 "Frame Time Background Color");
	AddInt("ft_size", 12, "min=4", false, "ft", "<px>", "Text size for the time of frames. Default 12.",
		   "Frame Time Font Size");
	AddInt("ft_border", 2, "min=0", false, "ft", "<px>", "Border in pixels for the time of frames. Default 2.",
		   "Frame Time Border");
	AddInt("ft_margin", 0, "min=0", false, "ft", "<px>", "Pixel spacing of the edges of the screen. Default 0.",
		   "Frame Time Margin");
	AddStr("ft_pos", "up_right", "up_left|up_right|down_left|down_right|up_center|down_center|middle_left|middle_right",
		   true, "ft", "<pos>", "Time of frames position up_left, up_right, down_left, down_right, "
		   "up_center, down_center, middle_left, middle_right.", "Frame Time Position");
	AddStr("ft_vertical", "disabled", "disabled|upward|downward", true, "ft", "<direction>",
		   "Print the time of frames in vertical position, it can be: disabled, upward, downward. Default disabled.",
		   "Frame Time Vertical Direcction");
	AddStr("ft_format", "", "", false, "ft", "<format>",
		   "Timestamp format, uses the same format characters as the C++ strftime function "
		   "(https://cplusplus.com/reference/ctime/strftime/) Special formats: "" is the HH:MM:SS.mmm format "
		   "\"short\" is the HH:MM:SS format, the same as %T or %H:%M:%S in strftime.", "Frame Time Format");
	AddStr("ft_custom", "", "", false, "ft", "<text>",
		   "Custom filter options for the time of frames. (https://ffmpeg.org/ffmpeg-filters.html#drawtext-1)",
		   "Custom Frame Time Parameters");

	HelpPrefixText.insert("vr", "\nVR Options:\n"
						  "This options allow you to correct the image of a VR video so that its content can be better appreciated.\n\n");

	AddBool("vr", false, false, "vr", "Enables the VR correction filter. (https://ffmpeg.org/ffmpeg-filters.html#v360)",
			"Enable VR");
	AddBool("vr_auto_enable", false, false, "vr",
			"Automatically activates the VR options when it detects a video with the correct name.", "Autoenable VR");
	AddBool("vr_only_crop", true, false, "vr",
			"If set to 1 it cuts only one of the eyes of the video, and deactivates the distortion correction.",
			"VR Only Discart One Eye");
	AddStr("vr_in", "hequirect", "hequirect|equirect|fisheye", false, "vr", "<format>", "Input format. Default hequirect",
		   "VR Input Format");
	AddStr("vr_out", "flat", "flat|hequirect|equirect|fisheye", false, "vr", "<format>", "Output format. Default flat.",
		   "VR Output Format");
	AddStr("vr_in_stereo", "sbs", "sbs|tb|2d", false, "vr", "<stereo>", "Input stereo mode, can be: sbs(default), tb, 2d.",
		   "VR Input Stereo Mode");
	AddStr("vr_out_stereo", "2d", "sbs|tb|2d", false, "vr", "<stereo>",
		   "Output stereo mode, Same options than input. Default 2d.", "VR Output Stereo Mode");
	AddInt("vr_ih_fov", 180, "min=0,max=360", false, "vr", "<degrees>",
		   "Set the input horizontal field of view. Default 180.", "VR Input Horizontal FOV");
	AddInt("vr_iv_fov", 180, "min=0,max=360", false, "vr", "<degrees>",
		   "Set the input vertical field of view. Default 180.", "VR Input Vertical FOV");
	AddInt("vr_d_fov", 105, "min=20,max=180", false, "vr", "<degrees>",
		   "Set the output diagonal field of view. Default 105.", "VR Output Diagonal FOV");
	AddInt("vr_h_fov", 0, "min=0,max=360", false, "vr", "<degrees>", "Set the output horizontal field of view. Default 0.",
		   "VR Output Horizontal FOV");
	AddInt("vr_v_fov", 0, "min=0,max=360", false, "vr", "<degrees>", "Set the output vertical field of view. Default 0.",
		   "VR Output Vertical FOV");
	AddInt("vr_yaw", 0, "min=-180,max=180", false, "vr", "<degrees>",
		   "Moves the camera to the right(positive value) and left(negative value).", "VR Camera Yaw");
	AddInt("vr_pitch", 0, "min=-180,max=180", false, "vr", "<degrees>",
		   "moves the camera upward(positive value) and downward(negative value).", "VR Camera Pitch");
	AddInt("vr_roll", 0, "min=-180,max=180", false, "vr", "<degrees>",
		   "rotates the camera counterclockwise(positive value) and clockwise(negative value)", "VR Camera Roll");
	AddStr("vr_w", "", "min=0", false, "vr", "<px>", "Set the output resolution width. Default the same as size.",
		   "VR Output Horizontal Resolution");
	AddStr("vr_h", "", "min=0", false, "vr", "<px>", "Set the output resolution height. Default the same as size.",
		   "VR Output Vertical Resolution");


	AddStr("vr_interp", "lanczos", "nearest|linear|lagrange9|cubic|lanczos|spline16|gaussian|mitchell", false, "vr",
		   "<int_method>", "Interpolation method. Default lanczos.", "VR Interpolation");
	AddBool("vr_h_flip", false, false, "vr", "Flip the output video horizontally", "VR Flip Output");
	AddBool("vr_right_eye", false, false, "vr", "Use the right eye. By default use the left.", "VR Show Right Eye");
	AddStr("vr_aditional", "", "", false, "vr", "<options>", "Pass aditional options to v360 ffmpeg filter.",
		   "VR Custom Aditional Options");

	AddHelp("vr", "fisheye200", "",
			"For fisheye 200FOV video files. Same as --vr --vr_in fisheye --vr_id_fov 0 --vr_ih_fov 200 --vr_iv_fov 200 --vr_in_stereo sbs.",
			"", "VR Fisheye 200 Format", param_bool);
	AddHelp("vr", "vr180", "",
			"For 180FOV video files. Same as --vr --vr_in hequirect --vr_id_fov 0 --vr_ih_fov 180 --vr_iv_fov 180 --vr_in_stereo sbs.",
			"", "VR 180 Format", param_bool);

	HelpPrefixText.insert("rename", "\nRename Options:\n");

	AddBool("rename", false, false, "rename", "Enables video renaming.", "Enable Renaming");
	AddStr("rename_type", "print", "print|move|copy|symlink|hardlink|shortcut|link|fastcopy", false, "rename", "<type>",
		   "Operation on the file system that will be executed, Can be print, move, copy, symlink, hardlink, shortcut, link, fastcopy.",
		   "Rename Type");
	AddStr("rename_dest", "", "", false, "rename", "<text>", "Path to the output file for renaming. Use text substitution.",
		   "Rename Destination");
	AddBool("rename_overwrite", false, false, "rename",
			"When activated it overwrites the output if it already exists. By default it fails.", "Remane Overwrite");
	AddBool("rename_mkdir", false, false, "rename",
			"When activated it creates all the intermediate folders to reach the output file. By default it fails if a folder that does not exist is used.",
			"Rename Create Directories");

	AddShortParam("l", "layout");
	AddShortParam("s", "size");
	AddShortParam("p", "process");

	HelpPrefixName.insert("", "General");
	HelpPrefixName.insert("sub", "Subtitles");
	HelpPrefixName.insert("vr", "Virtual Reality");
	HelpPrefixName.insert("tb", "Text Block");
	HelpPrefixName.insert("ft", "Frame Time");
	HelpPrefixName.insert("rename", "Rename");

	Settings = nullptr;
}

void ParamsSettings::AddStr(QString Param, QString DefaultVar, QString ValidValues, bool Required, QString Prefix,
							QString Type, QString Text, QString Name) {
	VarList.insert(Param, {DefaultVar, DefaultVar, false, Required, param_string, false, 0, "", "", ValidValues});
	AddHelp(Prefix, Param, Type, Text, "", Name, param_string);
}

void ParamsSettings::AddFile(QString Param, QString DefaultVar, bool Required, QString Prefix, QString Text,
							 QString Name) {
	VarList.insert(Param, {DefaultVar, DefaultVar, false, Required, param_file, false, 0, "", "", ""});
	AddHelp(Prefix, Param, "<file>", Text, "", Name, param_file);
}

void ParamsSettings::AddDir(QString Param, QString DefaultVar, bool Required, QString Prefix, QString Text,
							QString Name) {
	VarList.insert(Param, {DefaultVar, DefaultVar, false, Required, param_dir, false, 0, "", "", ""});
	AddHelp(Prefix, Param, "<dir>", Text, "", Name, param_dir);
}

void ParamsSettings::AddColor(QString Param, QString DefaultVar, bool Required, QString Prefix, QString Text,
							  QString Name) {
	VarList.insert(Param, {DefaultVar, DefaultVar, false, Required, param_color, false, 0, "", "", ""});
	AddHelp(Prefix, Param, "<color>", Text, "", Name, param_color);
}

void ParamsSettings::AddFont(QString Param, QString DefaultVar, bool Required, QString Prefix, QString Text,
							 QString Name) {
	VarList.insert(Param, {DefaultVar, DefaultVar, false, Required, param_font, false, 0, "", "", ""});
	AddHelp(Prefix, Param, "<font>", Text, "", Name, param_font);
}

void ParamsSettings::AddLayout(QString Param, QString DefaultVar, bool Required, QString Prefix, QString Text,
							   QString Name) {
	VarList.insert(Param, {DefaultVar, DefaultVar, false, Required, param_layout, false, 0, "", "", ""});
	AddHelp(Prefix, Param, "<CxR>", Text, "", Name, param_layout);
}

void ParamsSettings::AddInt(QString Param, int DefaultVar, QString ValidValues, bool Required, QString Prefix,
							QString Type, QString Text,
							QString Name) {
	QString Val = QString::number(DefaultVar);
	VarList.insert(Param, {Val, Val, false, Required, param_int, false, 0, "", "", ValidValues});
	AddHelp(Prefix, Param, Type, Text, "", Name, param_int);
}

void ParamsSettings::AddDouble(QString Param, double DefaultVar, QString ValidValues, bool Required, QString Prefix,
							   QString Type,
							   QString Text, QString Name) {
	QString Val = QString::number(DefaultVar, 'f');
	VarList.insert(Param, {Val, Val, false, Required, param_double, false, 0, "", "", ValidValues});
	AddHelp(Prefix, Param, Type, Text, "", Name, param_double);
}

void ParamsSettings::AddBool(QString Param, bool DefaultVar, bool Required, QString Prefix, QString Text,
							 QString Name) {
	QString Val;
	if (DefaultVar) Val = "1";
	else Val = "0";
	VarList.insert(Param, {Val, Val, false, Required, param_bool, false, 0, "", "", ""});
	AddHelp(Prefix, Param, "[0|1]", Text, "", Name, param_bool);
}

void ParamsSettings::AddUserDefined(QString Param, QString Name, int Num) {
	VarList.insert(Param, {"", "", false, false, param_string, true, Num, Name, "", ""});
	UserDefinedTranslator.insert(Name, Param);
}

void ParamsSettings::AddHelp(QString Prefix, QString Param, QString Type, QString Text,
							 QString ShortParam, QString Name, param_type DataType) {
	if (Text == "") return;
	HelpList.append({Prefix, Param, ShortParam, Type, Text, Name, DataType});
}

void ParamsSettings::AddShortParam(QString Short, QString Param) {
	ShortParams.insert("-" + Short, "--" + Param);
	for (int i = 0; i < HelpList.count(); ++i) {
		help_t Help = HelpList[i];
		if (Help.Param != Param) continue;
		Help.ShortParam = Short;
		HelpList.replace(i, Help);
		break;
	}
}

QString ParamsSettings::Value(QString Param) {
	if (!VarList.contains(Param)) return "";
	params_t Var = VarList.value(Param);
	if (Var.Required && Var.Value == "")
		return Var.DefaultValue;
	else
		return Var.Value;
}

bool ParamsSettings::Bool(QString Param) {
	if (VarList.contains(Param)) {
		params_t Var = VarList.value(Param);
		if (Var.Type == param_bool)
			return Var.Value != "0";
	}
	return false;
}

int ParamsSettings::Int(QString Param) {
	if (VarList.contains(Param)) {
		params_t Var = VarList.value(Param);
		if (Var.Type == param_int)
			return Var.Value.toInt();
	}
	return 0;
}

double ParamsSettings::Double(QString Param) {
	if (VarList.contains(Param)) {
		params_t Var = VarList.value(Param);
		if (Var.Type == param_double)
			return Var.Value.toDouble();
	}
	return 0;
}

bool ParamsSettings::NotEmpty(QString Param) {
	if (VarList.contains(Param))
		return VarList.value(Param).Value != "";
	return false;
}

bool ParamsSettings::Exists(QString Param) {
	return VarList.contains(Param);
}

void ParamsSettings::setValue(QString Param, QString Value, bool DetectFunction) {
	if (!VarList.contains(Param)) return;
	params_t Var = VarList.value(Param);
	Var.Changed = true;
	if (DetectFunction && TextSustitution::DetectFunction(Value)) {
		Var.Function = Value;
	} else {
		Var.Value = Value;
		if (DetectFunction) Var.Function = "";
	}
	VarList[Param] = Var;
	PrintDebug("\t--" + Param + " = " + Value);
}

void ParamsSettings::setValueInt(QString Param, int Value) {
	setValue(Param, QString::number(Value));
}

void ParamsSettings::setValueDouble(QString Param, double Value) {
	setValue(Param, QString::number(Value));
}

void ParamsSettings::setValueBool(QString Param, bool Value) {
	setValue(Param, QString::number(Value));
}

QStringList ParamsSettings::Params() {
	return VarList.keys();
}

ParamsSettings::param_type ParamsSettings::Type(QString Param) {
	return VarList.value(Param).Type;
}

QString ParamsSettings::Function(QString Param) {
	if (!VarList.contains(Param)) return "";
	params_t Var = VarList.value(Param);
	return Var.Function;
}

bool ParamsSettings::IsUserDefined(QString Param) {
	if (!VarList.contains(Param)) return false;
	params_t Var = VarList.value(Param);
	return Var.UserDefined;
}

void ParamsSettings::SetConfig(QString Name) {
	if (Settings != nullptr) Settings->deleteLater();
	#ifdef Q_OS_WIN
	QString File = ConfigPath + QDir::separator() + Name + "." + ConfigExt;
	Settings = new QSettings(File, QSettings::IniFormat, this);
	ConfigName = "";
	PrintDebug("Using config: " + File);
	#else
	Settings = new QSettings("subtitles_contact_sheet", Name, this);
	ConfigName = Name;
	PrintDebug("Using config: " + Name);
	#endif
}

void ParamsSettings::SetConfigFile(QString File) {
	if (Settings != nullptr) Settings->deleteLater();
	Settings = new QSettings(File, QSettings::IniFormat, this);
	ConfigName = "";
	PrintDebug("Using config: " + File);
}

bool ParamsSettings::ConfigExists(QString Name) {
	QDir ConfigDir(ConfigPath);
	if (ConfigDir.exists()) {
		QFileInfoList List = ConfigDir.entryInfoList({"*." + ConfigExt}, QDir::Files | QDir::NoDot | QDir::NoDotDot,
							 QDir::Time | QDir::Reversed);
		for (int i = 0; i < List.count(); ++i) {
			QFileInfo file = List.value(i);
			if (Name == file.baseName()) return true;
		}
	}
	return false;
}

QStringList ParamsSettings::ConfigList() {
	QStringList Result;
	QDir ConfigDir(ConfigPath);
	if (ConfigDir.exists()) {
		QFileInfoList List = ConfigDir.entryInfoList({"*." + ConfigExt}, QDir::Files | QDir::NoDot | QDir::NoDotDot,
							 QDir::Time | QDir::Reversed);
		for (int i = 0; i < List.count(); ++i) {
			QFileInfo fileinfo = List.value(i);
			if (fileinfo.baseName() == "config") continue;
			QSettings *File = new QSettings(fileinfo.absoluteFilePath(), QSettings::IniFormat, this);
			Result.append(fileinfo.baseName() + ": " + File->value("config_desc", "").toString());
			File->deleteLater();
		}
	}
	Result.sort();
	return Result;
}

void ParamsSettings::ReadFromConfigFile() {
	if (Settings == nullptr) return;
	QStringList SettingsKeys = Settings->allKeys();
	for (int i = 0; i < SettingsKeys.size(); ++i) {
		QString Key = SettingsKeys.at(i);
		QString NewKey = Key;
		if (Key.startsWith("VAR_")) {
			QRegularExpressionMatch match = RegexpUserDefinedVar.match(Key);
			int num;
			QString Varname = "";
			if (match.hasMatch()) {
				if (match.captured(4) != "") {
					num = GetUserDefinedVarCount();
					NewKey = "VAR_" + QString::number(num).rightJustified(3, '0') + "_" + match.captured(4);
					Varname = match.captured(4);
				} else {
					num = match.captured(1).toInt();
					if (match.captured(1).length() < 3)
						NewKey.replace(match.captured(1), match.captured(1).rightJustified(3, '0'));
					if (UserDefinedVarOrderCount < num) UserDefinedVarOrderCount = num;
					Varname = match.captured(3);
				}
				AddUserDefined(NewKey, Varname, num);
			}
		}
		if (!VarList.contains(NewKey)) continue;
		params_t Var = VarList.value(NewKey);
		if (TextSustitution::DetectFunction(Settings->value(Key).toString())) {
			Var.Function = Settings->value(Key).toString();
			Var.Value = Var.DefaultValue;
		} else {
			switch (Var.Type) {
				case param_string:
				case param_file:
				case param_color:
				case param_font:
				case param_dir:
				case param_layout:
					Var.Value = Settings->value(Key).toString();
					break;
				case param_int:
					Var.Value = QString::number(Settings->value(Key).toInt());
					break;
				case param_bool:
					if (Settings->value(Key).toBool())
						Var.Value = "1";
					else
						Var.Value = "0";
					break;
				case param_double:
					Var.Value = QString::number(Settings->value(Key).toDouble(), 'f');
					break;
				case param_time:
				case param_shortcut:
					//TODO
					//Var.Value = Settings->value(Key).toTime();'
					break;
			}
		}

		if (NewKey != Key) {
			Settings->remove(Key);
			Settings->setValue(NewKey, Var.Value);
		}
		Var.Changed = false;
		VarList[NewKey] = Var;
		PrintDebug("\t" + NewKey + " = " + Var.Value);
	}

	//Convert old comments, remove in future versions
	int comments_size = Settings->beginReadArray("TB_COMMENTS");
	if (comments_size > 0) {
		QString Comments = "";
		for (int i = 0; i < comments_size; i++) {
			Settings->setArrayIndex(i);
			QString Title = Settings->value("title", "").toString();
			QString Text = Settings->value("text", "").toString();
			QString Comment = "";
			if (Title != "") Comment += "%#%" + Title + ":%B1% ";
			if (Text != "") Comment += Text + "%B0%";
			if (Comment != "") {
				if (Comments != "") Comments += "%N%";
				Comments += Comment;
			}
		}
		Settings->setValue("tb_comments", Comments);
		Settings->remove("TB_COMMENTS");
		Settings->remove("tb_comment");
		Settings->remove("tb_comment2");
		Settings->remove("tb_comment_title");
		Settings->remove("tb_comment2_title");
		Settings->sync();
	}
	Settings->endArray();
}

void ParamsSettings::UpdateConfigFile(bool Sync, bool All) {
	QStringList Keys = VarList.keys();
	for (int i = 0; i < Keys.size(); ++i) {
		UpdateConfigFile(Keys.at(i), All);
	}
	if (Sync) Settings->sync();
}

void ParamsSettings::UpdateConfigFile(QString Param, bool All) {
	if (!VarList.contains(Param)) return;
	if (Settings->contains(Param) && VarList.value(Param).Changed == false) return;
	if (VarList.value(Param).Function != "") {
		Settings->setValue(Param, VarList.value(Param).Function);
	} else {
		if (!All && VarList.value(Param).Value == VarList.value(Param).DefaultValue) {
			Settings->remove(Param);
			return;
		}
		Settings->setValue(Param, VarList.value(Param).Value);
	}
}

void ParamsSettings::PrintParams(bool Changed, bool Default) {
	QHashIterator<QString, params_t> i(VarList);
	while (i.hasNext()) {
		i.next();
		if (Changed && !i.value().Changed) continue;
		if (!Default && i.value().Value == i.value().DefaultValue) continue;
		PrintInfo(i.key() + ": " + i.value().Value);
	}
}

bool ParamsSettings::ProcessParameter(int &Current, int Count, char *Params[]) {
	QString Param(Params[Current]);
	QString NextParam = "";
	bool hasNext = false;
	if (Current + 1 < Count) {
		NextParam = Params[Current + 1];
		hasNext = true;
	}
	bool usedNext = false;
	bool Result = ProcessParameter(Param, hasNext, NextParam, usedNext);
	if (usedNext) Current++;
	return Result;
}

bool ParamsSettings::ProcessParameter(QString Param, bool HasNext, QString NextParam, bool &UsedNext) {
	UsedNext = false;
	if (ShortParams.contains(Param)) Param = ShortParams.value(Param);

	if (!Param.startsWith("--")) {
		//		PrintError("Parameter Error: " + Param);
		return false;
	}
	Param.remove(0, 2);
	if (!VarList.contains(Param)) {
		//		PrintError("Parameter Not Found: " + Param);
		return false;
	}
	params_t Var = VarList.value(Param);

	if (Var.Type == param_bool) {
		bool HasParam = false;
		QString NextValue = NextParam;
		if (HasNext) {
			if (TextSustitution::DetectFunction(NextParam)) {
				Var.Function = NextParam;
				NextValue = Var.Value;
			}
			if (NextValue == "1" || NextValue == "0") HasParam = true;
		}
		if (HasParam) {
			Var.Value = NextValue;
			UsedNext = true;
		} else {
			if (Var.DefaultValue == "0")
				Var.Value = "1";
			else
				Var.Value = "0";
		}
	} else {
		UsedNext = true;
		QString Value = NextParam;
		if (!HasNext) {
			PrintError("Parameter Incorect value: " + Param);
			return false;
		}
		if (TextSustitution::DetectFunction(Value)) {
			Var.Function = Value;
			Value = Var.DefaultValue;
		}
		if (Var.Required && Value == "") {
			PrintError("Parameter: " + Param + " can't be empty");
			return false;
		}
		Var.Value = Value;
	}
	Var.Changed = true;
	VarList[Param] = Var;
	PrintDebug("\t --" + Param + " = " + Var.Value);
	return true;
}

void ParamsSettings::UserDefinedVar(QString Param, context_type Context) {
	bool overwrite = false;
	QString VarName = Param.section("=", 0, 0);
	QString SaveName;
	if (VarName != "") {
		if (Param.contains("==")) overwrite = true;
		QString Value = Param.section("=", 1, -1, QString::SectionSkipEmpty);
		QStringList ConfigNames = UserDefinedTranslator.values(VarName);
		if (overwrite && !ConfigNames.isEmpty()) {
			ConfigNames.sort();
			SaveName = ConfigNames.first();
			PrintDebug("\t\t" + SaveName + ": Change Variable " + VarName + " = " + Value);
		} else {
			int NewID = GetUserDefinedVarCount();
			SaveName = "VAR_" + QString::number(NewID).rightJustified(3, '0') + "_" + Context2Str(Context) + VarName;
			AddUserDefined(SaveName, VarName, NewID);
			PrintDebug("\t\t" + SaveName + ": Add Variable " + VarName + " = " + Value);
		}
		params_t Var = VarList.value(SaveName);
		if (TextSustitution::DetectFunction(Value)) {
			Var.Function = Value;
		} else {
			Var.Value = Value;
			Var.Changed = true;
		}
		VarList[SaveName] = Var;
	}
}

int ParamsSettings::GetUserDefinedVarCount() {
	return ++UserDefinedVarOrderCount;
}

bool ParamsSettings::GetVarAndContext(QString Name, QString &Var, context_type &Context) {
	QRegularExpressionMatch match = RegexpUserDefinedVar.match(Name);
	if (match.hasMatch()) {
		//		int num = match.captured(1).toInt();
		Var = match.captured(3);
		Context = Str2Context(match.captured(2));
		return true;
	}
	return false;
}

void ParamsSettings::printhelp(QString AppName, QString prefix) {
	int ConsoleWidth = 120;

	#ifdef Q_OS_WIN
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	int ret;
	ret = GetConsoleScreenBufferInfo(GetStdHandle( STD_OUTPUT_HANDLE ), &csbi);
	if (ret) ConsoleWidth = csbi.dwSize.X;
	#else
	struct winsize w;
	ioctl(0, TIOCGWINSZ, &w);
	ConsoleWidth = w.ws_col;
	#endif

	QString text;
	if (HelpPrefixText.contains(prefix))
		text = HelpPrefixText.value(prefix);

	fprintf(stdout, text.toUtf8().constData(), AppName.toUtf8().constData());

	int maxlengt = 0;
	int newlinelength = 0;
	QList<QString> List;
	QList<QString> TextList;
	for (int i = 0; i < HelpList.count(); ++i) {
		help_t helpline = HelpList.at(i);
		if (helpline.Prefix != prefix) continue;
		QString lineformat = "--" + helpline.Param;
		if (helpline.Type != "") lineformat += " " + helpline.Type;
		if (helpline.ShortParam != "") lineformat = "-" + helpline.ShortParam + ", " + lineformat;
		if (lineformat.length() > maxlengt) maxlengt = lineformat.length();
		List.append(lineformat);
		TextList.append(helpline.Text);
	}

	newlinelength = maxlengt + 2;

	for (int i = 0; i < TextList.count(); ++i) {
		QString Text = TextList.at(i);
		QStringList Words = Text.split(" ");
		int maxlinelength = ConsoleWidth - newlinelength;
		int linelength = 0;
		QString newtext = "";
		for (int i2 = 0; i2 < Words.count(); ++i2) {
			if (linelength + Words.at(i2).length() + 1 >= maxlinelength) {
				linelength = 0;
				newtext += "\n" + QString("").leftJustified(newlinelength);
			}
			if (newtext != "") {
				newtext += " ";
				linelength++;
			}
			newtext += Words.at(i2);
			linelength += Words.at(i2).length();
		}
		TextList[i] = newtext;
	}

	QString LineFormat = "  %-*s %s\n";

	for (int i = 0; i < List.count(); ++i) {
		fprintf(stdout, LineFormat.toUtf8().constData(), maxlengt, List.at(i).toUtf8().constData(),
				TextList.at(i).toUtf8().constData());
	}
	fprintf(stdout, "\n");
}

QList<ParamsSettings::help_t> ParamsSettings::SearchHelp(QString Search, bool OptionsOnly) {
	//	if (Search == "") return HelpList;
	QList<help_t> ResultParam;
	QList<help_t> ResultText;
	for (int i = 0; i < HelpList.count(); ++i) {
		help_t help = HelpList.at(i);
		if (help.Name == "") continue;
		if (OptionsOnly) {
			if (!VarList.contains(help.Param)) continue;
		}
		if (Search == "")
			ResultParam.append(help);
		else if (help.Param.contains(Search, Qt::CaseInsensitive) || help.Name.contains(Search, Qt::CaseInsensitive))
			ResultParam.append(help);
		else if (help.Text.contains(Search, Qt::CaseInsensitive))
			ResultText.append(help);
	}
	if (!ResultText.isEmpty()) ResultParam.append(ResultText);
	return ResultParam;
}

ParamsSettings::params_t ParamsSettings::GetVar(QString Param) {
	return VarList.value(Param);
}

void ParamsSettings::SetVar(QString Param, params_t &value) {
	VarList[Param] = value;
}

ParamsSettings::help_t ParamsSettings::GetHelp(QString Param) {
	for (int i = 0; i < HelpList.count(); ++i) {
		help_t help = HelpList.at(i);
		if (help.Param == Param)
			return help;
	}
	return help_t();
}

QString ParamsSettings::GetPrefixName(QString Prefix) {
	return HelpPrefixName.value(Prefix, "");
}

void ParamsSettings::DeleteConfig(QString Config) {
	QString File = QDir::toNativeSeparators(ConfigPath + QDir::separator() + Config + "." + ConfigExt);
	if (QFile::exists(File)) QFile::remove(File);
}

QString ParamsSettings::Context2Str(ParamsSettings::context_type Context) {
	switch (Context) {
		case context_global:
			return "";
		case context_stream:
			return "S_";
		case context_video:
			return "SV_";
		case context_audio:
			return "SA_";
		case context_subtitle:
			return "SS_";
		case context_attachment:
			return "SF_";
	}
	return "";
}

ParamsSettings::context_type ParamsSettings::Str2Context(QString Context) {
	if (Context == "") return context_global;
	if (Context == "S_") return context_stream;
	if (Context == "SV_") return context_video;
	if (Context == "SA_") return context_audio;
	if (Context == "SS_") return context_subtitle;
	if (Context == "SF_") return context_attachment;
	PrintError("Incorrect context \"" + Context + "\"");
	return context_global;
}

