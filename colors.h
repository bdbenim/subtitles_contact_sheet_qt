#ifndef COLORS_H
#define COLORS_H
#include <QString>

struct color_t {
	uint8_t R;
	uint8_t G;
	uint8_t B;
	uint8_t A;
	bool Valid;
};


color_t String2Color(QString StrColor);
QString Color2RGB(color_t Color);
QString Color2BGR(color_t Color);
QString ColorChannel2Hex(uint8_t Channel);
QString StringColor2BGR(QString InColor);
QString StringColor2RGB(QString InColor);
QString SetTransparentRGB(QString StrColor);

#endif // COLORS_H
