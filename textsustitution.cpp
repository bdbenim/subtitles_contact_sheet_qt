#include "textsustitution.h"
#include <QRegularExpression>
#include <QFile>
#include <QJsonArray>
#include "iso639.h"
#include "common.h"
#include "colors.h"

QRegularExpression
SearchTextEmpty("(?<!\\\\)\\$([^\\$]*)(?<!\\\\)\\$(?<!\\\\)\\%([^\\$\\%]+)(?<!\\\\)\\%(?<!\\\\)\\$([^\\$]*)(?<!\\\\)\\$");
QRegularExpression SearchSimpleText("(?<!\\\\)\\%([^\\$\\%]+)\\%");
QRegularExpression SearchFormatText("\\%([a-zA-Z0-9\\-_.,#]+)\\%");
QRegularExpression FunctionSearch("^([a-z_]+)\\((.*)\\)>?([^\\$\\%]*)$");
QRegularExpressionMatch Match;
QRegularExpressionMatchIterator Matches;
QRegularExpressionMatch FunctionMatch;

TextSustitution::TextSustitution(QObject *parent)
	: QObject{parent} {
	Local = new QLocale("EN");

	PixelFormatDatabase.insert("yuv420p", {8, "4:2:0", "YUV"});
	PixelFormatDatabase.insert("yuvj420p", {8, "4:2:0", "YUV"});
	PixelFormatDatabase.insert("yuv422p", {8, "4:2:2", "YUV"});
	PixelFormatDatabase.insert("yuvj422p", {8, "4:2:2", "YUV"});
	PixelFormatDatabase.insert("yuv444p", {8, "4:4:4", "YUV"});
	PixelFormatDatabase.insert("yuvj444p", {8, "4:4:4", "YUV"});
	PixelFormatDatabase.insert("gbrp", {8, "4:4:4", "GBR"});
	PixelFormatDatabase.insert("yuv420p10le", {10, "4:2:0", "YUV"});
	PixelFormatDatabase.insert("yuv422p10le", {10, "4:2:2", "YUV"});
	PixelFormatDatabase.insert("yuv444p10le", {10, "4:4:4", "YUV"});
	PixelFormatDatabase.insert("gbrp10le", {10, "4:4:4", "GBR"});
	PixelFormatDatabase.insert("yuv420p12le", {12, "4:2:0", "YUV"});
	PixelFormatDatabase.insert("yuv422p12le", {12, "4:2:2", "YUV"});
	PixelFormatDatabase.insert("yuv444p12le", {12, "4:4:4", "YUV"});
	PixelFormatDatabase.insert("gbrp12le", {12, "4:4:4", "GBR"});
	PixelFormatDatabase.insert("gray", {8, "", "Y"});
	PixelFormatDatabase.insert("gray10le", {10, "", "Y"});
	PixelFormatDatabase.insert("gray12le", {12, "", "Y"});
	PixelFormatDatabase.insert("nv12", {8, "4:2:0", "YUV"});
	PixelFormatDatabase.insert("nv16", {8, "4:2:2", "YUV"});
	PixelFormatDatabase.insert("nv21", {8, "4:2:0", "YUV"});
	PixelFormatDatabase.insert("nv20le", {8, "4:2:2", "YUV"});
	PixelFormatDatabase.insert("yuva420p", {8, "4:2:0", "YUV"});

}

QString TextSustitution::ScapeCaracters(QString in) {
	in.replace("%", "\\%");
	in.replace("$", "\\$");
	return in;
}

void TextSustitution::SetReplaceDir(QString dir) {
	ReplaceDir = dir;
}

void TextSustitution::LoadVars(QJsonObject Object, QString Name) {

	for (QJsonObject::const_iterator i = Object.begin(); i != Object.end(); ++i) {
		if (i.value().isUndefined()) continue;
		if (i.value().isString()) Vars.insert(Name + "." + i.key(), ScapeCaracters(i.value().toString()));
		if (i.value().isDouble()) Vars.insert(Name + "." + i.key(), QString::number(i.value().toDouble()));
		if (i.value().isBool()) Vars.insert(Name + "." + i.key(), QString::number(i.value().toBool()));

		if (i.value().isObject()) {
			QString levelname;
			if (Name != "")
				levelname = Name + "." + i.key();
			else
				levelname = i.key();

			LoadVars(i.value().toObject(), levelname);
		}
		if (i.value().isArray()) {
			QJsonArray array = i.value().toArray();
			QString arrayname;
			if (Name != "")
				arrayname = Name + "." + i.key();
			else
				arrayname = i.key();

			Vars.insert(arrayname + "." + "count", QString::number(array.count()));
			for (int i2 = 0; i2 < array.count(); i2++) {
				QString levelname = arrayname + "." + QString::number(i2);
				LoadVars(array.at(i2).toObject(), levelname);
			}
		}
	}
}

QString TextSustitution::PrefixConverter(double Value, bool Binary, uint8_t Decimals, bool Separated, bool Formated,
		QStringList Units, QList<uint8_t> DecimalsList) {
	uint8_t uinitindex = 0;
	int divisor = 1000;
	if (Binary) divisor = 1024;
	while (Value >= divisor) {
		uinitindex++;
		Value = Value / divisor;
		if (Units.size() - 1 == uinitindex) break;
	}

	QString unit = Units.value(uinitindex);
	if (Separated) unit = " " + unit;

	if (Value - (int)Value == 0)
		Decimals = 0;
	else if (!DecimalsList.isEmpty() && DecimalsList.count() > uinitindex)
		Decimals = DecimalsList.value(uinitindex);

	if (Formated)
		return Local->toString(Value, 'f', Decimals) + unit;
	else
		return QString::number(Value, 'f', Decimals) + unit;

}

QString TextSustitution::Operation(QString Operation, QString Value, QStringList Params, QString Context) {
	QString Operator;
	if (!Params.isEmpty()) Operator = Params.takeFirst();
	Operator = ValueCheckContext(Operator, Context);
	QString Decimals = 0;
	if (!Params.isEmpty()) Decimals = Params.takeFirst();
	Decimals = ValueCheckContext(Decimals);
	bool Formated = false;
	if (!Params.isEmpty()) Formated = Params.takeFirst().toInt() != 0;
	bool ok;
	double op1 = Value.toDouble(&ok);
	if (!ok) return "";
	double op2 = Operator.toDouble(&ok);
	if (!ok) return "";
	double Result = 0;
	if (Operation == "div") Result = op1 / op2;
	if (Operation == "mul") Result = op1 * op2;
	if (Operation == "sum") Result = op1 + op2;
	if (Operation == "res") Result = op1 - op2;
	if (Operation == "percent") Result = op1 / op2 * 100;
	if (qIsNaN(Result)) return "";
	if (Formated)
		return Local->toString(Result, 'f', Decimals.toInt());
	else
		return QString::number(Result, 'f', Decimals.toInt());
}

QString TextSustitution::ProcessFunctions(QString Function, QStringList Params, QString Context, bool TextFormat) {

	QString Param = Params.takeFirst();
	QString Result = "";

	if (Context != "" && Vars.contains(Context + Param)) {
		Result = Vars.value(Context + Param);
	} else if (Vars.contains(Param)) {
		Result = Vars.value(Param);
	}

	if (Function == "upper") return Result.toUpper();

	if (Function == "lower") return Result.toLower();

	if (Function == "trim") return Result.trimmed();

	if (Function == "capitalize") {
		if (Result == "") return "";
		Result[0] = Result[0].toUpper();
		return Result;
	}

	if (Function == "time") {
		if (Result == "") return "";
		return QTime::fromMSecsSinceStartOfDay(Result.toDouble() * 1000).toString("hh:mm:ss");
	}

	if (Function == "human_size") {
		if (Result == "") return "";
		return Local->formattedDataSize(Result.toLongLong(), 2, QLocale::DataSizeIecFormat);
	}

	if (Function == "numsep") {
		if (Result == "") return "";
		return Local->toString(Result.toLongLong());
	}

	if (Function == "human_framerate") {
		if (Result == "") return "";
		if (!Result.contains('/')) return Result;
		QStringList SplitFrameRate = Result.split('/');
		double div1 = SplitFrameRate.first().toInt();
		double div2 = SplitFrameRate.last().toInt();
		double FrameRate = div1 / div2;
		if (FrameRate == 0) return "";
		uint8_t decimals = 2;
		if (FrameRate - (int)FrameRate == 0)
			decimals = 0;
		return QString::number(FrameRate, 'f', decimals);
	}

	if (Function == "human_bitdepth") {
		if (PixelFormatDatabase.contains(Result)) {
			return QString::number(PixelFormatDatabase.value(Result).bits);
		}
		return "";
	}

	if (Function == "human_colormodel") {
		if (PixelFormatDatabase.contains(Result)) {
			return PixelFormatDatabase.value(Result).Mode;
		}
		return "";
	}

	if (Function == "human_chromasub") {
		if (PixelFormatDatabase.contains(Result)) {
			return PixelFormatDatabase.value(Result).Chroma;
		}
		return "";
	}

	if (Function == "human_pixformat") {
		if (PixelFormatDatabase.contains(Result)) {
			pix_fmt_data Data = PixelFormatDatabase.value(Result);
			Result = Data.Mode;
			if (Data.Chroma != "") Result += " " + Data.Chroma;
			Result += " " + QString::number(Data.bits);
			return Result;
		}
		return "";
	}

	if (Function == "prefix") {
		if (Result == "") return "";
		//Example "%prefix(format.size,0,2,1,0,Bytes,kBytes,MBytes,GBytes,0,0,2,3)%"
		double value = Result.toDouble();
		if (value == 0) return "";
		bool Binary = false;
		if (!Params.isEmpty()) Binary = Params.takeFirst().toInt() != 0;
		uint8_t Decimals = 3;
		if (!Params.isEmpty()) Decimals = Params.takeFirst().toInt();
		bool Separated = false;
		if (!Params.isEmpty()) Separated = Params.takeFirst().toInt() != 0;
		bool Formated = false;
		if (!Params.isEmpty()) Formated = Params.takeFirst().toInt() != 0;

		QStringList Texts;
		QList<uint8_t> DecimalsList;

		while (!Params.isEmpty()) {
			QString param = Params.takeFirst();
			bool ok = false;
			uint8_t Dec = param.toUInt(&ok);
			if (ok) DecimalsList.append(Dec);
			else Texts.append(param);
		}

		return PrefixConverter(value, Binary, Decimals, Separated, Formated, Texts, DecimalsList);
	}

	if (Function == "human_bitrate") {
		if (Result == "") return "";
		double bitrate = Result.toDouble();
		if (bitrate == 0) return "";
		return PrefixConverter(bitrate, false, 3, true, true, {"bit/s", "kbit/s", "Mbit/s", "Gbit/s"}, {0, 0, 2, 3});
	}

	if (Function == "human_sample_rate") {
		if (Result == "") return "";
		double sample = Result.toDouble();
		if (sample == 0) return "";
		return PrefixConverter(sample, false, 2, true, true, {"Hz", "kHz", "MHz", "GHz"}, {0, 1, 2, 2});
	}

	if (Function == "language_name") {
		if (Result == "") return "";
		if (Result == "und") return "";
		return Iso639ToLangName(Result);
	}

	if (Function == "language_shortname") {
		if (Result == "") return "";
		if (Result == "und") return "";
		return Iso639ToShortName(Result);
	}

	if (Function == "if") {
		//Example "%if(index,=,0,Stream:\\N,\\h\\h)%"

		QString Operation;
		if (!Params.isEmpty()) Operation = Params.takeFirst();
		QString CompareValue;
		if (!Params.isEmpty()) CompareValue = Params.takeFirst();
		QString iftrue = "";
		if (!Params.isEmpty()) iftrue = Params.takeFirst();
		QString ifelse = "";
		if (!Params.isEmpty()) ifelse = Params.takeFirst();

		CompareValue = ValueCheckContext(CompareValue, Context);

		iftrue = ValueCheckContext(iftrue, Context);
		ifelse = ValueCheckContext(ifelse, Context);

		if (Operation == "=") return (Result == CompareValue) ? iftrue : ifelse;
		if (Operation == "<") return (Result.toDouble() < CompareValue.toDouble()) ? iftrue : ifelse;
		if (Operation == ">") return (Result.toDouble() > CompareValue.toDouble()) ? iftrue : ifelse;
		if (Operation == "<=") return (Result.toDouble() <= CompareValue.toDouble()) ? iftrue : ifelse;
		if (Operation == ">=") return (Result.toDouble() >= CompareValue.toDouble()) ? iftrue : ifelse;
		if (Operation == "!=") return (Result != CompareValue) ? iftrue : ifelse;
		if (Operation == "contains") return (Result.contains(CompareValue)) ? iftrue : ifelse;
		if (Operation == "lenght<") return (Result.length() < CompareValue.toInt()) ? iftrue : ifelse;
		if (Operation == "length>") return (Result.length() > CompareValue.toInt()) ? iftrue : ifelse;
		if (Operation == "lenght<=") return (Result.length() <= CompareValue.toInt()) ? iftrue : ifelse;
		if (Operation == "lenght>=") return (Result.length() >= CompareValue.toInt()) ? iftrue : ifelse;
	}

	if (Function == "ifempty") {
		if (Result != "") return Result;
		while (!Params.isEmpty()) {
			QString Value = Params.takeFirst();
			Value = ValueCheckContext(Value, Context, !Params.isEmpty());
			if (Value != "") return Value;
		}
		return "";
	}
	if (Function == "replace") {
		if (Result == "") return "";
		QString ReplaceList = "";
		if (!Params.isEmpty()) ReplaceList = Params.takeFirst();
		ReplaceList = ValueCheckContext(ReplaceList);
		QStringList List = ReplaceList.split("|");
		for (int i = 0; i < List.count(); ++i) {
			QString Ori = List.at(i).section("=", 0, 0);
			QString Dest = List.at(i).section("=", 1);
			if (Ori.contains("\\n")) Ori.replace("\\n", "\n");
			if (Dest.contains("\\n")) Dest.replace("\\n", "\n");
			if (Result.contains(Ori)) Result.replace(Ori, Dest);
		}
		return Result;
	}

	if (Function == "replace_regexp") {
		if (Result == "") return "";
		if (Params.isEmpty()) return "";
		QString Regexp = Params.takeFirst();
		Regexp = ValueCheckContext(Regexp);
		if (Params.isEmpty()) return "";
		QString To = Params.takeFirst();
		To = ValueCheckContext(To);
		QRegularExpression RE(Regexp);
		return Result.replace(RE, To);
	}

	if (Function == "replace_file") {
		if (Result == "") return "";
		QString ReplaceFile;
		if (!Params.isEmpty()) ReplaceFile = Params.takeFirst();
		ReplaceFile = ValueCheckContext(ReplaceFile);
		if (!QFile::exists(ReplaceFile))
			ReplaceFile = QDir::toNativeSeparators(ReplaceDir + QDir::separator() + ReplaceFile);
		QStringList ReplaceList;
		if (Replace.contains(ReplaceFile)) {
			ReplaceList = Replace[ReplaceFile];
		} else {
			PrintDebug("\t\tReplace_text load file: " + ReplaceFile);

			QFile RF(ReplaceFile);

			if (!RF.open(QIODevice::ReadOnly | QIODevice::Text)) {
				PrintError("reading replace file: " + ReplaceFile);
				return "";
			}
			while (!RF.atEnd()) {
				QByteArray line = RF.readLine();
				ReplaceList.append(line.trimmed());
			}
			RF.close();
			Replace.insert(ReplaceFile, ReplaceList);
		}

		for (int i = 0; i < ReplaceList.count(); ++i) {
			QString Ori = ReplaceList.at(i).section("=", 0, 0);
			QString Dest = ReplaceList.at(i).section("=", 1);
			if (Ori.contains("\\n")) Ori.replace("\\n", "\n");
			if (Dest.contains("\\n")) Dest.replace("\\n", "\n");
			if (Result.contains(Ori)) Result.replace(Ori, Dest);
		}

		return Result;
	}

	//Example: %mul(size,8,0,1)%
	//Example: %porcent(tags.NUMBER_OF_BYTES,format.size,2,1)%
	if (Function == "div" ||
		Function == "mul" ||
		Function == "sum" ||
		Function == "sub" ||
		Function == "percent") return Operation(Function, Result, Params, Context);

	if (Function == "concat") {
		QString Separation = Param;
		Result = "";
		while (!Params.isEmpty()) {
			QString Value = Params.takeFirst();
			Value = ValueCheckContext(Value, Context);
			if (Value != "") {
				if (Result != "") Result += Separation;
				Result += Value;
			}
		}
		return Result;
	}

	if (Function == "file_exists") {
		QString Name = Result;
		QString Dir = "";
		if (!Params.isEmpty()) Dir = Params.takeFirst();
		if (Dir == "" && Vars.contains("file.dir"))
			Dir = Vars.value("file.dir");
		QString File = QDir::toNativeSeparators(Dir + QDir::separator() + Name);
		if (QFile::exists(File))
			return File;
		else
			return "";
	}

	if (Function == "read_text_file") {
		if (Result == "") Result = Param;
		QString Name = Result;
		QString Dir = "";
		QString File = Name;
		if (!QFile::exists(File)) {
			if (!Params.isEmpty()) Dir = Params.takeFirst();
			if (Dir == "" && Vars.contains("file.dir"))
				Dir = Vars.value("file.dir");
			File = QDir::toNativeSeparators(Dir + QDir::separator() + Name);
		}

		PrintDebug("\t\tloading file: " + File);

		QFile F(File);
		if (!F.open(QIODevice::ReadOnly | QIODevice::Text)) {
			PrintError("reading file: " + File);
			return "";
		}
		QByteArray Content = F.readAll();
		F.close();
		Result = Content;
		if (TextFormat) Result.replace("\n", "%N%");
		return Result;
	}

	if (Function == "command") {
		if (Param == "") return "";
		QStringList CommandParams;
		while (!Params.isEmpty()) {
			QString CurParam = Params.takeFirst();
			CurParam = ValueCheckContext(CurParam, Context);
			if (CurParam != "") CommandParams.append(CurParam);
		}
		Result = ReadProgram(Param, CommandParams);
		Result.replace("%", "\\%");
		if (TextFormat) Result.replace("\n", "%N%");
		return Result;
	}

	if (Function == "line") {
		if (Param == "") return "";
		int StartLine = 0;
		if (!Params.isEmpty()) StartLine = Params.takeFirst().toInt();
		int EndLine = StartLine;
		if (!Params.isEmpty()) EndLine = Params.takeFirst().toInt();
		return Result.section("\n", StartLine, EndLine);
	}

	PrintError("unrecognized function " + Function);
	return "";
}

bool TextSustitution::DetectFunction(QString Text) {
	if (Text == "") return false;
	FunctionMatch = FunctionSearch.match(Text);
	return FunctionMatch.hasMatch();
}

QString TextSustitution::DetectAndEjecuteFunction(QString Text, QString Context, bool *Executed) {
	if (Executed != nullptr) *Executed = false;
	if (Text == "") return Text;

	FunctionMatch = FunctionSearch.match(Text);

	if (!FunctionMatch.hasMatch()) return Text;

	QString Function = FunctionMatch.captured(1);
	QString ParamsStr = FunctionMatch.captured(2);
	QStringList Params = ParamsStr.split(",");

	if (Function != "") {
		if (Executed != nullptr) *Executed = true;
		return ProcessFunctions(Function, Params, Context, false);
	}
	return Text;
}

void TextSustitution::EmptyMatch(QString Context, QString &Result, bool TextFormat) {
	int Offset = 0;
	int GlobalCount = 0;
	int PosCount = 0;
	int AntPos = 0;
	while (Match = SearchTextEmpty.match(Result, Offset), Match.hasMatch()) {
		QString TextBefore = Match.captured(1);
		QString TextAfter = Match.captured(3);
		QString Function = "";
		QString Var = Match.captured(2);
		QString Value = "";
		QStringList Params;
		QString Output = "";

		GlobalCount++;

		if (AntPos != Match.capturedStart()) {
			AntPos = Match.capturedStart();
			PosCount = 0;
		} else {
			PosCount++;
		}

		if (PosCount > 10 || GlobalCount > 100) {
			PrintError("Error reached text substitution limit, posible infinite loop");
			break;
		}

		PrintDebug("\tMatch: " + Match.captured(0) + " - " + TextBefore + " - " + Var + " - " + TextAfter);

		FunctionMatch = FunctionSearch.match(Var);

		if (FunctionMatch.hasMatch()) {
			Function = FunctionMatch.captured(1);
			Var = FunctionMatch.captured(2);
			Output = FunctionMatch.captured(3);
			Params = Var.split(",");
			Value = ProcessFunctions(Function, Params, Context, TextFormat);
			if (Output != "") {
				if (Output.startsWith("*")) Output.replace("*", Context);
				Vars.insert(Output, Value);
				PrintDebug("\t\tResult to var: " + Output + "=" + Value);
				Value = "";
			}
		} else {
			if (Vars.contains(Context + Var)) {
				Value = Vars.value(Context + Var);
			} else if (Vars.contains(Var)) {
				Value = Vars.value(Var);
			} else {
				Result.replace(Match.captured(0), "");
				PrintDebug("\t\tResult: Empty");
				continue;
			}
		}

		if (Value == "") {
			Result.replace(Match.captured(0), "");
			PrintDebug("\t\tResult: Empty");
		} else {
			Result.replace(Match.captured(0), TextBefore + Value + TextAfter);
			PrintDebug("\t\tResult: " + TextBefore + Value + TextAfter);
		}
	}
}

void TextSustitution::SimpleMatch(QString Context, QString &Result, bool TextFormat) {
	int Offset = 0;
	int GlobalCount = 0;
	int PosCount = 0;
	int AntPos = 0;
	while (Match = SearchSimpleText.match(Result, Offset), Match.hasMatch()) {
		QString Function = "";
		QString Var = Match.captured(1);
		QString Value = "";
		QStringList Params;
		QString Output = "";

		if (Var.startsWith("#") || Var.length() < 3) {
			Offset = Match.capturedEnd();
			continue;
		}

		GlobalCount++;

		if (AntPos != Match.capturedStart()) {
			AntPos = Match.capturedStart();
			PosCount = 0;
		} else {
			PosCount++;
		}

		if (PosCount > 10 || GlobalCount > 100) {
			PrintError("Error reached text substitution limit, posible infinite loop");
			return;
		}

		PrintDebug("\tMatch Simple: " + Match.captured(0) + " - " + Var);

		FunctionMatch = FunctionSearch.match(Var);

		if (FunctionMatch.hasMatch()) {
			Function = FunctionMatch.captured(1);
			Params = FunctionMatch.captured(2).split(",");
			Output = FunctionMatch.captured(3);
		}

		if (Function != "") {
			Value = ProcessFunctions(Function, Params, Context, TextFormat);
			if (Output != "") {
				if (Output.startsWith("*")) Output.replace("*", Context);
				Vars.insert(Output, Value);
				PrintDebug("\t\tResult to var: " + Output + "=" + Value);
				Value = "";
			}
		} else {
			if (Vars.contains(Context + Var)) {
				Value = Vars.value(Context + Var);
			} else if (Vars.contains(Var)) {
				Value = Vars.value(Var);
			}
		}

		Result.replace(Match.captured(0), Value);
		if (Value == "")
			PrintDebug("\t\tResult: Empty");
		else
			PrintDebug("\t\tResult: " + Value);
	}
}

QString TextSustitution::ValueCheckContext(QString Var, QString Context, bool Empty) {
	if (Var == "") return Var;
	if (Context != "" && Vars.contains(Context + Var))
		return Vars.value(Context + Var);
	else if (Vars.contains(Var))
		return Vars.value(Var);
	if (Empty)
		return "";
	else
		return Var;
}

QString TextSustitution::ProcessText(QString Context, QString Text, text_sub_style_t *Style) {
	if (Text == "") return "";
	QString Result = Text;

	PrintDebug("Text substitution for:" + Text);

	EmptyMatch(Context, Result);

	PrintDebug("\tComplex Intermediate Result: " + Result);

	SimpleMatch(Context, Result);

	PrintDebug("\tSimple Intermediate Result: " + Result);

	Matches = SearchFormatText.globalMatch(Result);
	while (Matches.hasNext()) {
		QRegularExpressionMatch Match = Matches.next();
		QString Var = Match.captured(1);
		QString Value = "";
		PrintDebug("\tMatch Format: " + Match.captured(0) + " - " + Var);

		if (Var == "N") Value = "\\N";
		else if (Var == "n") Value = "\\n";
		else if (Var == "h") Value = "\\h";
		else if (Var == "h2") Value = "\\h\\h";
		else if (Var == "h4") Value = "\\h\\h\\h\\h";
		else if (Var == "h8") Value = "\\h\\h\\h\\h\\h\\h\\h\\h";
		else if (Var == "I1") Value = "{\\i1}";
		else if (Var == "I0") Value = "{\\i0}";
		else if (Var == "B1") Value = "{\\b1}";
		else if (Var == "B0") Value = "{\\b0}";
		else if (Var == "O0") Value = "{\\bord0}";
		else if (Var == "O1") Value = "{\\bord1}";
		else if (Var == "O2") Value = "{\\bord2}";
		else if (Var == "O3") Value = "{\\bord3}";
		else if (Var == "O4") Value = "{\\bord4}";
		else if (Var == "O5") Value = "{\\bord5}";
		else if (Var == "S0") Value = "{\\shad0}";
		else if (Var == "S1") Value = "{\\shad1}";
		else if (Var == "S2") Value = "{\\shad2}";
		else if (Var == "S3") Value = "{\\shad3}";
		else if (Var == "S4") Value = "{\\shad4}";
		else if (Var == "S5") Value = "{\\shad5}";
		else if (Var.startsWith("O#")) {
			Var.remove(0, 2);
			if ( Var == "")  {
				Var = Style->OutlineColour;
			} else {
				if (Vars.contains(Var)) Var = Vars.value(Var, Style->OutlineColour);
			}
			Value = "{\\3c&H" + StringColor2BGR(Var) + "&}";
		} else if (Var.startsWith("S#")) {
			Var.remove(0, 2);
			if ( Var == "")  {
				Var = Style->BackColour;
			} else {
				if (Vars.contains(Var)) Var = Vars.value(Var, Style->BackColour);
			}
			Value = "{\\3c&H" + StringColor2BGR(Var) + "&}";
		} else if (Var.startsWith("#")) {
			Var.remove(0, 1);
			if ( Var == "")  {
				Var = Style->PrimaryColour;
			} else {
				if (Vars.contains(Var)) Var = Vars.value(Var, Style->PrimaryColour);
			}
			Value = "{\\1c&H" + StringColor2BGR(Var) + "&}";
		}
		Result.replace(Match.captured(0), Value);
		PrintDebug("\t\tResult: " + Value);
	}

	if (Result == "") return "";

	Result.replace("\\%", "%");
	Result.replace("\\$", "$");

	PrintDebug("Total Result: " + Result);
	PrintDebug("");
	return Result;
}

QString TextSustitution::ProcessCMD(QString Context, QString CMD) {
	QString Result = CMD;
	PrintDebug("CMD substitution for:" + CMD);
	EmptyMatch(Context, Result, false);
	PrintDebug("\tComplex Intermediate Result: " + Result);
	SimpleMatch(Context, Result, false);
	Result.replace("\\%", "%");
	Result.replace("\\$", "$");
	PrintDebug("Total Result: " + Result);
	PrintDebug("");
	return Result;
}

QString TextSustitution::ProcessTemplate(QString Template) {
	QString Result = ReadFile(Template);
	PrintDebug("Template substitution for file:" + Template);
	EmptyMatch("", Result, false);
	SimpleMatch("", Result, false);
	Result.replace("\\%", "%");
	Result.replace("\\$", "$");
	return Result;
}

void TextSustitution::Insert(QString Var, QString Value, bool scape) {
	if (scape)
		Vars.insert(Var, ScapeCaracters(Value));
	else
		Vars.insert(Var, Value);
}

QString TextSustitution::Value(QString Var, QString Default) {
	return Vars.value(Var, Default);
}

bool TextSustitution::Contains(QString Var) {
	return Vars.contains(Var);
}

void TextSustitution::Remove(QString Var) {
	Vars.remove(Var);
}

void TextSustitution::Update(QString Var, QString Value, bool scape) {
	if (scape)
		Vars[Var] = ScapeCaracters(Value);
	else
		Vars[Var] = Value;
}

void TextSustitution::PrintAll() {
	for (QMap<QString, QString>::const_iterator i = Vars.constBegin();
		 i != Vars.constEnd();
		 ++i) {
		PrintInfo("\t" + i.key() + " = " + i.value());
	}
}
