#include "textblock.h"
#include "colors.h"
#include "common.h"
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QRegularExpression>
#include <QProcess>

aligment_t String2Alingment(QString pos) {
	pos = pos.toLower();
	if (pos == "up_left") return align_up_left;
	if (pos == "up_right") return align_up_right;
	if (pos == "down_left") return align_down_left;
	if (pos == "down_right") return align_down_right;
	if (pos == "up_center") return align_up_center;
	if (pos == "down_center") return align_down_center;
	if (pos == "middle_left") return align_middle_left;
	if (pos == "middle_right") return align_middle_right;
	if (pos == "middle_center") return align_middle_center;
	return align_up_left;
}

text_sub_style_t::text_sub_style_t(QString name, text_sub_style_t *copy, ParamsSettings *PS, QString style) {
	Name = name;
	if (copy != nullptr) {
		Fontname = copy->Fontname;
		Fontsize = copy->Fontsize;
		PrimaryColour = copy->PrimaryColour;
		SecondaryColour = copy->SecondaryColour;
		OutlineColour = copy->OutlineColour;
		BackColour = copy->BackColour;
		Bold = copy->Bold;
		Italic = copy->Italic;
		Underline = copy->Underline;
		StrikeOut = copy->StrikeOut;
		BorderStyle = copy->BorderStyle;
		Outline = copy->Outline;
		Shadow = copy->Shadow;
		Alignment = copy->Alignment;
		MarginL = copy->MarginL;
		MarginR = copy->MarginR;
		MarginV = copy->MarginV;
		BorderBlur = copy->BorderBlur;
	} else {
		Fontname = "Sans";
		Fontsize = 12;
		PrimaryColour = "white";
		SecondaryColour = "yellow";
		OutlineColour = "black";
		BackColour = "black";
		Bold = false;
		Italic = false;
		Underline = false;
		StrikeOut = false;
		BorderStyle = border_outline_shadow;
		Outline = 0;
		Shadow = 0;
		Alignment = align_up_left;
		MarginL = 10;
		MarginR = 10;
		MarginV = 10;
		BorderBlur = 0;
	}
	if (PS != nullptr) {
		if (style == "font") {
			Fontname = PS->Value("tb_font");
			Fontsize = PS->Int("tb_font_size");
		} else {
			if (PS->NotEmpty("tb_" + style + "_font")) Fontname = PS->Value("tb_" + style + "_font");
			if (PS->Int("tb_" + style + "_font_size") > 0) Fontsize = PS->Int("tb_" + style + "_font_size");
		}
		PrimaryColour = PS->Value("tb_" + style + "_color");
		OutlineColour = PS->Value("tb_" + style + "_border_color");
		BackColour = PS->Value("tb_" + style + "_shadow_color");
		Outline = PS->Int("tb_" + style + "_border");
		BorderBlur = PS->Int("tb_" + style + "_border_blur");
		Shadow = PS->Int("tb_" + style + "_shadow");
		Italic = PS->Bool("tb_" + style + "_italic");
		Bold = PS->Bool("tb_" + style + "_bold");
		SetAlignment(PS->Value("tb_" + style + "_pos"));
	}
}

void text_sub_style_t::SetAlignment(QString align) {
	Alignment = String2Alingment(align);
}

TextBlock::TextBlock(QString TmpDir, QObject *parent)
	: QObject{parent} {
	Tmp = TmpDir;
	BGInputImage = "";
	BGImageOpacity = 100;
	TmpSub = QDir::toNativeSeparators(Tmp + QDir::separator() + "head_subtitle.ass");
	TmpBackground = QDir::toNativeSeparators(Tmp + QDir::separator() + "tb_background.png");
	BGGradient = false;
}

void TextBlock::AddStyle(text_sub_style_t &Style) {
	Styles.append(Style);
}

int TextBlock::AddLine(QString text, QString StyleName, int MarginL, int MarginR, int MarginV) {
	if (text == "") return -1;

	text_sub_line_t Line;
	Line.Text = text;
	Line.Style = nullptr;
	Line.MarginL = MarginL;
	Line.MarginR = MarginR;
	Line.MarginV = MarginV;
	Line.Lines = text.count("\\N") + 1;

	if (StyleName != "") {
		for (int i = 0; i < Styles.count(); ++i) {
			if (StyleName == Styles.at(i).Name) Line.Style = &Styles[i];
		}
	} else {
		if (Styles.empty()) {
			Styles.append(text_sub_style_t("Default"));
		}
		Line.Style = &Styles.first();
	}
	if (Line.Style->BorderBlur > 0)
		Line.Text = "{\\blur" + QString::number(Line.Style->BorderBlur) + "}" + Line.Text;

	int pos = Lines.size();
	Lines.insert(pos, Line);
	return pos;
}

void TextBlock::AppendLine(QString text, int line) {
	if (text == "") return;
	if (line >= Lines.size()) return;
	text_sub_line_t Line = Lines[line];
	Line.Text += "\\N" + text;
	Line.Lines = Line.Text.count("\\N") + 1;
	Lines[line] = Line;
}

void TextBlock::SetFontDir(QString Dir) {
	FontDir = QDir::toNativeSeparators(Dir);
}

void TextBlock::SetBGImage(QString File, haligned_t ha, valigned_t va, bool scale, bool tile, int blur, int exposure,
						   int saturation, int opacity, QString mask) {
	BGImageFile = QDir::toNativeSeparators(File);
	if (BGImageFile != "" && !QFileInfo::exists(BGImageFile)) {
		PrintError("tb_bg_image file is not accessible: " + BGImageFile);
		BGImageFile = "";
	}
	BGImageHAlign = ha;
	BGImageVAlign = va;
	BGImageScale = scale;
	BGImageTile = tile;
	BGImageBlur = blur;
	BGImageExposure = exposure;
	BGImageSaturation = saturation;
	BGImageOpacity = opacity;
	BGInputMask = mask;
}

void TextBlock::SetBGColor(QString Color) {
	BGColor = Color;
}

void TextBlock::SetGradient(QString grad) {
	BGGradient = (grad != "none");
	BGGrad = grad;
}

void TextBlock::SetLogo(QString logo, bool left, bool overlay) {
	Logo = QDir::toNativeSeparators(logo);
	LogoLeft = left;
	LogoOverlay = overlay;

	QString TB_LOGO_ASPECT_RATIO;
	ffprobe(Logo, "stream=display_aspect_ratio", TB_LOGO_ASPECT_RATIO);
	QStringList AS_SPLIT = TB_LOGO_ASPECT_RATIO.split(":");
	LogoAR = AS_SPLIT.first().toDouble() / AS_SPLIT.last().toDouble();
	if (LogoAR == 0 || qIsNaN(LogoAR)) {
		uint64_t w, h;
		GetImageSize(Logo, w, h);
		LogoAR = (double)w / (double)h;
	}
}

void TextBlock::SetBorder(int border) {
	Border = border;
}

void TextBlock::SetBGInputImage(QString image, uint64_t pos) {
	BGInputImage = image;
	BGInputImagePos = pos;
}

void TextBlock::GenerateSubTmpFile(int Width, int Height, wrap_style_t Wrap) {

	QString SubtitleContent = "[Script Info]\n\
Title: Text header file\n\
ScriptType: v4.00+\n\
WrapStyle: " + QString::number(Wrap) + "\n\
ScaledBorderAndShadow: yes\n\
YCbCr Matrix: TV.601\n\
PlayResX: " + QString::number(Width) + "\n\
PlayResY: " + QString::number(Height) + "\n\
\n\
[V4+ Styles]\n\
Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, Encoding\n";


	for (int i = 0; i < Styles.count(); ++i) {
		text_sub_style_t Style = Styles.at(i);
		int L = Style.MarginL;
		int R = Style.MarginR;
		if (!LogoOverlay) {
			if (LogoLeft)
				L = LogoWidth + (Border * 2);
			else
				R = LogoWidth + (Border * 2);
		}

		SubtitleContent += "Style: " + Style.Name + ", " +							//Name
						   Style.Fontname + ", " +									//Fontname
						   QString::number(Style.Fontsize) + ", " +					//Fontsize
						   "&H" + StringColor2BGR(Style.PrimaryColour) + ", " +		//PrimaryColour
						   "&H" + StringColor2BGR(Style.SecondaryColour) + ", " +	//SecondaryColour
						   "&H" + StringColor2BGR(Style.OutlineColour) + ", " +		//OutlineColour
						   "&H" + StringColor2BGR(Style.BackColour) + ", " +		//BackColour
						   QString::number(Style.Bold) + ", " +						//Bold
						   QString::number(Style.Italic) + ", " +					//Italic
						   QString::number(Style.Underline) + ", " +				//Underline
						   QString::number(Style.StrikeOut) + ", " +				//StrikeOut
						   "100, " +												//ScaleX
						   "100, " +												//ScaleY
						   "0, " +													//Spacing
						   "0, " +													//Angle
						   QString::number(Style.BorderStyle) + ", " +				//BorderStyle
						   QString::number(Style.Outline) + ", " +					//Outline
						   QString::number(Style.Shadow) + ", " +					//Shadow
						   QString::number(Style.Alignment) + ", " +				//Alignment
						   QString::number(L) + ", " +								//MarginL
						   QString::number(R) + ", " +								//MarginR
						   QString::number(Style.MarginV) + ", " +					//MarginV
						   "1\n";													//Encoding
	}

	SubtitleContent += "\n\
[Events]\n\
Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text\n";

	for (int i = 0; i < Lines.count(); ++i) {
		text_sub_line_t Line = Lines.at(i);
		SubtitleContent += "Dialogue: 0, "								//Layer
						   "0: 00: 00.00, "								//Start
						   "0: 00: 05.00, " +							//End
						   Line.Style->Name + "," +						//Style
						   ", " +										//Name
						   QString::number(Line.MarginL) + ", " +		//MarginL
						   QString::number(Line.MarginR) + ", " +		//MarginR
						   QString::number(Line.MarginV) + "," +		//MarginV
						   "," +										//Effect
						   Line.Text + "\n";							//Text
	}

	QFile SubTmpFile(TmpSub);
	if (SubTmpFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
		QTextStream stream(&SubTmpFile);
		stream << SubtitleContent;
		stream.flush();
	}
	SubTmpFile.close();
}

QStringList TextBlock::GenerateParams(int Width, int Height, QString Output) {

	QString FONTDIR = "";
	if (FontDir != "") {
		FONTDIR = ":fontsdir='" + ScapeFFFilters(FontDir) + "'";
	}

	QStringList Params;
	Params << "-hide_banner" << "-loglevel" << "error" << "-y";

	QString bgcolororgradient = BGColor;
	if (BGGradient)
		bgcolororgradient = BGGrad;


	if (BGImageFile != "") {
		if (GenerateBackgroundImage(BGImageFile,
									TmpBackground,
									Width,
									Height,
									BGImageHAlign,
									BGImageVAlign,
									BGImageScale,
									BGImageTile,
									BGImageBlur,
									BGImageExposure,
									BGImageSaturation,
									BGImageOpacity,
									BGColor,
									BGInputImage,
									bgcolororgradient,
									BGInputMask,
									BGInputImagePos)) {
			Params << "-i" << TmpBackground;
		} else {
			PrintError("Error in text block background image");
			BGImageFile = "";
		}
	} else {
		Params << "-f" << "lavfi" << "-i";
		if (BGGradient) {
			Params << String2GradientFilter(Width, Height, BGGrad);
		} else {
			Params << GenerateSolidColorFilter(Width, Height, BGColor);
		}
	}

	QStringList Filters;

	if (Logo != "") {
		Params << "-i" << Logo;

		if (BGInputImage != "" && BGImageFile == "") {
			Params << "-i" << BGInputImage;
		}

		Filters << "[1:v]scale=" + QString::number(LogoWidth) + ":" +
				QString::number(Height - (Border * 2)) + ":flags=lanczos[logo]";

		if (LogoLeft) {
			Filters << "[0:v][logo]overlay=x=" + QString::number(Border) + ":y=" + QString::number(Border) + ":format=rgb[over]";
		} else {
			Filters << "[0:v][logo]overlay=x=main_w-overlay_w-" + QString::number(Border) +
					":y=" + QString::number(Border) + ":format=rgb[over]";
		}
		if (BGInputImage != "" && BGImageFile == "") {
			Filters << "[2:v]crop=w=" + QString::number(Width) + ":h=" + QString::number(Height) +
					":x=0:y=" + QString::number(BGInputImagePos) + "[bg]" <<
					"[bg][over]overlay=x=0:y=0:format=rgb[over]";
		}

		Filters << "[over]subtitles=alpha=1:f='" + ScapeFFFilters(TmpSub) + "'" + FONTDIR;
	} else {
		Filters << "subtitles=alpha=1:f='" + ScapeFFFilters(TmpSub) + "'" + FONTDIR;
	}

	Params << "-filter_complex" << Filters.join("; ") << "-vframes" << "1" << Output;

	return Params;
}

bool TextBlock::GenerateBackgroundImage(QString In, QString Out, int w, int h, haligned_t ha, valigned_t va,
										bool scale, bool tile, int blur, int exposure, int saturation, int opacity, QString fillcolor, QString bgimage,
										QString bg, QString mask, int pos) {
	if (!QFile(In).exists()) return false;

	struct layers_t {
		QString Name;
		QString Filters;
		QString File;
	};

	QList<layers_t> Layers;

	uint64_t BgWidth, BgHeight;
	GetImageSize(In, BgWidth, BgHeight);

	QString cropx = "";

	switch (ha) {
		case aligned_left:
			cropx = "x=0";
			break;
		case aligned_right:
			cropx = "x=in_w-out_w";
			break;
		case aligned_center:
			cropx = "x=(in_w-out_w)/2";
			break;
		default:
			cropx = "x=(in_w-out_w)*(" + QString::number(ha) + "/100)";
			break;
	}

	QString cropy = "";

	switch (va) {
		case aligned_up:
			cropy = "y=0";
			break;
		case aligned_down:
			cropy = "y=in_h-out_h";
			break;
		case aligned_middle:
			cropy = "y=(in_h-out_h)/2";
			break;
		default:
			cropy = "y=(in_h-out_h)*(" + QString::number(va) + "/100)";
			break;
	}

	QStringList bg_filters;
	if (scale) {
		bg_filters << "scale=flags=lanczos:force_original_aspect_ratio=increase:w=" + QString::number(w) +
				   ":h=" + QString::number(h);
	} else if (tile) {
		uint64_t TileCols = 0;
		uint64_t TileRows = 0;

		if (BgWidth < (uint64_t)w)
			TileCols = w / BgWidth + 1;
		else
			TileCols = 1;

		if (BgHeight < (uint64_t)h)
			TileRows = h / BgHeight + 1;
		else
			TileRows = 1;
		DEBUGVAR(TileCols);
		DEBUGVAR(TileRows);

		if (TileCols > 1 || TileRows > 1) {
			bg_filters << "loop=loop=" + QString::number(TileRows * TileCols) + ":size=1";
			bg_filters << "tile=layout=" + QString::number(TileCols) + "x" + QString::number(TileRows);
		}
	} else {
		uint64_t neww = w;
		if (neww < BgWidth) neww = BgWidth;
		uint64_t newh = h;
		if (newh < BgHeight) newh = BgHeight;

		QString padx = "";
		switch (ha) {
			case aligned_left:
				padx = "x=0";
				break;
			case aligned_right:
				padx = "x=out_w-in_w";
				break;
			case aligned_center:
				padx = "x=(out_w-in_w)/2";
				break;
			default:
				padx = "x=(out_w-in_w)*(" + QString::number(ha) + "/100)";
				break;
		}

		QString pady = "";
		switch (va) {
			case aligned_up:
				pady = "y=0";
				break;
			case aligned_down:
				pady = "y=out_h-in_h";
				break;
			case aligned_middle:
				pady = "y=(out_h-in_h)/2";
				break;
			default:
				pady = "y=(out_h-in_h)*(" + QString::number(va) + "/100)";
				break;
		}
		bg_filters << "pad=w=" + QString::number(neww) + ":h=" + QString::number(newh) + ":" + padx + ":" + pady +
				   ":color=" + StringColor2RGB(fillcolor);
	}

	bg_filters << "crop=w=" + QString::number(w) + ":h=" + QString::number(h) + ":" + cropx + ":" + cropy;

	if (saturation != 100)
		bg_filters << "eq=saturation=" + QString::number((double)saturation / 100);

	if (exposure != 0)
		bg_filters << "exposure=" + QString::number((double)exposure / 100);

	if (blur != 0)
		bg_filters << "gblur=sigma=" + QString::number((double)blur / 100);

	if (opacity != 100)
		bg_filters << "format=rgba" << "colorchannelmixer=aa=" + QString::number((double)opacity / 100);

	if (bgimage != "")
		Layers.append({"bgimage", "crop=w=" + QString::number(w) + ":h=" + QString::number(h) +
					   ":x=0:y=" + QString::number(pos), "-i \"" + bgimage + "\""});

	if (bg != "")
		Layers.append({"bg", "", "-f lavfi -i " + String2GradientFilter(w, h, bg)});

	if (mask != "") {
		if (QFile::exists(mask))
			Layers.append({"normal", bg_filters.join(",") + "[normal];movie=" + mask + "[mask];[normal][mask]alphamerge", "-i \"" + In + "\""});
		else
			Layers.append({"normal", bg_filters.join(",") + "[normal];" + String2GradientFilter(w, h, mask) + "[mask];[normal][mask]alphamerge", "-i \"" + In + "\""});
	} else {
		Layers.append({"normal", bg_filters.join(","), "-i \"" + In + "\""});
	}


	QString ComplexFilter = "";
	QStringList Params;
	Params << "-hide_banner" << "-loglevel" << "error" << "-y";

	bool firtlayer = true;
	QString LastName = "";
	for (int i = 0; i < Layers.count(); ++i) {
		Params << QProcess::splitCommand(Layers.at(i).File);
		QString name = Layers.at(i).Name;
		if (Layers.at(i).Filters != "") {
			if (ComplexFilter != "") ComplexFilter += ";";
			ComplexFilter += "[" + QString::number(i) + "]" + Layers.at(i).Filters;
			if (Layers.count() > 1) ComplexFilter += "[" + name + "]";
			if (!firtlayer) {
				ComplexFilter += ";[" + LastName + "][" + name + "]overlay=x=0:y=0:format=rgb";
				if (i < Layers.count() - 1) ComplexFilter += "[" + name + "]";
			}
		} else {
			if (!firtlayer) {
				if (ComplexFilter != "") ComplexFilter += ";";
				ComplexFilter += "[" + LastName + "][" + QString::number(i) + "]overlay=x=0:y=0:format=rgb";
				if (i < Layers.count() - 1) ComplexFilter += "[" + name + "]";
			} else {
				name = QString::number(i);
			}
		}
		firtlayer = false;
		LastName = name;
	}

	Params << "-filter_complex" << ComplexFilter << "-frames:v" << "1" << Out;

	return ffmpeg(Params);
}

QString TextBlock::String2GradientFilter(int width, int height, QString gradient) {
	if (!gradient.contains(":"))
		return GenerateSolidColorFilter(width, height, gradient);

	QString type = gradient.section(":", 0, 0);
	QString direcction = gradient.section(":", 1, 1);
	QString colors = gradient.section(":", 2, 2);
	return GenerateGradientFilter(width, height, type, colors, direcction);
}

QString TextBlock::GenerateGradientFilter(int width, int height, QString type, QString colors, QString direction) {
	QString Result = "gradients=size=" + QString::number(width) + "x" + QString::number(height);
	Result += ":type=" + type;
	QStringList ColorsSplit = colors.split(",");
	int NColors = 0;
	for (int i = 0; i < ColorsSplit.count(); ++i) {
		if (i > 7) break;
		Result += ":c" + QString::number(i) + "=" + StringColor2RGB(ColorsSplit.at(i));
		NColors++;
	}
	if (NColors > 2 && NColors < 8) Result += ":nb_colors=" + QString::number(NColors);

	int FromX = 0;
	int FromY = 0;
	int ToX = 100;
	int ToY = 0;

	if (direction.contains("-")) {
		QString From = direction.section('-', 0, 0).toLower();
		QString To = direction.section('-', 1, 1).toLower();
		FromX = From.section("x", 0, 0).toInt();
		FromY = From.section("x", 1, 1).toInt();
		ToX = To.section("x", 0, 0).toInt();
		ToY = To.section("x", 1, 1).toInt();
	} else {
		if (direction == "horizontal" || direction == "h") {
			FromX = 0;
			FromY = 0;
			ToX = 100;
			ToY = 0;
		}
		if (direction == "vertical" || direction == "v") {
			FromX = 0;
			FromY = 0;
			ToX = 0;
			ToY = 100;
		}
		if (direction == "diagonalup" || direction == "du") {
			FromX = 100;
			FromY = 100;
			ToX = 0;
			ToY = 0;
		}
		if (direction == "diagonaldown" || direction == "dd") {
			FromX = 0;
			FromY = 0;
			ToX = 100;
			ToY = 100;
		}
	}

	FromX = FromX * width / 100;
	FromY = FromY * height / 100;
	ToX = ToX * width / 100;
	ToY = ToY * height / 100;

	if (FromX >= width) FromX = width - 1;
	if (FromY >= height) FromY = height - 1;
	if (ToX >= width) ToX = width - 1;
	if (ToY >= height) ToY = height - 1;

	if (FromX < 0) FromX = 0;
	if (FromY < 0) FromY = 0;
	if (ToX < 0) ToX = 0;
	if (ToY < 0) ToY = 0;

	Result += ":x0=" + QString::number(FromX) +
			  ":y0=" + QString::number(FromY) +
			  ":x1=" + QString::number(ToX) +
			  ":y1=" + QString::number(ToY);

	//	Result += ",format=rgba";
	return Result;
}

QString TextBlock::GenerateSolidColorFilter(int width, int height, QString color) {
	return "color=c=0x" + StringColor2RGB(color) +
		   ":size=" + QString::number(width) + "x" + QString::number(height) +
		   ",format=rgba";
}

bool TextBlock::GenerateGradient(QString Out, int width, int height, QString gradient) {
	QStringList gradient_params = {"-hide_banner",
								   "-loglevel",
								   "error",
								   "-y",
								  };
	gradient_params << "-f" << "lavfi" << "-i";
	gradient_params << String2GradientFilter(width, height, gradient);
	gradient_params << "-vframes" << "1" << Out;
	return ffmpeg(gradient_params);
}

int TextBlock::Generate(int Width, int Height, QString Output) {
	LogoWidth = qRound(((Height - (Border * 2)) / 2) * LogoAR) * 2;
	GenerateSubTmpFile(Width, Height);
	QStringList Params = GenerateParams(Width, Height, Output);
	return ffmpeg(Params);
}

int TextBlock::TotalTextHeight() {
	int Total = 0;
	for (int i = 0; i < Lines.count(); ++i) {
		Total += Lines.at(i).Lines * Lines.at(i).Style->Fontsize;
	}
	return Total;
}
