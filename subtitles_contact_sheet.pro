SCS_VERSION = 1.0

QT -= gui
QT += core

CONFIG += c++17
CONFIG += console
CONFIG -= app_bundle

DEFINES += SCS_VERSION=\\\"$$SCS_VERSION\\\"

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
		batch.cpp \
		colors.cpp \
		common.cpp \
		iso639.cpp \
		main.cpp \
		paramssettings.cpp \
		subtitles.cpp \
		textblock.cpp \
		textsustitution.cpp

HEADERS += \
	batch.h \
	colors.h \
	common.h \
	iso639.h \
	paramssettings.h \
	subtitles.h \
	textblock.h \
	textsustitution.h

unix:!android{
	!defined(USR_DIR, var) { USR_DIR = /usr/local }
	!defined(MAN_DIR) { MAN_DIR = /man/man1 }
	BIN_DIR = $$USR_DIR/bin
	target.path = $$BIN_DIR
	INSTALLS += target

	mantarget.target = makeman
	DOC_DATE = $$system(LANG=en date \"+%B %Y\")
        mantarget.commands = pandoc manpage.md -s -t man -M \"title=subtitles_contact_sheet(1) subtitles_contact_sheet $${SCS_VERSION}\" -M \"date=$${DOC_DATE}\" | sed -r \"s/\\-\\-/\\\\\\-\\\\\\-/g\" | sed -r \"s/([^\\\\])\\-/\\1\\\\\\-/g\" | gzip > $${TARGET}.1.gz
	QMAKE_EXTRA_TARGETS += mantarget
	POST_TARGETDEPS += makeman

	maninstall.CONFIG = no_check_exist
	maninstall.path = $${USR_DIR}$${MAN_DIR}
	maninstall.files += $${TARGET}.1.gz
	INSTALLS += maninstall
}
