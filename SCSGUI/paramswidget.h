#ifndef PARAMSWIDGET_H
#define PARAMSWIDGET_H

#include <QWidget>
#include <QScrollArea>
#include <QFormLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include "../paramssettings.h"

class ParamsWidget : public QWidget {
		Q_OBJECT
	public:
		explicit ParamsWidget(QString &program, QStringList *configlist, ParamsSettings *ps, QWidget *parent = nullptr);

		void LoadParams(QString Command);
		void LoadParams(QStringList &Params);
		void LoadCommand(QString Command);
		QString GetCommand();
		QString GetParams();
		void SetCommand(QString CommandText);
		void SetParams(QString ParamsText);
		void SetInputFile(QString InputFile);

		void Clean();
		void AddItem(QString Param, QString Value);
		void AddBool(QString Param);
		void Add(QString Param, QString Value);
		void HideCommand();

		const QStringList VideoExtensions = {"mkv", "avi", "mp4", "mov", "m4v", "wmv"};
		const QStringList SubtitleExtensions = {"srt", "vtt", "ass"};

	private:
		QString Program;
		QStringList *AvailableConfigs;
		ParamsSettings *PS;
		QVBoxLayout *MainLayout;
		QScrollArea *Scroll;
		QWidget *ListWidget;
		QFormLayout *ListLayout;
		QWidget *ButtonsWidget;
		QHBoxLayout *ButtonsLayout;
		QPushButton *AddParam;
		QPushButton *RemoveParam;
		QLineEdit *Command;
		bool AutomaticUpdate;
	signals:

	public slots:
		void Updated();
		void Removed(QWidget *RemovedObject);
		void OpenSearch();
		void TougleRemove(bool Presed);

};

#endif // PARAMSWIDGET_H
