#include <QApplication>
#include <QFile>
#include "scsmainwindow.h"

bool Debug = false;
bool Quiet = false;
bool Cmd = false;
bool GUIProgress = false;

int main(int argc, char *argv[]) {
	QApplication a(argc, argv);

	//	QString CONFIG = "config";
	QString File = "";
	for (int i = 1; i < argc; i++) {
		QString param(argv[i]);
		if (QFile::exists(param)) {
			File = param;
		}
	}

	SCSMainWindow w(File);
	w.show();
	return a.exec();
}
