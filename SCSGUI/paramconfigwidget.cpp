#include "paramconfigwidget.h"
#include "configurationdialog.h"
#include <QInputDialog>
#include <QMessageBox>

ParamConfigWidget::ParamConfigWidget(ParamsSettings *ps, QStringList *configlist, QString value, QWidget *parent)
	: QWidget{parent} {
	AvailableConfigs = configlist;
	PS = ps;

	Layout = new QHBoxLayout(this);
	Layout->setContentsMargins(0, 0, 0, 0);
	setLayout(Layout);

	Combo = new QComboBox(this);
	Combo->addItems(*AvailableConfigs);
	Combo->setCurrentText(value);

	Edit = new QPushButton("Edit", this);
	New = new QPushButton("New", this);
	Del = new QPushButton("Del", this);

	Layout->addWidget(Combo);
	Layout->addWidget(Edit);
	Layout->addWidget(New);
	Layout->addWidget(Del);

	connect(Edit, &QPushButton::clicked, this, &ParamConfigWidget::EditClick);
	connect(New, &QPushButton::clicked, this, &ParamConfigWidget::NewClick);
	connect(Del, &QPushButton::clicked, this, &ParamConfigWidget::DelClick);
	connect(Combo, &QComboBox::currentTextChanged, this, &ParamConfigWidget::ComboChanged);
}

void ParamConfigWidget::EditClick() {
	QString Config = Combo->currentText();
	if (Config == "") Config = "config";
	ConfigurationDialog *CD = new ConfigurationDialog(Config, this);
	CD->exec();
}

void ParamConfigWidget::NewClick() {
	bool ok;
	QString New = QInputDialog::getText(this, "New Config", "Select new config name", QLineEdit::Normal, "", &ok);
	if (!ok) return;
	if (New == "") {
		QMessageBox::critical(this, "Error creating config file", "The name cannot be empty");
		return;
	}
	if (AvailableConfigs->contains(New)) {
		QMessageBox::critical(this, "Error creating config file", "The name is already in use");
		return;
	}
	ConfigurationDialog *CD = new ConfigurationDialog(New, this);
	if (CD->exec() == QDialog::Accepted) {
		AvailableConfigs->append(New);
		AvailableConfigs->sort();
		Combo->clear();
		Combo->addItems(*AvailableConfigs);
		Combo->setCurrentText(New);
	}
}

void ParamConfigWidget::DelClick() {
	QString remove = Combo->currentText();
	PS->DeleteConfig(remove);
	AvailableConfigs->removeOne(remove);
	Combo->removeItem(Combo->currentIndex());
}

void ParamConfigWidget::ComboChanged(QString Value) {
	emit Updated(Value);
}
