#ifndef SCSMAINWINDOW_H
#define SCSMAINWINDOW_H

#include <QMainWindow>
#include <QTabWidget>
#include <QPlainTextEdit>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QProgressBar>
#include <QPushButton>
#include "../paramssettings.h"
#include "paramswidget.h"
#include "batchformwidget.h"
#include "QSettings"
#include <QProcess>

class SCSMainWindow : public QMainWindow {
		Q_OBJECT
	public:
		explicit SCSMainWindow(QString OpenFile, QWidget *parent = nullptr);
		void ReadConfigList();
		QString GetCurrentTabCommand();
	private:
		QString Program;
		QStringList AvailableConfigs;
		bool ProgramFailed;
		ParamsSettings *PS;

		QWidget *CentralWidget;
		QVBoxLayout *CentralLayout;

		QPlainTextEdit *Log;
		QWidget *ProgressWidget;
		QHBoxLayout *ProgressLayout;
		QProgressBar *Progress;
		QPushButton *ShowLog;
		QPushButton *Cancel;
		QPushButton *Execute;

		QTabWidget *Tabs;
		QHash<QString, int> Tabids;
		QHash<QString, QWidget *> TabWidgets;

		ParamsWidget *Normal;
		ParamsWidget *Subtitles;
		ParamsWidget *VR;
		ParamsWidget *Gif;
		ParamsWidget *Screenshots;
		BatchFormWidget *BatchWidget;

		QSettings *Settings;
		QProcess Process;
	protected:
		void closeEvent(QCloseEvent *event) override;
	signals:

	public slots:
		void ExecuteClick();
		void CancelClick();

		void ProcessTerminate(int exitCode, QProcess::ExitStatus exitStatus);
		void ProcessReadData();
		void ProcessErrorData();


};

#endif // SCSMAINWINDOW_H
