#include "configurationdialog.h"
#include <QApplication>

ConfigurationDialog::ConfigurationDialog(QString name, QWidget *parent, const Qt::WindowFlags &f) : QDialog(parent, f) {

	PS = new ParamsSettings(QApplication::applicationDirPath(), this);
	PS->SetConfig(name);
	PS->ReadFromConfigFile();
	CW = new ConfigurationWidget(PS, this);

	setWindowTitle("Edit \"" + name + "\" configuration file");

	MainLayout = new QVBoxLayout(this);

	ButtonsWidget = new QWidget(this);
	ButtonsLayout = new QHBoxLayout(ButtonsWidget);
	CancelButton = new QPushButton("Cancel", ButtonsWidget);
	ButtonsLayout->addWidget(CancelButton);
	SaveButton = new QPushButton("Save", ButtonsWidget);
	ButtonsLayout->addWidget(SaveButton);

	connect(CancelButton, &QPushButton::clicked, this, &ConfigurationDialog::Cancel);
	connect(SaveButton, &QPushButton::clicked, this, &ConfigurationDialog::Save);

	MainLayout->addWidget(CW);
	MainLayout->addWidget(ButtonsWidget);

	setLayout(MainLayout);
}

void ConfigurationDialog::Save() {
	PS->UpdateConfigFile(true, false);
	accept();
	//	close();
}

void ConfigurationDialog::Cancel() {
	reject();
	//	close();
}
