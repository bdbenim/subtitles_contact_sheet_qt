SCS_VERSION = 1.0

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

DEFINES += SCS_VERSION=\\\"$$SCS_VERSION\\\"

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

RC_ICONS = SCS.ico

SOURCES += \
	common.cpp \
	main.cpp \
	../colors.cpp \
	../paramssettings.cpp \
	../textsustitution.cpp \
	../iso639.cpp \
	batchcommandswidget.cpp \
	batchformwidget.cpp \
	batchowncommandwidget.cpp \
	optionssearchdialog.cpp \
	paramboolwidget.cpp \
	paramconfigwidget.cpp \
	paramsdialog.cpp \
	paramswidget.cpp \
	paramwidget.cpp \
	configurationdialog.cpp \
	configurationwidget.cpp \
	optioneditor.cpp \
	optionssearch.cpp \
	optionssearchitem.cpp \
	scsmainwindow.cpp \
	useroptioneditor.cpp

HEADERS += \
	common.h \
	../colors.h \
	../paramssettings.h \
	../textsustitution.h \
	../iso639.h \
	batchcommandswidget.h \
	batchformwidget.h \
	batchowncommandwidget.h \
	optionssearchdialog.h \
	paramboolwidget.h \
	paramconfigwidget.h \
	paramsdialog.h \
	paramswidget.h \
	paramwidget.h \
	configurationdialog.h \
	configurationwidget.h \
	optioneditor.h \
	optionssearch.h \
	optionssearchitem.h \
	scsmainwindow.h \
	useroptioneditor.h

RESOURCES += \
	icon.qrc

unix:!android{
	!defined(USR_DIR, var) { USR_DIR = /usr/local }
	!defined(ICONS_DIR) { ICONS_DIR = /share/icons }
	!defined(DESKTOP_DIR) { DESKTOP_DIR = /share/applications }
	BIN_DIR = $$USR_DIR/bin

	target.path = $$BIN_DIR
	INSTALLS += target

	DESKTOP_FILE = $${TARGET}.desktop
	DESKTOP_CONTENT += "[Desktop Entry]"
	DESKTOP_CONTENT += "Name=Subtitles Contact Sheet GUI"
	DESKTOP_CONTENT += "Comment=Generates video and subtitles previews, small animations, screenshots, thumbnails, etc."
	DESKTOP_CONTENT += "Type=Application"
	DESKTOP_CONTENT += "Exec=$${TARGET}"
	DESKTOP_CONTENT += "Terminal=false"
	DESKTOP_CONTENT += "Categories=AudioVideo"
	DESKTOP_CONTENT += "Icon=SCS.svg"
	write_file($$DESKTOP_FILE, DESKTOP_CONTENT)
	desktopinstall.CONFIG = no_check_exist
	desktopinstall.path = $${USR_DIR}$${DESKTOP_DIR}
	desktopinstall.files = $$DESKTOP_FILE
	INSTALLS += desktopinstall

	iconinstall.CONFIG = no_check_exist
	iconinstall.path = $${USR_DIR}$${ICONS_DIR}
	iconinstall.files += SCS.svg
	INSTALLS += iconinstall
}
