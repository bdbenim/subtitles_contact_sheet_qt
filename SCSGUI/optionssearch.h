#ifndef OPTIONSSEARCH_H
#define OPTIONSSEARCH_H

#include <QWidget>
#include <QListWidget>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QScrollArea>
#include <QPushButton>
#include "../paramssettings.h"

class OptionsSearch : public QWidget {
		Q_OBJECT
	public:
		explicit OptionsSearch(ParamsSettings *ps, QList<ParamsSettings::help_t> *ReturnList, QWidget *parent = nullptr);

	private:
		QList<ParamsSettings::help_t> *ReturnList;
		ParamsSettings *PS;
		QVBoxLayout *GeneralLayout;
		QLineEdit *Search;
		QListWidget *List;

		QWidget *ButtonsWidget;
		QHBoxLayout *ButtonsLayout;
		QPushButton *Add;
		QPushButton *Cancel;
	signals:
		void Acepted();
		void Canceled();

	public slots:
		void UpdateSearch(const QString &Search);
		void Clear();
		void AddClicked();
		void CancelClick();
};

#endif // OPTIONSSEARCH_H
