#include "paramswidget.h"
#include "paramwidget.h"
#include "paramboolwidget.h"
#include <QProcess>
#include <QFileInfo>
#include "optionssearchdialog.h"

ParamsWidget::ParamsWidget(QString &program, QStringList *configlist, ParamsSettings *ps, QWidget *parent)
	: QWidget{parent}, AutomaticUpdate(false) {

	Program = program;
	AvailableConfigs = configlist;
	PS = ps;
	MainLayout = new QVBoxLayout(this);
	MainLayout->setContentsMargins(0, 0, 0, 0);
	setLayout(MainLayout);
	Scroll = new QScrollArea(this);
	Scroll->setWidgetResizable(true);
	Scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	MainLayout->addWidget(Scroll);

	ButtonsWidget = new QWidget(this);
	ButtonsLayout = new QHBoxLayout(ButtonsWidget);
	ButtonsLayout->setContentsMargins(0, 0, 0, 0);
	AddParam = new QPushButton("Add Param", ButtonsWidget);
	RemoveParam = new QPushButton("Show Rmove Buttons", ButtonsWidget);
	RemoveParam->setCheckable(true);
	ButtonsLayout->addWidget(AddParam);
	ButtonsLayout->addWidget(RemoveParam);
	MainLayout->addWidget(ButtonsWidget);

	Command = new QLineEdit(this);
	MainLayout->addWidget(Command);

	ListWidget = new QWidget(Scroll);
	ListLayout = new QFormLayout(ListWidget);

	ListWidget->setLayout(ListLayout);
	Scroll->setWidget(ListWidget);

	connect(Command, &QLineEdit::textChanged, this, &ParamsWidget::LoadCommand);
	connect(AddParam, &QPushButton::clicked, this, &ParamsWidget::OpenSearch);
	connect(RemoveParam, &QPushButton::toggled, this, &ParamsWidget::TougleRemove);
}

void ParamsWidget::LoadParams(QString Command) {
	if (AutomaticUpdate) return;
	Command.replace("\"\"", "\"_*_\"");
	QStringList Params = QProcess::splitCommand(Command);
	LoadParams(Params);
}

void ParamsWidget::LoadParams(QStringList &Params) {
	for (int i = 0; i < Params.count(); ++i) {
		if (Params[i] == "_*_") Params[i] = "";
	}
	if (Params.count() == 0) return;

	Clean();

	while (!Params.isEmpty()) {
		if (Params.count() == 0) return;
		QString Param = Params.takeFirst();
		bool hasNext = false;
		QString NextParam = "";
		if (!Params.isEmpty()) {
			hasNext = true;
			NextParam = Params.first();
		}
		bool usedNext = false;

		if (Param == "--") {
			continue;
		} else if (PS->ProcessParameter(Param, hasNext, NextParam, usedNext)) {
			QString param_name = Param;
			param_name.remove(0, 2);
			if (usedNext) {
				AddItem(param_name, NextParam);
				Params.takeFirst();
			} else {
				AddItem(param_name, PS->Value(param_name));
			}
		} else if (Param.startsWith("--")) {
			QString param_name = Param;
			param_name.remove(0, 2);
			if (AvailableConfigs->contains(param_name)) {
				AddItem("config", param_name);
				continue;
			}
			ParamsSettings::help_t help = PS->GetHelp(param_name);
			if (help.Param == "") continue;
			if (help.DataType == ParamsSettings::param_bool && help.Type == "") {
				AddBool(param_name);
			} else if (help.DataType == ParamsSettings::param_shortcut) {
				if (!hasNext) break;
				Add(param_name, Params.takeFirst());
			} else {
				if (!hasNext) break;
				AddItem(param_name, Params.takeFirst());
			}

		} else {
			QFileInfo FileInfo(Param);
			QString Ext = FileInfo.suffix().toLower();
			if (SubtitleExtensions.contains(Ext)) {
				AddItem("sub", Param);
			} else if (VideoExtensions.contains(Ext)) {
				AddItem("video", Param);
			} else {
				continue;
			}
		}
	}
}

void ParamsWidget::LoadCommand(QString Command) {
	if (AutomaticUpdate) return;
	Command.replace("\"\"", "\"_*_\"");
	QStringList Params = QProcess::splitCommand(Command);
	Params.takeFirst();
	LoadParams(Params);
}

QString ParamsWidget::GetCommand() {
	return Program + " " + GetParams();
}

QString ParamsWidget::GetParams() {
	QString Result;
	for (int i = 0; i < ListLayout->rowCount(); ++i) {
		QWidget *W = ListLayout->itemAt(i, QFormLayout::FieldRole)->widget();
		QString command;
		if (QString(W->metaObject()->className()) == "ParamWidget") {
			ParamWidget *PW = static_cast<ParamWidget *>(W);
			command = PW->GetCommand();
		} else if (QString(W->metaObject()->className()) == "ParamBoolWidget") {
			ParamBoolWidget *PB = static_cast<ParamBoolWidget *>(W);
			command = PB->GetCommand();
		}
		if (command != "") Result += " " + command;
	}
	return Result;
}

void ParamsWidget::SetCommand(QString CommandText) {
	Command->setText(CommandText);
}

void ParamsWidget::SetParams(QString ParamsText) {
	LoadParams(ParamsText);
}

void ParamsWidget::SetInputFile(QString InputFile) {
	QFileInfo FileInfo(InputFile);
	QString Ext = FileInfo.suffix().toLower();
	if (SubtitleExtensions.contains(Ext)) {
		for (int i = 0; i < ListLayout->rowCount(); ++i) {
			QWidget *Item = ListLayout->itemAt(i, QFormLayout::FieldRole)->widget();
			if (Item == nullptr) continue;
			if (QString(Item->metaObject()->className()) != "ParamWidget") continue;
			ParamWidget *Editor = static_cast<ParamWidget *>(Item);
			if (Editor->Help.Param == "sub") {
				Editor->SetValue(InputFile);
				return;
			}
		}
	} else if (VideoExtensions.contains(Ext)) {
		for (int i = 0; i < ListLayout->rowCount(); ++i) {
			QWidget *Item = ListLayout->itemAt(i, QFormLayout::FieldRole)->widget();
			if (Item == nullptr) continue;
			if (QString(Item->metaObject()->className()) != "ParamWidget") continue;
			ParamWidget *Editor = static_cast<ParamWidget *>(Item);
			if (Editor->Help.Param == "video") {
				Editor->SetValue(InputFile);
				return;
			}
		}
	}
}

void ParamsWidget::Clean() {
	while (ListLayout->rowCount() > 0) {
		ListLayout->removeRow(ListLayout->rowCount() - 1);
	}
}

void ParamsWidget::AddItem(QString Param, QString Value) {
	ParamsSettings::help_t help = PS->GetHelp(Param);
	if (help.Param == "") return;
	ParamWidget *PW = new ParamWidget(PS, AvailableConfigs, help, Value, ListWidget);
	connect(PW, &ParamWidget::Updated, this, &ParamsWidget::Updated);
	connect(PW, &ParamWidget::Removed, this, &ParamsWidget::Removed);
	ListLayout->addRow(help.Name, PW);
}

void ParamsWidget::AddBool(QString Param) {
	if (Param == "time") {
		AddItem("mode", "time");
		return;
	} else if (Param == "line") {
		AddItem("mode", "line");
		return;
	} else if (Param == "jpg") {
		AddItem("format", "jpg");
		return;
	} else if (Param == "jpeg") {
		AddItem("format", "jpg");
		return;
	} else if (Param == "png") {
		AddItem("format", "png");
		return;
	} else if (Param == "apng") {
		AddItem("format", "apng");
		return;
	} else if (Param == "webp") {
		AddItem("format", "webp");
		return;
	} else if (Param == "webploop") {
		AddItem("format", "webploop");
		return;
	} else if (Param == "gif") {
		AddItem("format", "gif");
		return;
	} else if (Param == "giflow") {
		AddItem("format", "giflow");
		return;
	} else if (Param == "xvid") {
		AddItem("format", "xvid");
		return;
	} else if (Param == "x264") {
		AddItem("format", "x264");
		return;
	} else if (Param == "x265") {
		AddItem("format", "x265");
		return;
	} else if (Param == "tb_list_vars" || Param == "list_vars") {
		return;
	} else if (Param == "gui_progress") {
		return;
	} else if (Param == "save") {
		return;
	} else if (Param == "save_all") {
		return;
	}

	ParamsSettings::help_t help = PS->GetHelp(Param);
	if (help.Param == "") return;
	ParamBoolWidget *PB = new ParamBoolWidget(help, ListWidget);
	connect(PB, &ParamBoolWidget::Updated, this, &ParamsWidget::Updated);
	ListLayout->addRow(help.Name, PB);
}

void ParamsWidget::Add(QString Param, QString Value) {
	if (Param == "contact_sheet") {
		AddItem("layout", Value);
		AddItem("tb_pos", "up");
		AddItem("format", "jpg");
		AddItem("suffix", ".contact_sheet");
		AddItem("concat", "0");
		AddItem("no_grid", "0");
		AddItem("ft_hide", "0");
	} else if (Param == "gif_images") {
		AddItem("layout", Value + "x1");
		AddItem("format", "gif");
		AddItem("ft_hide", "1");
		AddItem("concat", "1");
		AddItem("video_fps", "1");
		AddItem("suffix", ".images");
	} else if (Param == "webp_images") {
		AddItem("layout", Value + "x1");
		AddItem("format", "webploop");
		AddItem("ft_hide", "1");
		AddItem("concat", "1");
		AddItem("video_fps", "1");
		AddItem("suffix", ".images");
	} else if (Param == "gif_clip") {
		AddItem("layout", Value + "x1");
		AddItem("format", "gif");
		AddItem("ft_hide", "1");
		AddItem("concat", "1");
		AddItem("video_preview", "1000");
		AddItem("video_fps", "12");
		AddItem("size", "320");
		AddItem("min_size", "0");
		AddItem("suffix", ".clip");
	} else if (Param == "gif_singles") {
		AddItem("layout", Value + "x1");
		AddItem("format", "gif");
		AddItem("ft_hide", "1");
		AddItem("video_preview", "1000");
		AddItem("video_fps", "12");
		AddItem("size", "320");
		AddItem("min_size", "0");
		AddItem("suffix", ".clip");
	} else if (Param == "webp_clip") {
		AddItem("layout", Value + "x1");
		AddItem("format", "webploop");
		AddItem("ft_hide", "1");
		AddItem("concat", "1");
		AddItem("video_preview", "1000");
		AddItem("video_fps", "12");
		AddItem("min_size", "0");
		AddItem("suffix", ".clip");
	} else if (Param == "screenshots") {
		AddItem("format", "png");
		AddItem("ft_hide", "1");
		AddItem("no_grid", "1");
		AddItem("concat", "0");
		AddItem("suffix", ".screenshot_");
	} else {
		AddItem(Param, Value);
	}
}

void ParamsWidget::HideCommand() {
	Command->setVisible(false);
}

void ParamsWidget::Updated() {
	AutomaticUpdate = true;
	Command->setText(GetCommand());
	AutomaticUpdate = false;
}

void ParamsWidget::Removed(QWidget *RemovedObject) {
	ListLayout->removeRow(RemovedObject);
	Updated();
}

void ParamsWidget::OpenSearch() {
	OptionsSearchDialog *OS = new OptionsSearchDialog(PS, this);
	if (OS->exec() == QDialog::Accepted) {
		QList<ParamsSettings::help_t> *ReturnList = OS->GetReturnList();
		for (int i = 0; i < ReturnList->count(); ++i) {
			ParamsSettings::help_t help = ReturnList->at(i);
			ParamsSettings::params_t param = PS->GetVar(help.Param);
			if (help.DataType == ParamsSettings::param_bool)
				AddBool(help.Param);
			else
				AddItem(help.Param, param.DefaultValue);
		}
		Updated();
	}
}

void ParamsWidget::TougleRemove(bool Presed) {
	for (int i = 0; i < ListLayout->rowCount(); ++i) {
		QWidget *W = ListLayout->itemAt(i, QFormLayout::FieldRole)->widget();
		if (QString(W->metaObject()->className()) == "ParamWidget") {
			ParamWidget *PW = static_cast<ParamWidget *>(W);
			PW->TougleRemoveVisible(Presed);
			//		} else if (QString(W->metaObject()->className()) == "ParamBoolWidget") {
			//			ParamBoolWidget *PB = static_cast<ParamBoolWidget *>(W);
			//			command = PB->GetCommand();
		}
	}
}
