#ifndef CONFIGURATIONWIDGET_H
#define CONFIGURATIONWIDGET_H

#include <QTabWidget>
#include <QHash>
#include "../paramssettings.h"

class ConfigurationWidget : public QTabWidget {
		Q_OBJECT
	public:
		explicit ConfigurationWidget(ParamsSettings *ps, QWidget *parent = nullptr);
		explicit ConfigurationWidget(QString ConfigName, QWidget *parent = nullptr);

		ParamsSettings *PS;

		QHash<QString, int> Tabs;
		QHash<QString, QWidget *> TabsWidgets;
		QList<ParamsSettings::help_t> Options;

	private:
		void Load();

	signals:

	private slots:
		void OptionChanged(QString Param, QString Value);
		void UserDefinedChanged(QString Param, QString Name, QString Value);

};

#endif // CONFIGURATIONWIDGET_H
