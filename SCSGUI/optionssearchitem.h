#ifndef OPTIONSSEARCHITEM_H
#define OPTIONSSEARCHITEM_H

#include <QWidget>
#include <QListWidget>
#include <QVBoxLayout>
#include <QFrame>
#include <QLabel>
#include "../paramssettings.h"

class OptionsSearchItem : public QFrame {
	public:
		OptionsSearchItem(const ParamsSettings::help_t &Help, QWidget *parent = nullptr,
						  const Qt::WindowFlags &f = Qt::WindowFlags());

		ParamsSettings::help_t *GetHelp();
	private:
		ParamsSettings::help_t Help;
		QWidget *Widget;
		QVBoxLayout *Layout;
		QLabel *Name;
		QLabel *Text;

	signals:

};

#endif // OPTIONSSEARCHITEM_H
