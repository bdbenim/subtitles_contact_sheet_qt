#include "batchowncommandwidget.h"
#include <QApplication>
#include "paramsdialog.h"
#include "../paramssettings.h"

BatchOwnCommandWidget::BatchOwnCommandWidget(QStringList *Configs, QWidget *parent)
	: QWidget{parent} {
	AvailableConfigs = Configs;
	Layout = new QHBoxLayout(this);
	Config = new QComboBox(this);
	CustomParameters = new QLineEdit(this);
	Config->addItems(*AvailableConfigs);
	EditParamsButton = new QPushButton("Edit", this);
	DeleteButton = new QPushButton("Remove", this);

	Layout->addWidget(Config);
	Layout->addWidget(CustomParameters);
	Layout->addWidget(EditParamsButton);
	Layout->addWidget(DeleteButton);

	connect(DeleteButton, &QPushButton::clicked, this, &BatchOwnCommandWidget::RemoveClick);
	connect(EditParamsButton, &QPushButton::clicked, this, &BatchOwnCommandWidget::EditClick);
	connect(Config, &QComboBox::currentTextChanged, this, &BatchOwnCommandWidget::EmitChanged);
	connect(CustomParameters, &QLineEdit::textChanged, this, &BatchOwnCommandWidget::EmitChanged);
}

QString BatchOwnCommandWidget::GetCommand() {
	QString Commands = "";
	if (Config->currentText() != "") {
		if (Commands != "") Commands += " ";
		Commands += "--config " + Config->currentText();
	}
	if (CustomParameters->text() != "") {
		if (Commands != "") Commands += " ";
		Commands += CustomParameters->text();
	}
	return Commands;
}

void BatchOwnCommandWidget::SetCommand(QStringList *parameters) {
	QString text = "";
	while (!parameters->empty()) {
		QString parameter = parameters->takeFirst();
		if (parameter == "--config") {
			if (parameters->empty()) break;
			QString config = parameters->takeFirst();
			Config->setCurrentText(config);
		} else {
			if (text != "") text += " ";
			text += parameter;
		}
	}
	CustomParameters->setText(text);
}

void BatchOwnCommandWidget::RemoveClick() {
	emit Deleted(this);
}

void BatchOwnCommandWidget::EmitChanged() {
	emit Changed();
}

void BatchOwnCommandWidget::EditClick() {
	QString Program;
	ParamsSettings *PS = new ParamsSettings(QApplication::applicationDirPath(), this);
	ParamsDialog *Dia = new ParamsDialog(Program, AvailableConfigs, PS, this);
	Dia->SetParams(CustomParameters->text());
	if (Dia->exec() == QDialog::Accepted) {
		CustomParameters->setText(Dia->GetParams());
	}
}
