#ifndef USEROPTIONEDITOR_H
#define USEROPTIONEDITOR_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QStringList>
#include "../paramssettings.h"

class UserOptionEditor : public QWidget {
		Q_OBJECT
	public:
		explicit UserOptionEditor(QString ParamName, ParamsSettings::params_t param, QWidget *parent = nullptr);
		QString ParamName;
		ParamsSettings::params_t Param;

		QHBoxLayout *Layout;

		QWidget *VarWidget;
		QHBoxLayout *VarLayout;
		QLineEdit *VarName;
		QLabel *EqualLabel;
		QLineEdit *VarValue;

		QLineEdit *RawEditor;
		QPushButton *RawTougle;

		bool RawBlock;
		bool ValueBlock;

	signals:
		void Changed(QString Param, QString Name, QString Value);

	public slots:
		void RawTougleChange(bool Checked);
		void ValueChanged();
		void RawChange();
};

#endif // USEROPTIONEDITOR_H
