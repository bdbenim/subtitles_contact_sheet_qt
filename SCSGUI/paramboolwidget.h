#ifndef PARAMBOOLWIDGET_H
#define PARAMBOOLWIDGET_H

#include <QWidget>
#include "../paramssettings.h"
#include <QCheckBox>
#include <QHBoxLayout>

class ParamBoolWidget : public QWidget {
		Q_OBJECT
	public:
		explicit ParamBoolWidget(ParamsSettings::help_t help, QWidget *parent = nullptr);

		QString GetCommand();
	private:
		QHBoxLayout *Layout;
		QCheckBox *Check;
		ParamsSettings::help_t Help;
		bool Value;
	signals:
		void Updated();
	public slots:
		void UpdatedCheck(int State);

};

#endif // PARAMBOOLWIDGET_H
