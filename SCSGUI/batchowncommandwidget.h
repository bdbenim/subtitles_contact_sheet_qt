#ifndef BATCHOWNCOMMANDWIDGET_H
#define BATCHOWNCOMMANDWIDGET_H

#include <QWidget>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QPushButton>
#include <QComboBox>

class BatchOwnCommandWidget : public QWidget {
		Q_OBJECT
	public:
		explicit BatchOwnCommandWidget(QStringList *ConfigList, QWidget *parent = nullptr);

		QHBoxLayout *Layout;
		QLineEdit *CustomParameters;
		QComboBox *Config;
		QPushButton *EditParamsButton;
		QPushButton *DeleteButton;

		QString GetCommand();
		void SetCommand(QStringList *parameters);

	private:
		QStringList *AvailableConfigs;

	signals:
		void Deleted(QWidget *Item);
		void Changed();

	public slots:
		void RemoveClick();
		void EmitChanged();
		void EditClick();

};

#endif // BATCHOWNCOMMANDWIDGET_H
