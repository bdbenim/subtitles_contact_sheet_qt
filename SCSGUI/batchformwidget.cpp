#include "batchformwidget.h"
#include <QFileDialog>
#include <QList>
#include <QStringList>
#include <QMimeData>
#include <QDragEnterEvent>
#include <QProcess>

BatchFormWidget::BatchFormWidget(QString &program, QStringList *configlist, QWidget *parent)
	: QWidget{parent},
	  CommandCounter(0) {
	Program = program;
	AvailableConfigs = configlist;
	this->setAcceptDrops(true);
	CentralLayout = new QVBoxLayout(this);
	CentralLayout->setContentsMargins(0, 0, 0, 0);

	MainSplitter = new QSplitter(Qt::Horizontal, this);
	CentralLayout->addWidget(MainSplitter);

	Command = new QLineEdit(this);
	CentralLayout->addWidget(Command);

	LeftWidget = new QWidget(this);
	LeftLayout = new QVBoxLayout(LeftWidget);
	//	LeftLayout->setContentsMargins(0,0,0,0);
	MainSplitter->addWidget(LeftWidget);

	RightWidget = new QWidget(this);
	RightLayout = new QVBoxLayout(RightWidget);
	//	RightLayout->setContentsMargins(0,0,0,0);
	MainSplitter->addWidget(RightWidget);

	InputList = new QListWidget(LeftWidget);
	InputList->setViewMode(QListView::ListMode);
	InputList->setSelectionMode(QAbstractItemView::ExtendedSelection);

	InputButtonsWidget = new QWidget(LeftWidget);
	InputButtonsLayout = new QHBoxLayout(InputButtonsWidget);
	InputButtonsLayout->setContentsMargins(0, 0, 0, 0);
	InputButtonsWidget->setLayout(InputButtonsLayout);

	LeftLayout->addWidget(InputList);
	LeftLayout->addWidget(InputButtonsWidget);

	AddFile = new QPushButton("Add File", InputButtonsWidget);
	connect(AddFile, &QPushButton::clicked, this, &BatchFormWidget::AddFileClick);

	AddDir = new QPushButton("Add Folder", InputButtonsWidget);
	connect(AddDir, &QPushButton::clicked, this, &BatchFormWidget::AddDirClick);

	DelInput = new QPushButton("Remove", InputButtonsWidget);
	connect(DelInput, &QPushButton::clicked, this, &BatchFormWidget::RemoveClick);

	InputButtonsLayout->addWidget(AddFile);
	InputButtonsLayout->addWidget(AddDir);
	InputButtonsLayout->addWidget(DelInput);

	OptionsRow1Widget = new QWidget(RightWidget);
	OptionsRow1Layout = new QHBoxLayout(OptionsRow1Widget);
	OptionsRow1Layout->getContentsMargins(0, 0, 0, 0);
	RightLayout->addWidget(OptionsRow1Widget);

	OptionsGroup = new QGroupBox("Options", RightWidget);
	OptionsLayout = new QVBoxLayout(OptionsGroup);
	OptionsRow1Layout->addWidget(OptionsGroup);

	NProcessWidget = new QWidget(OptionsGroup);
	NProcessLayout = new QHBoxLayout(NProcessWidget);
	NProcessLayout->setContentsMargins(0, 0, 0, 0);
	NProcessLabel = new QLabel("Parellel process", NProcessWidget);
	NProcess = new QSpinBox(NProcessWidget);
	NProcess->setMaximum(16);
	NProcess->setMinimum(1);
	NProcessLayout->addWidget(NProcessLabel);
	NProcessLayout->addWidget(NProcess);
	OptionsLayout->addWidget(NProcessWidget);

	Recursive = new QCheckBox("Recursive", OptionsGroup);
	IgnoreFail = new QCheckBox("Ignore Fail", OptionsGroup);
	ShowCMD = new QCheckBox("Only show CMD", OptionsGroup);
	Quiet = new QCheckBox("Quiet", OptionsGroup);
	ShowOutput = new QCheckBox("Show Output", OptionsGroup);

	OptionsLayout->addWidget(Recursive);
	OptionsLayout->addWidget(IgnoreFail);
	OptionsLayout->addWidget(ShowCMD);
	OptionsLayout->addWidget(Quiet);
	OptionsLayout->addWidget(ShowOutput);

	FileTypesGroup = new QGroupBox("File types", RightWidget);
	FileTypesLayout = new QVBoxLayout(FileTypesGroup);
	OptionsRow1Layout->addWidget(FileTypesGroup);

	VideoFiles = new QCheckBox("Video", FileTypesGroup);
	SubtitlesFiles = new QCheckBox("Subtitles", FileTypesGroup);
	SubtitlesFiles->setObjectName("subs");
	FileTypesLayout->addWidget(VideoFiles);
	FileTypesLayout->addWidget(SubtitlesFiles);

	CustomFileExtWidget = new QWidget(FileTypesGroup);
	CustomFileExtLayout = new QHBoxLayout(CustomFileExtWidget);
	CustomFileExtLayout->setContentsMargins(0, 0, 0, 0);
	CustomFileExtLabel = new QLabel("Extensions list", CustomFileExtWidget);
	CustomFileExt = new QLineEdit(FileTypesGroup);
	CustomFileExtLayout->addWidget(CustomFileExtLabel);
	CustomFileExtLayout->addWidget(CustomFileExt);
	FileTypesLayout->addWidget(CustomFileExtWidget);
	Spacer = new QSpacerItem(1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding);
	FileTypesLayout->addSpacerItem(Spacer);


	DaemonGroup = new QGroupBox("Backgound Options", OptionsRow1Widget);
	DaemonLayout = new QFormLayout(DaemonGroup);
	CustomFileExtLayout->setContentsMargins(0, 0, 0, 0);
	Watch = new QCheckBox(DaemonGroup);
	Time = new QSpinBox(DaemonGroup);
	Time->setMaximum(INT_MAX);
	Time->setMinimum(0);
	Modified = new QSpinBox(DaemonGroup);
	Modified->setMaximum(INT_MAX);
	Modified->setMinimum(0);
	Recheck = new QSpinBox(DaemonGroup);
	Recheck->setMaximum(INT_MAX);
	Recheck->setMinimum(0);
	Delete = new QSpinBox(DaemonGroup);
	Delete->setMaximum(INT_MAX);
	Delete->setMinimum(0);
	DaemonLayout->addRow("Watch Filesistem", Watch);
	DaemonLayout->addRow("Search Time", Time);
	DaemonLayout->addRow("Modification Time Delay", Modified);
	DaemonLayout->addRow("Recheck Time", Recheck);
	DaemonLayout->addRow("Deleted Check Time", Delete);
	OptionsRow1Layout->addWidget(DaemonGroup);

	BatchCommand = new BatchCommandsWidget(AvailableConfigs, RightWidget);
	RightLayout->addWidget(BatchCommand);

	RightSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
	RightLayout->addSpacerItem(RightSpacer);

	connect(InputList, &QListWidget::itemChanged, this, &BatchFormWidget::CommandChanged);
	connect(VideoFiles, &QCheckBox::stateChanged, this, &BatchFormWidget::CommandChanged);
	connect(SubtitlesFiles, &QCheckBox::stateChanged, this, &BatchFormWidget::CommandChanged);
	connect(Recursive, &QCheckBox::stateChanged, this, &BatchFormWidget::CommandChanged);
	connect(IgnoreFail, &QCheckBox::stateChanged, this, &BatchFormWidget::CommandChanged);
	connect(ShowCMD, &QCheckBox::stateChanged, this, &BatchFormWidget::CommandChanged);
	connect(Quiet, &QCheckBox::stateChanged, this, &BatchFormWidget::CommandChanged);
	connect(ShowOutput, &QCheckBox::stateChanged, this, &BatchFormWidget::CommandChanged);
	connect(CustomFileExt, &QLineEdit::textChanged, this, &BatchFormWidget::CommandChanged);
	connect(BatchCommand, &BatchCommandsWidget::Changed, this, &BatchFormWidget::CommandChanged);
	connect(NProcess, &QSpinBox::textChanged, this, &BatchFormWidget::CommandChanged);
	connect(Watch, &QCheckBox::stateChanged, this, &BatchFormWidget::CommandChanged);
	connect(Time, &QSpinBox::textChanged, this, &BatchFormWidget::CommandChanged);
	connect(Modified, &QSpinBox::textChanged, this, &BatchFormWidget::CommandChanged);
	connect(Recheck, &QSpinBox::textChanged, this, &BatchFormWidget::CommandChanged);
	connect(Delete, &QSpinBox::textChanged, this, &BatchFormWidget::CommandChanged);

	connect(Command, &QLineEdit::textChanged, this, &BatchFormWidget::CommandManualChanged);
}

void BatchFormWidget::AddFileClick() {
	QStringList Files = QFileDialog::getOpenFileNames(this, "Add File", "",
						"Suported Files (*.mkv *.mp4 *.avi *.mov *.wmv *.srt *.ass *.vtt);;"
						"Video Files (*.mkv *.mp4 *.avi *.mov *.wmv);;"
						"Subtitles Files (*.srt *.ass *.vtt);;"
						"All Files (*)");
	for (int i = 0; i < Files.count(); ++i) {
		InputList->addItem(QDir::toNativeSeparators(Files.at(i)));
	}
	CommandChanged();
}

void BatchFormWidget::AddDirClick() {
	QString Dir = QFileDialog::getExistingDirectory(this,
				  tr("Add Directory"),
				  "",
				  QFileDialog::ShowDirsOnly
				  | QFileDialog::DontResolveSymlinks);
	if (Dir != "") InputList->addItem(QDir::toNativeSeparators(Dir));
	CommandChanged();
}

void BatchFormWidget::RemoveClick() {
	QList<QListWidgetItem *> List = InputList->selectedItems();
	for (int i = 0; i < List.count(); ++i) {
		int row = InputList->row(List.at(i));
		InputList->takeItem(row);
		delete List.at(i);
	}
	CommandChanged();
}

void BatchFormWidget::CommandChanged() {
	AutomaticChange = true;
	if (!Loading) Command->setText(GetCommand());
	AutomaticChange = false;
}

void BatchFormWidget::CommandManualChanged() {
	if (!AutomaticChange) LoadCommand(Command->text());
}

void BatchFormWidget::dragEnterEvent(QDragEnterEvent *e) {
	if (e->mimeData()->hasUrls()) {
		e->acceptProposedAction();
	}
}

void BatchFormWidget::dropEvent(QDropEvent *e) {
	foreach (const QUrl &url, e->mimeData()->urls()) {
		QString fileName = url.toLocalFile();
		InputList->addItem(fileName);
	}
	CommandChanged();
}

void BatchFormWidget::LoadCommand(QString Command) {
	Loading = true;
	QStringList Params = QProcess::splitCommand(Command);
	if (Params.count() == 0) return;
	QString program = Params.takeFirst();

	InputList->clear();
	CustomFileExt->setText("");
	Recursive->setCheckState(Qt::Unchecked);
	VideoFiles->setCheckState(Qt::Unchecked);
	SubtitlesFiles->setCheckState(Qt::Unchecked);
	IgnoreFail->setCheckState(Qt::Unchecked);
	ShowCMD->setCheckState(Qt::Unchecked);
	ShowOutput->setCheckState(Qt::Unchecked);
	Quiet->setCheckState(Qt::Unchecked);
	Watch->setCheckState(Qt::Unchecked);

	while (!Params.isEmpty()) {
		if (Params.count() == 0) return;
		QString Param = Params.takeFirst();

		if (Param == "-r" || Param == "--recursive" ) {
			Recursive->setCheckState(Qt::Checked);
		} else if (Param == "-f" || Param == "--file") {
			QString filetext = CustomFileExt->text();
			if (Params.count() == 0) return;
			QString fileparam = Params.takeFirst();
			if (filetext != "") filetext += ",";
			CustomFileExt->setText(filetext + fileparam);
		} else if (Param == "--video") {
			VideoFiles->setCheckState(Qt::Checked);
		} else if (Param == "--subs") {
			SubtitlesFiles->setCheckState(Qt::Checked);
		} else if (Param == "--no_fail") {
			IgnoreFail->setCheckState(Qt::Checked);
		} else if (Param == "-c" || Param == "--commnad") {
			if (Params.count() == 0) return;
			//TODO:
			Params.takeFirst();
		} else if (Param == "-p" || Param == "--parallel") {
			if (Params.count() == 0) return;
			NProcess->setValue(Params.takeFirst().toInt());
		} else if (Param == "-t" || Param == "--time") {
			if (Params.count() == 0) return;
			Time->setValue(Params.takeFirst().toInt());
		} else if (Param == "-d" || Param == "--delete") {
			if (Params.count() == 0) return;
			Delete->setValue(Params.takeFirst().toInt());
		} else if (Param == "-w" || Param == "--watch") {
			Watch->setCheckState(Qt::Checked);
		} else if (Param == "-m" || Param == "--modified") {
			if (Params.count() == 0) return;
			Modified->setValue(Params.takeFirst().toInt());
		} else if (Param == "--recheck") {
			if (Params.count() == 0) return;
			Recheck->setValue(Params.takeFirst().toInt());
		} else if (Param == "--cmd") {
			ShowCMD->setCheckState(Qt::Checked);
		} else if (Param == "--show_output") {
			ShowOutput->setCheckState(Qt::Checked);
		} else if (Param == "-q" || Param == "--quiet") {
			Quiet->setCheckState(Qt::Checked);
		} else if (Param == "--") {
			BatchCommand->SetCommand(Params);
			break;
		} else {
			QFileInfo CheckInputFile(Param);
			if (CheckInputFile.isDir()) {
				InputList->addItem(Param);
			} else if (CheckInputFile.isFile()) {
				InputList->addItem(Param);
			}
		}
	}
	CurrentCommand = Command;
	Loading = false;
}

QString BatchFormWidget::GetCommand() {
	QStringList result;
	if (InputList->count() == 0) return "";

	for (int i = 0; i < InputList->count(); ++i) {
		QListWidgetItem *Cur = InputList->item(i);
		QString text = Cur->text();
		if (text.contains(" ")) text = "\"" + text + "\"";
		result.append(text);
	}

	result.append("--parallel " + QString::number(NProcess->value()));
	if (Recursive->isChecked()) result.append("-r");
	if (VideoFiles->isChecked()) result.append("--video");
	if (SubtitlesFiles->isChecked()) result.append("--subs");
	if (CustomFileExt->text() != "") {
		QStringList Extensions = CustomFileExt->text().split(",");
		for (int i = 0; i < Extensions.count(); ++i) {
			result.append("-f");
			result.append("\"" + Extensions.at(i) + "\"");
		}
	}
	if (IgnoreFail->isChecked()) result.append("--no_fail");
	if (ShowCMD->isChecked()) result.append("--cmd");
	if (ShowOutput->isChecked()) result.append("--show_output");
	if (Quiet->isChecked()) result.append("-q");
	if (Watch->isChecked() || Time->value() > 0) {
		if (Watch->isChecked()) result.append("--watch");
		if (Time->value() > 0) result.append("--time " + QString::number(Time->value()));
		if (Delete->value() > 0) result.append("--delete " + QString::number(Delete->value()));
		if (Modified->value() > 0) result.append("--modified " + QString::number(Modified->value()));
		if (Recheck->value() > 0) result.append("--recheck " + QString::number(Recheck->value()));
	}
	QString Commands = BatchCommand->getCommands();
	if (Commands != "") Commands = " -- " + Commands;
	return Program + " --batch " + result.join(" ") + Commands;
}
