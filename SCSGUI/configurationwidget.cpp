#include "configurationwidget.h"
#include <QFormLayout>
#include <QLabel>
#include <QScrollArea>
#include <QLineEdit>
#include <QSpinBox>
#include <QCheckBox>
#include <QDoubleSpinBox>
#include "optioneditor.h"
#include "useroptioneditor.h"
#include <QApplication>
#include "../textsustitution.h"

ConfigurationWidget::ConfigurationWidget(ParamsSettings *ps, QWidget *parent)
	: QTabWidget{parent} {
	PS = ps;
	Load();
}

ConfigurationWidget::ConfigurationWidget(QString ConfigName, QWidget *parent)
	: QTabWidget{parent} {
	PS = new ParamsSettings(QApplication::applicationDirPath(), this);
	PS->SetConfig(ConfigName);
	PS->ReadFromConfigFile();
	Load();
}

void ConfigurationWidget::Load() {
	Options = PS->SearchHelp("", true);

	for (int i = 0; i < Options.count(); ++i) {
		ParamsSettings::help_t option = Options.at(i);
		QWidget *Widget = nullptr;
		QFormLayout *Layout = nullptr;
		if (!Tabs.contains(option.Prefix)) {
			QScrollArea *Scroll = new QScrollArea(this);
			Scroll->setWidgetResizable(true);
			Scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
			Widget = new QWidget(Scroll);
			Layout = new QFormLayout(Widget);
			Widget->setLayout(Layout);
			Scroll->setWidget(Widget);
			int tabid = addTab(Scroll, PS->GetPrefixName(option.Prefix));
			Tabs.insert(option.Prefix, tabid);
			TabsWidgets.insert(option.Prefix, Widget);
		} else {
			Widget = TabsWidgets.value(option.Prefix);
			Layout = static_cast<QFormLayout *>(Widget->layout());
		}
		ParamsSettings::params_t Param = PS->GetVar(option.Param);
		OptionEditor *Editor = new OptionEditor(option.Param, Param, option, Widget);
		connect(Editor, &OptionEditor::Changed, this, &ConfigurationWidget::OptionChanged);
		Layout->addRow(option.Name, Editor);
	}

	//User Defined Vars
	QScrollArea *Scroll = new QScrollArea(this);
	Scroll->setWidgetResizable(true);
	Scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	QWidget *Widget = new QWidget(Scroll);
	QFormLayout *Layout = new QFormLayout(Widget);
	Widget->setLayout(Layout);
	Scroll->setWidget(Widget);
	int tabid = addTab(Scroll, "User Defined");
	Tabs.insert("UserDefined", tabid);
	TabsWidgets.insert("UserDefined", Widget);

	QStringList ListParams = PS->Params();
	ListParams.sort();
	for (int i = 0; i < ListParams.size(); ++i) {
		QString Option = ListParams.at(i);
		if (!PS->IsUserDefined(Option)) continue;
		ParamsSettings::context_type Context;
		QString VarName;
		if (!PS->GetVarAndContext(Option, VarName, Context)) continue;
		ParamsSettings::params_t Param = PS->GetVar(Option);
		ParamsSettings::help_t Help;
		switch (Context) {
			case ParamsSettings::context_global:
				Help = PS->GetHelp("var");
				break;
			case ParamsSettings::context_stream:
				Help = PS->GetHelp("var_stream");
				break;
			case ParamsSettings::context_video:
				Help = PS->GetHelp("var_stream_video");
				break;
			case ParamsSettings::context_audio:
				Help = PS->GetHelp("var_stream_audio");
				break;
			case ParamsSettings::context_subtitle:
				Help = PS->GetHelp("var_stream_subtitle");
				break;
			case ParamsSettings::context_attachment:
				Help = PS->GetHelp("var_stream_attachment");
				break;
		}
		UserOptionEditor *Editor = new UserOptionEditor(Option, Param, Widget);
		connect(Editor, &UserOptionEditor::Changed, this, &ConfigurationWidget::UserDefinedChanged);
		Layout->addRow(Help.Name, Editor);
	}
}

void ConfigurationWidget::OptionChanged(QString Param, QString Value) {
	PS->setValue(Param, Value);
}

void ConfigurationWidget::UserDefinedChanged(QString Param, QString Name, QString Value) {
	if (!PS->Exists(Param)) return;
	ParamsSettings::params_t Option = PS->GetVar(Param);
	Option.UserName = Name;
	if (TextSustitution::DetectFunction(Value))
		Option.Function = Value;
	else
		Option.Value = Value;
	Option.Changed = true;
	PS->SetVar(Param, Option);
}
