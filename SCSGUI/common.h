#ifndef COMMON_H
#define COMMON_H

#include <QString>
#include <QStringList>

void PrintInfo(QString line);
void PrintDebug(QString line);
void PrintError(QString line);
void PrintProgress(int step, int total);

#define DEBUGVAR(var)
void DebugVarFunt(QString opt, QString var);
void DebugVarFunt(QString opt, uint64_t var);

QString ReadProgram(QString program, QStringList params);
QString ReadFile(QString Filename);

#endif // COMMON_H
