#include "batchcommandswidget.h"
#include "batchowncommandwidget.h"

BatchCommandsWidget::BatchCommandsWidget(QStringList *Configs, QWidget *parent)
	: QWidget{parent}, Count(0) {
	AvailableConfigs = Configs;

	MainLayout = new QVBoxLayout(this);

	CommandsGroup = new QGroupBox("Commands", this);
	CommandsLayout = new QVBoxLayout(CommandsGroup);
	MainLayout->addWidget(CommandsGroup);

	ButtonsWidget = new QWidget(this);
	ButtonsLayout = new QHBoxLayout(ButtonsWidget);
	MainLayout->addWidget(ButtonsWidget);

	AddButton = new QPushButton("Add Command", this);
	ButtonsLayout->addWidget(AddButton);

	connect(AddButton, &QPushButton::clicked, this, &BatchCommandsWidget::AddCommandClick);
}

void BatchCommandsWidget::SetCommand(QStringList Commands) {
	Clear();

	QStringList WidgetCommands;
	for (int i = 0; i < Commands.count(); ++i) {
		QString Command = Commands.at(i);
		if (Command == "--") {
			AddCommand(&WidgetCommands);
			WidgetCommands.clear();
		} else {
			WidgetCommands.append(Command);
		}
	}
	AddCommand(&WidgetCommands);
}

QString BatchCommandsWidget::getCommands() {
	QString Commands = "";
	for (int i = 0; i < Items.count(); ++i) {
		BatchOwnCommandWidget *Item = static_cast<BatchOwnCommandWidget *>(Items.at(i));
		if (Commands != "" ) Commands += " -- ";
		Commands += Item->GetCommand();
	}
	return Commands;
}

void BatchCommandsWidget::AddCommand(QStringList *Commands) {
	BatchOwnCommandWidget *Command = new BatchOwnCommandWidget(AvailableConfigs, CommandsGroup);
	if (Commands != nullptr) Command->SetCommand(Commands);
	Command->setObjectName("Command_" + QString::number(++Count));
	CommandsLayout->addWidget(Command);
	Items.insert(Count, Command);
	connect(Command, &BatchOwnCommandWidget::Deleted, this, &BatchCommandsWidget::DeletedItem);
	connect(Command, &BatchOwnCommandWidget::Changed, this, &BatchCommandsWidget::ItemChanged);
}

void BatchCommandsWidget::AddCommandClick() {
	AddCommand();
}

void BatchCommandsWidget::DeletedItem(QWidget *Item) {
	Items.removeOne(Item);
	CommandsLayout->removeWidget(Item);
	delete Item;
}

void BatchCommandsWidget::Clear() {
	while (Items.count() > 0) {
		QWidget *item = Items.takeLast();
		CommandsLayout->removeWidget(item);
		delete item;
	}
	Count = 0;
}

void BatchCommandsWidget::ItemChanged() {
	emit Changed();
}
