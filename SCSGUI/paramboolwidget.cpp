#include "paramboolwidget.h"

ParamBoolWidget::ParamBoolWidget(ParamsSettings::help_t help, QWidget *parent)
	: QWidget{parent} {
	Help = help;
	Check = new QCheckBox(this);
	Check->setToolTip(Help.Text);
	Check->setChecked(true);
	Value = true;
	Layout = new QHBoxLayout(this);
	setLayout(Layout);
	Layout->addWidget(Check);
	connect(Check, &QCheckBox::stateChanged, this, &ParamBoolWidget::UpdatedCheck);
}

QString ParamBoolWidget::GetCommand() {
	if (Value)
		return "--" + Help.Param;
	else
		return "";
}

void ParamBoolWidget::UpdatedCheck(int State) {
	if (State == Qt::Checked)
		Value = true;
	else
		Value = false;
	emit Updated();
}
