#include "optionssearch.h"
#include "optionssearchitem.h"

OptionsSearch::OptionsSearch(ParamsSettings *ps, QList<ParamsSettings::help_t> *ReturnList, QWidget *parent)
	: QWidget{parent}, ReturnList(ReturnList) {
	PS = ps;

	GeneralLayout = new QVBoxLayout(this);
	GeneralLayout->setContentsMargins(0, 0, 0, 0);
	Search = new QLineEdit(this);
	GeneralLayout->addWidget(Search);

	List = new QListWidget(this);
	List->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	List->setMinimumSize(600, 400);
	GeneralLayout->addWidget(List);

	ButtonsWidget = new QWidget(this);
	ButtonsLayout = new QHBoxLayout(ButtonsWidget);
	ButtonsLayout->setContentsMargins(0, 0, 0, 0);
	Cancel = new QPushButton("Cancel", ButtonsWidget);
	Add = new QPushButton("Add", ButtonsWidget);
	ButtonsLayout->addWidget(Cancel);
	ButtonsLayout->addWidget(Add);
	GeneralLayout->addWidget(ButtonsWidget);

	connect(Search, &QLineEdit::textChanged, this, &OptionsSearch::UpdateSearch);
	connect(Cancel, &QPushButton::clicked, this, &OptionsSearch::CancelClick);
	connect(Add, &QPushButton::clicked, this, &OptionsSearch::AddClicked);
	connect(List, &QListWidget::itemDoubleClicked, this, &OptionsSearch::AddClicked);
	UpdateSearch("");
}

void OptionsSearch::UpdateSearch(const QString &Search) {
	Clear();
	QList<ParamsSettings::help_t> Results = PS->SearchHelp(Search, false);

	for (int i = 0; i < Results.count(); ++i) {
		ParamsSettings::help_t Result = Results.at(i);
		QListWidgetItem *Item = new QListWidgetItem();
		OptionsSearchItem *Widget = new OptionsSearchItem(Result, List);
		Item->setSizeHint(Widget->sizeHint());
		List->addItem(Item);
		List->setItemWidget(Item, Widget);
	}
}

void OptionsSearch::Clear() {
	List->clear();
}

void OptionsSearch::AddClicked() {
	ReturnList->clear();
	QList<QListWidgetItem *> Selected = List->selectedItems();
	for (int i = 0; i < Selected.count(); ++i) {
		QListWidgetItem *Item = Selected.at(i);
		OptionsSearchItem *Widget = static_cast<OptionsSearchItem *>(List->itemWidget(Item));
		ReturnList->append(*Widget->GetHelp());
	}
	emit Acepted();
}

void OptionsSearch::CancelClick() {
	ReturnList->clear();
	emit Canceled();
}
