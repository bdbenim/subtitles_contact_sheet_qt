#include "paramwidget.h"
#include <QFileDialog>

ParamWidget::ParamWidget(ParamsSettings *ps, QStringList *configlist, ParamsSettings::help_t help, QString value,
						 QWidget *parent)
	: QWidget{parent} {
	PS = ps;
	Help = help;
	AvailableConfigs = configlist;
	Value = value;
	Load();
}

ParamWidget::ParamWidget(ParamsSettings *ps, QStringList *configlist, QString param, QString value, QWidget *parent)
	: QWidget{parent} {
	PS = ps;
	Help = PS->GetHelp(param);
	AvailableConfigs = configlist;
	Value = value;
	Load();
}

void ParamWidget::Load() {
	if (PS->Exists(Help.Param)) {
		Param = PS->GetVar(Help.Param);
		Param.Value = Value;
		isOption = true;
	} else {
		isOption = false;
	}

	Layout = new QHBoxLayout(this);
	Layout->setContentsMargins(0, 0, 0, 0);
	setLayout(Layout);

	if (isOption) {
		OE = new OptionEditor(Help.Param, Param, Help, this);
		connect(OE, &OptionEditor::Changed, this, &ParamWidget::RawUpdated);
		Layout->addWidget(OE);
		AddRemove();
		return;
	}

	if (Help.Param == "config")	{
		PC = new ParamConfigWidget(PS, AvailableConfigs, Value, this);
		connect(PC, &ParamConfigWidget::Updated, this, &ParamWidget::ConfigUpdated);
		Layout->addWidget(PC);
		QSpacerItem *Spacer = new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum);
		Layout->addSpacerItem(Spacer);
		AddRemove();
		return;
	}

	if (Help.DataType == ParamsSettings::param_file) {
		RawEdit = new QLineEdit(this);
		RawEdit->setText(Value);
		connect(RawEdit, &QLineEdit::textChanged, this, &ParamWidget::RawUpdated);
		Layout->addWidget(RawEdit);

		SelectButton = new QPushButton("Select", this);
		Layout->addWidget(SelectButton);
		connect(SelectButton, &QPushButton::clicked, this, &ParamWidget::SelectFile);

		AddRemove();
		return;
	}

	isVar = false;
	if (Help.Param == "var" ||
		Help.Param == "var_stream" ||
		Help.Param == "var_stream_video" ||
		Help.Param == "var_stream_audio" ||
		Help.Param == "var_stream_subtitle" ||
		Help.Param == "var_stream_attachment") {
		isVar = true;
		VarWidget = new QWidget(this);
		VarLayout = new QHBoxLayout(VarWidget);
		VarLayout->setContentsMargins(0, 0, 0, 0);
		VarName = new QLineEdit(VarWidget);
		VarName->setText(Value.section("=", 0, 0));
		VarName->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
		VarEqual = new QComboBox(VarWidget);
		VarEqual->addItem("=");
		VarEqual->addItem("==");
		VarValue = new QLineEdit(VarWidget);
		if (Value.contains("==")) {
			VarEqual->setCurrentText("==");
			VarValue->setText(Value.section("==", 1));
		} else {
			VarEqual->setCurrentText("=");
			VarValue->setText(Value.section("=", 1));
		}
		VarLayout->addWidget(VarName);
		VarLayout->addWidget(VarEqual);
		VarLayout->addWidget(VarValue);
		Layout->addWidget(VarWidget);
		connect(VarName, &QLineEdit::textChanged, this, &ParamWidget::RawUpdated);
		connect(VarEqual, &QComboBox::currentTextChanged, this, &ParamWidget::RawUpdated);
		connect(VarValue, &QLineEdit::textChanged, this, &ParamWidget::RawUpdated);
		AddRemove();
		return;
	}

	RawEdit = new QLineEdit(this);
	RawEdit->setText(Value);
	connect(RawEdit, &QLineEdit::textChanged, this, &ParamWidget::RawUpdated);
	Layout->addWidget(RawEdit);

	AddRemove();
}

QString ParamWidget::GetCommand() {
	QString Val = "";
	if (isOption) {
		Val = OE->GetText();
	} else if (Help.Param == "video" || Help.Param == "sub") {
		if (RawEdit->text() == "")
			Val = "\"\"";
		else
			Val = RawEdit->text();
	} else if (Help.Param == "config") {
		if (Value == "")
			Val = "config";
		else
			Val = Value;
	} else if (isVar) {
		Val = VarName->text() + VarEqual->currentText() + VarValue->text();
	} else {
		Val = RawEdit->text();
	}
	if (Val.contains(" ")) Val = "\"" + Val + "\"";
	return "--" + Help.Param + " " + Val;
}

void ParamWidget::SetValue(QString Value) {
	if (Help.DataType != ParamsSettings::param_file) return;
	RawEdit->setText(Value);
}

void ParamWidget::AddRemove() {
	Remove = new QPushButton("-", this);
	Remove->setVisible(false);
	connect(Remove, &QPushButton::clicked, this, &ParamWidget::RemoveClick);
	Layout->addWidget(Remove);
}

void ParamWidget::ConfigUpdated(QString value) {
	Value = value;
	emit Updated();
}

void ParamWidget::RawUpdated() {
	emit Updated();
}

void ParamWidget::RemoveClick() {
	emit Removed(this);
}

void ParamWidget::SelectFile() {
	QString File = QFileDialog::getOpenFileName(this,
				   "Select File for " + Help.Name,
				   "",
				   "All Files (*)");
	if (File != "") RawEdit->setText(QDir::toNativeSeparators(File));
}

void ParamWidget::TougleRemoveVisible(bool Visible) {
	Remove->setVisible(Visible);
}
