#ifndef BATCHFORMWIDGET_H
#define BATCHFORMWIDGET_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QListWidget>
#include <QSplitter>
#include <QPushButton>
#include <QFormLayout>
#include <QGroupBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QLabel>
#include <QSpacerItem>
#include <QSpinBox>
#include "batchcommandswidget.h"
#include <QSpacerItem>


class BatchFormWidget : public QWidget {
		Q_OBJECT
	public:
		explicit BatchFormWidget(QString &program, QStringList *configlist, QWidget *parent = nullptr);

		QVBoxLayout *CentralLayout;
		QSplitter *MainSplitter;
		QWidget *LeftWidget;
		QVBoxLayout *LeftLayout;
		QWidget *RightWidget;
		QVBoxLayout *RightLayout;
		QListWidget *InputList;
		QPushButton *AddFile;
		QPushButton *AddDir;
		QPushButton *DelInput;
		QWidget *InputButtonsWidget;
		QHBoxLayout *InputButtonsLayout;

		QWidget *OptionsRow1Widget;
		QHBoxLayout *OptionsRow1Layout;

		QGroupBox *FileTypesGroup;
		QVBoxLayout *FileTypesLayout;
		QCheckBox *VideoFiles;
		QCheckBox *SubtitlesFiles;
		QLineEdit *CustomFileExt;
		QLabel *CustomFileExtLabel;
		QWidget *CustomFileExtWidget;
		QHBoxLayout *CustomFileExtLayout;
		QSpacerItem *Spacer;

		QGroupBox *OptionsGroup;
		QVBoxLayout *OptionsLayout;
		QCheckBox *Recursive;
		QCheckBox *IgnoreFail;
		QCheckBox *ShowCMD;
		QCheckBox *Quiet;
		QCheckBox *ShowOutput;
		QSpinBox *NProcess;
		QWidget *NProcessWidget;
		QHBoxLayout *NProcessLayout;
		QLabel *NProcessLabel;
		QLineEdit *Command;

		QGroupBox *DaemonGroup;
		QFormLayout *DaemonLayout;
		QCheckBox *Watch;
		QSpinBox *Time;
		QSpinBox *Modified;
		QSpinBox *Recheck;
		QSpinBox *Delete;



		BatchCommandsWidget *BatchCommand;

		QSpacerItem *RightSpacer;

		void dragEnterEvent(QDragEnterEvent *e);
		void dropEvent(QDropEvent *e);

		void LoadCommand(QString Command);
		QString GetCommand();

	protected:
		int CommandCounter;
		QString CurrentCommand;
		bool Loading;
		QString Program;
		QStringList *AvailableConfigs;
		bool AutomaticChange;

	signals:


	public slots:
		void AddFileClick();
		void AddDirClick();
		void RemoveClick();
		void CommandChanged();
		void CommandManualChanged();
};

#endif // BATCHFORMWIDGET_H
