#ifndef PARAMWIDGET_H
#define PARAMWIDGET_H

#include <QWidget>
#include "../paramssettings.h"
#include <QHBoxLayout>
#include <QLineEdit>
#include "optioneditor.h"
#include "paramconfigwidget.h"
#include <QPushButton>

class ParamWidget : public QWidget {
		Q_OBJECT
	public:
		explicit ParamWidget(ParamsSettings *ps, QStringList *configlist, ParamsSettings::help_t help, QString value,
							 QWidget *parent = nullptr);
		explicit ParamWidget(ParamsSettings *ps, QStringList *configlist, QString param, QString value,
							 QWidget *parent = nullptr);
		void Load();
		QString GetCommand();
		ParamsSettings::help_t Help;
		void SetValue(QString Value);
	private:
		ParamsSettings *PS;
		QStringList *AvailableConfigs;
		bool isOption;
		ParamsSettings::params_t Param;
		QString Value;
		QHBoxLayout *Layout;
		QLineEdit *RawEdit;
		OptionEditor *OE;
		ParamConfigWidget *PC;
		QPushButton *Remove;
		QPushButton *SelectButton;

		bool isVar;
		QWidget *VarWidget;
		QHBoxLayout *VarLayout;
		QLineEdit *VarName;
		QComboBox *VarEqual;
		QLineEdit *VarValue;

		void AddRemove();

	signals:
		void Updated();
		void Removed(QWidget *RemovedObject);

	public slots:
		void ConfigUpdated(QString value);
		void RawUpdated();
		void RemoveClick();
		void SelectFile();
		void TougleRemoveVisible(bool Visible);
};

#endif // PARAMWIDGET_H
