#include "useroptioneditor.h"

UserOptionEditor::UserOptionEditor(QString ParamName, ParamsSettings::params_t param, QWidget *parent)
	: QWidget{parent}, ParamName(ParamName), Param(param) {
	Layout = new QHBoxLayout(this);
	Layout->setContentsMargins(0, 0, 0, 0);

	VarWidget = new QWidget(this);
	VarLayout = new QHBoxLayout(VarWidget);
	VarLayout->setContentsMargins(0, 0, 0, 0);
	VarName = new QLineEdit(VarWidget);
	VarName->setText(Param.UserName);
	EqualLabel = new QLabel("=");
	VarName->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	VarValue = new QLineEdit(VarWidget);
	if (Param.Function != "")
		VarValue->setText(Param.Function);
	else
		VarValue->setText(Param.Value);
	VarLayout->addWidget(VarName);
	VarLayout->addWidget(EqualLabel);
	VarLayout->addWidget(VarValue);
	Layout->addWidget(VarWidget);

	RawEditor = new QLineEdit(this);
	RawEditor->setVisible(false);
	RawEditor->setText(Param.Value);
	RawEditor->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	RawEditor->setText(VarName->text() + "=" + VarValue->text());
	Layout->addWidget(RawEditor);
	RawTougle = new QPushButton("Show Raw", this);
	RawTougle->setCheckable(true);
	Layout->addWidget(RawTougle);

	connect(VarName, &QLineEdit::textChanged, this, &UserOptionEditor::ValueChanged);
	connect(VarValue, &QLineEdit::textChanged, this, &UserOptionEditor::ValueChanged);
	connect(RawTougle, &QPushButton::toggled, this, &UserOptionEditor::RawTougleChange);
	connect(RawEditor, &QLineEdit::textChanged, this, &UserOptionEditor::RawChange);
}

void UserOptionEditor::RawTougleChange(bool Checked) {
	setUpdatesEnabled(false);
	RawEditor->setVisible(Checked);
	VarWidget->setVisible(!Checked);
	setUpdatesEnabled(true);
}

void UserOptionEditor::ValueChanged() {
	if (RawBlock) return;
	ValueBlock = true;
	RawEditor->setText(VarName->text() + "=" + VarValue->text());
	emit Changed(ParamName, VarName->text(), VarValue->text());
	ValueBlock = false;
}

void UserOptionEditor::RawChange() {
	if (ValueBlock) return;
	RawBlock = true;
	VarName->setText(RawEditor->text().section("=", 0, 0));
	VarValue->setText(RawEditor->text().section("=", 1, 1));
	emit Changed(ParamName, VarName->text(), VarValue->text());
	RawBlock = false;
}
