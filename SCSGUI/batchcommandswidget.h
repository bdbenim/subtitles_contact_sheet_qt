#ifndef BATCHCOMMANDSWIDGET_H
#define BATCHCOMMANDSWIDGET_H

#include <QWidget>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QPushButton>

class BatchCommandsWidget : public QWidget {
		Q_OBJECT
	public:
		explicit BatchCommandsWidget(QStringList *Configs, QWidget *parent = nullptr);

		QVBoxLayout *MainLayout;
		QGroupBox *CommandsGroup;
		QVBoxLayout *CommandsLayout;
		QWidget *ButtonsWidget;
		QHBoxLayout *ButtonsLayout;
		QPushButton *AddButton;

		void SetCommand(QStringList Commands);
		QString getCommands();
		void AddCommand(QStringList *Commands = nullptr);

	protected:
		QStringList *AvailableConfigs;
		int Count;
		QList<QWidget *> Items;

	signals:
		void Changed();

	public slots:
		void AddCommandClick();
		void DeletedItem(QWidget *Item);
		void Clear();
		void ItemChanged();

};

#endif // BATCHCOMMANDSWIDGET_H
