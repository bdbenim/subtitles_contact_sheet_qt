#include "scsmainwindow.h"
#include <QProcess>
#include <QApplication>
#include <QDir>
#include <QFileInfo>
#include <QRegularExpression>
#include <QIcon>

SCSMainWindow::SCSMainWindow(QString OpenFile, QWidget *parent)
	: QMainWindow{parent} {

	setWindowTitle("Subtitles Contact Sheet GUI");
	QIcon icon;
	icon.addFile(QString::fromUtf8(":/SCS.ico"), QSize(), QIcon::Normal, QIcon::Off);
	setWindowIcon(icon);

	#ifdef Q_OS_WIN
	Program = QDir::toNativeSeparators(QApplication::applicationDirPath() + QDir::separator() +
									   "subtitles_contact_sheet.exe");
	if (!QFileInfo::exists(Program)) Program = QDir::toNativeSeparators(QApplication::applicationDirPath() +
				QDir::separator() + "scs.exe");
	#else
	Program = QDir::toNativeSeparators(QApplication::applicationDirPath() + QDir::separator() +
									   "subtitles_contact_sheet");
	if (!QFileInfo::exists(Program)) Program = QDir::toNativeSeparators(QApplication::applicationDirPath() +
				QDir::separator() + "scs");
	if (!QFileInfo::exists(Program)) Program = "subtitles_contact_sheet";
	#endif

	#ifdef Q_OS_WIN
	QString GUIConfigFile = ConfigPath + QDir::separator() + "GUI" + QDir::separator() + "config.ini";
	Settings = new QSettings(GUIConfigFile, QSettings::IniFormat, this);
	#else
	Settings = new QSettings("subtitles_contact_sheet_GUI", "config", this);
	#endif

	ReadConfigList();

	if (ProgramFailed) {
		//TODO: Ask for subtitles_contcat_sheet binary
	}

	PS = new ParamsSettings(QApplication::applicationDirPath(), this);

	CentralWidget = new QWidget(this);
	CentralLayout = new QVBoxLayout(CentralWidget);
	CentralWidget->setLayout(CentralLayout);
	setCentralWidget(CentralWidget);

	Tabs = new QTabWidget(CentralWidget);

	Normal = new ParamsWidget(Program, &AvailableConfigs, PS, Tabs);
	QString NormalTabCommand = Settings->value("NormalTabCommand",
							   Program + " --video \"\" --config config --layout 3x7 --size 640 --process 2 "
							   "--start 30 --end -15 --only_keyframes 0").toString();
	Normal->SetCommand(NormalTabCommand);
	Tabs->addTab(Normal, "Normal");

	Subtitles = new ParamsWidget(Program, &AvailableConfigs, PS, Tabs);
	QString SubtitlesTabCommand = Settings->value("SubtitlesTabCommand",
								  Program + " --video \"\" --sub \"\" --config config --mode time --min 4 --start 30 "
								  "--end 15 --lang en --sub_font \"DejaVu Sans\" --sub_size 16 --sub_color white").toString();
	Subtitles->SetCommand(SubtitlesTabCommand);
	Tabs->addTab(Subtitles, "Subtitles");

	VR = new ParamsWidget(Program, &AvailableConfigs, PS, Tabs);
	QString VRTabCommand = Settings->value("VRTabCommand",
										   Program + " --video \"\" --config config --layout 3x7 --size 960 --vr 1 "
										   "--vr_only_crop 1 --vr_in hequirect --vr_in_stereo sbs --vr_ih_fov 180 --vr_iv_fov 180 "
										   "--vr_d_fov 100 --vr_right_eye 0").toString();
	VR->SetCommand(VRTabCommand);
	Tabs->addTab(VR, "VR");

	Gif = new ParamsWidget(Program, &AvailableConfigs, PS, Tabs);
	QString GifTabCommand = Settings->value("GifTabCommand",
											Program + " --video \"\" --config config --gif_clip 10").toString();
	Gif->SetCommand(GifTabCommand);
	Tabs->addTab(Gif, "Gif");

	Screenshots = new ParamsWidget(Program, &AvailableConfigs, PS, Tabs);
	QString ScreenshotsTabCommand = Settings->value("ScreenshotsTabCommand",
									Program + " --video \"\" --config config --screenshots 10").toString();
	Screenshots->SetCommand(ScreenshotsTabCommand);
	Tabs->addTab(Screenshots, "Screenshost");

	BatchWidget = new BatchFormWidget(Program, &AvailableConfigs, CentralWidget);
	Tabs->addTab(BatchWidget, "Batch");

	Log = new QPlainTextEdit(CentralWidget);
	Log->setVisible(false);
	ProgressWidget = new QWidget(CentralWidget);
	ProgressLayout = new QHBoxLayout(ProgressWidget);
	ProgressWidget->setLayout(ProgressLayout);
	CentralLayout->addWidget(Tabs);
	CentralLayout->addWidget(Log);
	CentralLayout->addWidget(ProgressWidget);

	Progress = new QProgressBar(ProgressWidget);
	ShowLog = new QPushButton("Show Log", ProgressWidget);
	ShowLog->setCheckable(true);
	connect(ShowLog, &QPushButton::toggled, Log, &QPlainTextEdit::setVisible);
	Cancel = new QPushButton("Cancel", ProgressWidget);
	Cancel->setDisabled(true);
	connect(Cancel, &QPushButton::clicked, this, &SCSMainWindow::CancelClick);
	Execute = new QPushButton("Execute", ProgressWidget);
	connect(Execute, &QPushButton::clicked, this, &SCSMainWindow::ExecuteClick);
	ProgressLayout->addWidget(Progress);
	ProgressLayout->addWidget(ShowLog);
	ProgressLayout->addWidget(Cancel);
	ProgressLayout->addWidget(Execute);

	connect(&Process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this,
			&SCSMainWindow::ProcessTerminate);
	connect(&Process, &QProcess::readyReadStandardOutput, this, &SCSMainWindow::ProcessReadData);
	connect(&Process, &QProcess::readyReadStandardError, this, &SCSMainWindow::ProcessErrorData);

	Settings->beginGroup("MainWindow");
	if (Settings->contains("size"))
		resize(Settings->value("size", QSize(400, 400)).toSize());
	if (Settings->contains("pos"))
		move(Settings->value("pos", QPoint(200, 200)).toPoint());
	if (Settings->value("fullscreen", "0").toBool())
		showFullScreen();
	Settings->endGroup();

	if (OpenFile != "") {
		Normal->SetInputFile(OpenFile);
		Subtitles->SetInputFile(OpenFile);
		VR->SetInputFile(OpenFile);
		Gif->SetInputFile(OpenFile);
		Screenshots->SetInputFile(OpenFile);
	}
}

void SCSMainWindow::ReadConfigList() {
	ProgramFailed = false;
	QStringList params;
	params << "--list_config_gui";
	QProcess process;
	process.start(Program, params);
	if (!process.waitForFinished(-1)) {
		ProgramFailed = true;
		return;
	}
	if (process.exitCode() != 0 ) {
		ProgramFailed = true;
		return;
	}

	QString Data = process.readAll().trimmed();
	AvailableConfigs = Data.split("\n");
	AvailableConfigs.prepend("");
}

QString SCSMainWindow::GetCurrentTabCommand() {
	QWidget *Widget = Tabs->currentWidget();
	if (QString(Widget->metaObject()->className()) == "ParamsWidget") {
		ParamsWidget *CurParamsWidget = static_cast<ParamsWidget *>(Widget);
		return CurParamsWidget->GetCommand();
	} else if (QString(Widget->metaObject()->className()) == "BatchFormWidget") {
		BatchFormWidget *Batch = static_cast<BatchFormWidget *>(Widget);
		return Batch->GetCommand();
	}
	return "";
}

void SCSMainWindow::ExecuteClick() {
	QString Command = GetCurrentTabCommand();
	if (Command == "") return;
	QStringList Parameters = QProcess::splitCommand(Command);
	Parameters.takeFirst();
	Parameters.prepend("--gui_progress");
	Process.start(Program, Parameters);
	Process.waitForStarted(-1);
	Execute->setDisabled(true);
	Cancel->setDisabled(false);
}

void SCSMainWindow::CancelClick() {
	if (Process.state() != QProcess::NotRunning) Process.close();
	Progress->setValue(0);
	Execute->setDisabled(false);
	Cancel->setDisabled(true);
}

void SCSMainWindow::ProcessTerminate(int /*exitCode*/, QProcess::ExitStatus /*exitStatus*/) {
	Progress->setValue(Progress->maximum());
	Execute->setDisabled(false);
	Cancel->setDisabled(true);
}

void SCSMainWindow::ProcessReadData() {
	QString Data = Process.readAllStandardOutput();
	QRegularExpression ProgressRegExp("^([0-9]+)/([0-9]+)$");
	QRegularExpressionMatch Match;
	#ifdef Q_OS_WIN
	QStringList Lines = Data.split("\r\n");
	#else
	QStringList Lines = Data.split("\n");
	#endif
	for (int i = 0; i < Lines.count(); ++i) {
		Match = ProgressRegExp.match(Lines.at(i));
		if (Match.hasMatch()) {
			int Current = Match.captured(1).toInt();
			int Total = Match.captured(2).toInt();
			Progress->setMaximum(Total);
			Progress->setValue(Current);
		} else {
			if (Lines.at(i) != "") Log->appendPlainText(Lines.at(i));
		}
	}
}

void SCSMainWindow::ProcessErrorData() {
	QString Data = Process.readAllStandardError();
	#ifdef Q_OS_WIN
	QStringList Lines = Data.split("\r\n");
	#else
	QStringList Lines = Data.split("\n");
	#endif
	for (int i = 0; i < Lines.count(); ++i) {
		if (Lines.at(i) != "") Log->appendHtml("<p style=color:red;>" + Lines.at(i) + "</p>");
	}
	ShowLog->setChecked(true);
}

void SCSMainWindow::closeEvent(QCloseEvent */*event*/) {
	Settings->setValue("NormalTabCommand", Normal->GetCommand());
	Settings->setValue("SubtitlesTabCommand", Subtitles->GetCommand());
	Settings->setValue("VRTabCommand", VR->GetCommand());
	Settings->setValue("GifTabCommand", Gif->GetCommand());
	Settings->setValue("ScreenshotsTabCommand", Screenshots->GetCommand());

	Settings->beginGroup("MainWindow");
	Settings->setValue("size", size());
	Settings->setValue("pos", pos());
	Settings->setValue("fullscreen", isFullScreen());
	Settings->endGroup();
}
