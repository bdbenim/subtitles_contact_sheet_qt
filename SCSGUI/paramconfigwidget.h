#ifndef PARAMCONFIGWIDGET_H
#define PARAMCONFIGWIDGET_H

#include <QWidget>
#include <QComboBox>
#include <QPushButton>
#include <QHBoxLayout>
#include <QString>
#include <QStringList>
#include "../paramssettings.h"

class ParamConfigWidget : public QWidget {
		Q_OBJECT
	public:
		explicit ParamConfigWidget(ParamsSettings *ps, QStringList *configlist, QString value, QWidget *parent = nullptr);

	private:
		QStringList *AvailableConfigs;
		QHBoxLayout *Layout;
		QComboBox *Combo;
		QPushButton *Edit;
		QPushButton *New;
		QPushButton *Del;
		ParamsSettings *PS;

	signals:
		void Updated(QString Value);
		void NewConfig(QString name);
	public slots:
		void EditClick();
		void NewClick();
		void DelClick();
		void ComboChanged(QString Value);

};

#endif // PARAMCONFIGWIDGET_H
