#ifndef CONFIGURATIONDIALOG_H
#define CONFIGURATIONDIALOG_H

#include <QWidget>
#include <QDialog>
#include "../paramssettings.h"
#include "configurationwidget.h"
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>

class ConfigurationDialog : public QDialog {
		Q_OBJECT
	public:
		ConfigurationDialog(QString name, QWidget *parent = nullptr, const Qt::WindowFlags &f = Qt::WindowFlags());


	protected:
		QString Name;
		ParamsSettings *PS;
		ConfigurationWidget *CW;

		QVBoxLayout *MainLayout;
		QWidget *ButtonsWidget;
		QHBoxLayout *ButtonsLayout;
		QPushButton *CancelButton;
		QPushButton *SaveButton;

	public slots:
		void Save();
		void Cancel();
};

#endif // CONFIGURATIONDIALOG_H
