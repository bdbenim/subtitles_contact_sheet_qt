#ifndef BATCH_H
#define BATCH_H

#include <QFileSystemWatcher>
#include <QQueue>
#include <QString>
#include <QStringList>

struct command_queue_item {
	QString File;
	bool InProgress;
	QString Program;
	QStringList Params;
	QString Text;
};

struct batch_instance_t {
	QProcess *Process;
	command_queue_item Command;
	bool InProgress;
};

class Batch {
	public:
		Batch();
		int Execute(int argc, char *argv[]);
		void DirSelect(const QString Dir);
		void PrintInfo(QString line);
		void PrintError(QString line);
		void PrintCommand(QString command);
		void AddFileToCommandQueue(QString File);
	protected:
		bool Quiet = false;
		bool Daemon = false;
		bool GUIProgress = false;
		int Steps = 0;
		int TotalSteps = 0;
		QStringList StartDirList;
		QStringList DirListQueue;
		QStringList FileListQueue;
		QStringList FileListDone;
		QStringList FileListProcessing;
		QStringList FileListIncomplete;
		QStringList FileFilters;
		QQueue<command_queue_item> CommandQueue;
		QStringList ExtCommand;
		QList<QStringList> ExtParams;
		int ExtCurrent = 0;
		QString Command;

		int ScanDirsEvery = 0;
		int CleanDeletedFilesEvery = 3600;

		#ifdef Q_OS_WIN
		//On windows the watcher not works correctly, this value mitigated the problems
		int ModifiedTime = 10;
		int CheckIncompleteEvery = 10;
		#else
		//On linux this option isn't necesary
		int ModifiedTime = 1;
		int CheckIncompleteEvery = 0;
		#endif

		bool Recursive = false;
		bool OwnParams = true;
		bool NoFail = false;
		int Watch = false;
		bool CMD = false;
		bool ShowOutput = false;

		QFileSystemWatcher *Watcher;
		int CurrentInstances = 1;
		QList<batch_instance_t> Instances;
};

#endif // BATCH_H
