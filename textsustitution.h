#ifndef TEXTSUSTITUTION_H
#define TEXTSUSTITUTION_H

#include <QObject>
#include <QJsonObject>
#include <QMap>
#include <QLocale>
#include "textblock.h"

class TextSustitution : public QObject {
		Q_OBJECT
	public:
		explicit TextSustitution(QObject *parent = nullptr);
		QString ScapeCaracters(QString in);
		void SetReplaceDir(QString dir);
		void LoadVars(QJsonObject Object, QString Name = "");
		QString PrefixConverter(double Value, bool Binary, uint8_t Decimals, bool Separated, bool Formated, QStringList Units,
								QList<uint8_t> DecimalsList = {});
		static bool DetectFunction(QString Text);
		QString DetectAndEjecuteFunction(QString Text, QString Context = "", bool *Executed = nullptr);
		QString ProcessFunctions(QString Function, QStringList Params, QString Context, bool TextFormat = true);
		QString ProcessText(QString Context, QString Text, text_sub_style_t *Style);
		QString ProcessCMD(QString Context, QString CMD);
		QString ProcessTemplate(QString Template);

		void Insert(QString Var, QString Value, bool scape = true);
		QString Value(QString Var, QString Default = "");
		bool Contains(QString Var);
		void Remove(QString Var);
		void Update(QString Var, QString Value, bool scape = true);
		void PrintAll();
	private:
		struct pix_fmt_data {
			uint8_t bits;
			QString Chroma;
			QString Mode;
		};

		QString ReplaceDir;

		QLocale *Local;
		QMap<QString, pix_fmt_data> PixelFormatDatabase;
		QMap<QString, QString> Vars;
		QMap<QString, QStringList> Replace;

		void EmptyMatch(QString Context, QString &Result, bool TextFormat = true);
		void SimpleMatch(QString Context, QString &Result, bool TextFormat = true);
		QString ValueCheckContext(QString Var, QString Context = "", bool Empty = false);
		QString Operation(QString Operation, QString Value, QStringList Params, QString Context);

	signals:

};

#endif // TEXTSUSTITUTION_H
